# Prepare chef image 
FROM lukemathwalker/cargo-chef:latest-rust-1 AS chef
WORKDIR app

# Prepare project recipe
FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

# Build project
FROM chef AS builder
COPY --from=planner /app/recipe.json recipe.json
RUN apt-get clean && apt-get -y update && apt-get -y install libssl-dev pkg-config protobuf-compiler libprotobuf-dev
RUN cargo chef cook --release --recipe-path recipe.json
COPY . .
RUN RUSTFLAGS="-C target-cpu=native" cargo install --path .

# Run project
FROM gcr.io/distroless/cc-debian11
WORKDIR /app
COPY --from=builder /usr/local/cargo/bin/* /usr/local/bin/
ENTRYPOINT [ "carp" ]
