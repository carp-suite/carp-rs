use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Invalid scheme provided. Expected \"http\" or \"https\". Got {0}")]
    InvalidScheme(String),
    #[error("No scheme provided. Expected \"http\" or \"https\".")]
    MissingScheme,
}
