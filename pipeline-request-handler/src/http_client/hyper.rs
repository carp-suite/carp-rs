use http::uri::{Authority, Parts};
use http::{Request, Response, Uri};
use hyper::client::{HttpConnector, ResponseFuture};
use hyper::{Body, Client};
use hyper_tls::HttpsConnector;
use pipeline::http_client::HttpClient;

use super::scheme::Scheme;

#[derive(Clone)]
enum HyperClient {
    HTTP(Client<HttpConnector>),
    HTTPS(Client<HttpsConnector<HttpConnector>>),
}

#[derive(Clone)]
pub struct HyperClientService {
    hyper_client: HyperClient,
    https: bool,
    authority: http::uri::Authority,
}

impl HyperClientService {
    pub fn new(scheme: Scheme, authority: Authority) -> Self {
        let (client, https) = match scheme {
            Scheme::HTTP => (HyperClient::HTTP(Client::builder().build_http()), false),
            Scheme::HTTPS => (
                HyperClient::HTTPS(Client::builder().build(HttpsConnector::new())),
                true,
            ),
        };

        HyperClientService {
            hyper_client: client,
            https,
            authority,
        }
    }
}

impl HttpClient<Body, Body> for HyperClientService {
    type Error = hyper::Error;
    type Future = ResponseFuture;
    type BeforeRequestFuture =
        futures::future::Ready<Result<either::Either<Request<Body>, Response<Body>>, Self::Error>>;
    type AfterRequestFuture = futures::future::Ready<Result<Response<Body>, Self::Error>>;

    #[inline]
    fn request(&self, mut request: Request<Body>) -> Self::Future {
        let mut parts = Parts::default();

        parts.scheme = if self.https {
            Some(http::uri::Scheme::HTTPS)
        } else {
            Some(http::uri::Scheme::HTTP)
        };
        parts.authority = Some(self.authority.clone());
        parts.path_and_query = request.uri().path_and_query().cloned();

        log::debug!("Making request with method {}", request.method());
        *request.uri_mut() = Uri::from_parts(parts).unwrap();

        match self.hyper_client {
            HyperClient::HTTP(ref client) => client.request(request),
            HyperClient::HTTPS(ref client) => client.request(request),
        }
    }

    #[inline]
    fn before_request(&self, req: Request<Body>) -> Self::BeforeRequestFuture {
        futures::future::ready(Ok(either::Either::Left(req)))
    }

    #[inline]
    fn after_request(&self, res: Response<Body>) -> Self::AfterRequestFuture {
        futures::future::ready(Ok(res))
    }
}
