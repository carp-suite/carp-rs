use super::error::Error;
use http::uri;
use url::Url;

#[repr(u8)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Scheme {
    HTTP,
    HTTPS,
}

impl<'a> TryFrom<&'a Url> for Scheme {
    type Error = Error;

    fn try_from(value: &'a Url) -> Result<Self, Self::Error> {
        let scheme = value.scheme().to_uppercase();
        match &*scheme {
            "HTTP" => Ok(Scheme::HTTP),
            "HTTPS" => Ok(Scheme::HTTPS),
            _ => Err(Error::InvalidScheme(scheme)),
        }
    }
}

impl<'a> TryFrom<&'a uri::Uri> for Scheme {
    type Error = Error;

    fn try_from(value: &'a uri::Uri) -> Result<Self, Self::Error> {
        let Some(scheme) = value.scheme() else {
            return Err(Error::MissingScheme);
        };

        if scheme == &uri::Scheme::HTTP {
            Ok(Scheme::HTTP)
        } else if scheme == &uri::Scheme::HTTPS {
            Ok(Scheme::HTTPS)
        } else {
            Err(Error::InvalidScheme(scheme.to_string()))
        }
    }
}
