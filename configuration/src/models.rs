use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use models::route::http_method;

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(untagged)]
pub enum Value {
    Number(isize),
    String(String),
    Array(Vec<Value>),
    Map(HashMap<String, Value>),
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Plugin {
    pub url: String,
    pub configuration: Option<HashMap<String, Value>>,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Settings {
    pub host: String,
    pub port: u16,
    pub base_path: String,
    pub http_version: u8,
    pub cert: Option<String>,
    pub key: Option<String>,
    pub plugins: HashMap<String, Plugin>,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(untagged)]
pub enum CachePluginEntry {
    PluginName(String),
    OverridePlugin(HashMap<String, String>),
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Cache {
    pub name: String,
    pub ttl: Option<u64>,
    pub dependencies: Vec<String>,
    pub plugins: Option<Vec<CachePluginEntry>>,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct CacheUsage {
    pub name: String,
    #[serde(rename = "route parameters")]
    pub route_parameters: Vec<String>,
    #[serde(rename = "query parameters")]
    pub query_parameters: Vec<String>,
    pub headers: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct RoutePluginEntry {
    pub request_start: Vec<String>,
    pub request_end: Vec<String>,
}

#[derive(Debug, Deserialize, PartialEq)]
pub struct Route {
    pub host: String,
    pub cache: Option<CacheUsage>,
    pub invalidates: Vec<String>,
    pub plugins: Option<RoutePluginEntry>,
}

#[derive(Debug, Hash, PartialEq, Eq, Deserialize)]
pub enum HttpMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    WILDCARD,
}

impl From<HttpMethod> for http_method::HttpMethod {
    fn from(value: HttpMethod) -> Self {
        match value {
            HttpMethod::GET => http_method::HttpMethod::GET,
            HttpMethod::POST => http_method::HttpMethod::POST,
            HttpMethod::PUT => http_method::HttpMethod::PUT,
            HttpMethod::PATCH => http_method::HttpMethod::PATCH,
            HttpMethod::DELETE => http_method::HttpMethod::DELETE,
            HttpMethod::WILDCARD => http_method::HttpMethod::WILDCARD,
        }
    }
}

pub type Routes = Vec<HashMap<HttpMethod, Route>>;
pub type RouteDef = HashMap<HttpMethod, Route>;
pub type Caches = Vec<Cache>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_converts_from_get_method_from_config_http_method_to_models_http_method() {
        // Given
        let http_method = HttpMethod::GET;

        // When
        let config_http_method = http_method::HttpMethod::from(http_method);

        // Then
        assert_eq!(http_method::HttpMethod::GET, config_http_method)
    }

    #[test]
    fn it_converts_from_post_method_from_config_http_method_to_models_http_method() {
        // Given
        let http_method = HttpMethod::POST;

        // When
        let config_http_method = http_method::HttpMethod::from(http_method);

        // Then
        assert_eq!(http_method::HttpMethod::POST, config_http_method)
    }

    #[test]
    fn it_converts_from_put_method_from_config_http_method_to_models_http_method() {
        // Given
        let http_method = HttpMethod::PUT;

        // When
        let config_http_method = http_method::HttpMethod::from(http_method);

        // Then
        assert_eq!(http_method::HttpMethod::PUT, config_http_method)
    }

    #[test]
    fn it_converts_from_patch_method_from_config_http_method_to_models_http_method() {
        // Given
        let http_method = HttpMethod::PATCH;

        // When
        let config_http_method = http_method::HttpMethod::from(http_method);

        // Then
        assert_eq!(http_method::HttpMethod::PATCH, config_http_method)
    }

    #[test]
    fn it_converts_from_delete_method_from_config_http_method_to_models_http_method() {
        // Given
        let http_method = HttpMethod::DELETE;

        // When
        let config_http_method = http_method::HttpMethod::from(http_method);

        // Then
        assert_eq!(http_method::HttpMethod::DELETE, config_http_method)
    }
}
