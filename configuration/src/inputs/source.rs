use std::path::PathBuf;

#[derive(Debug, PartialEq)]
pub enum Source {
    Filesystem(PathBuf, &'static str),
}
