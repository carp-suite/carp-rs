use std::error::Error;

use futures::Future;

use self::{format::Format, source::Source};

pub mod clap;
pub mod format;
pub mod source;

pub trait ConfigurationInputs<I> {
    type Error: Error;
    type Future: Future<Output = Result<(Format, Source), Self::Error>>;

    fn get_configuration_inputs(self, input: I) -> Self::Future;
}

impl<F, Fut, E, I> ConfigurationInputs<I> for F
where
    F: FnOnce(I) -> Fut,
    Fut: Future<Output = Result<(Format, Source), E>>,
    E: Error,
{
    type Error = E;
    type Future = Fut;

    fn get_configuration_inputs(self, input: I) -> Self::Future {
        (self)(input)
    }
}

pub fn fn_inputs<F, E, I, Fut>(f: F) -> impl ConfigurationInputs<I, Future = Fut, Error = E>
where
    F: FnOnce(I) -> Fut,
    Fut: Future<Output = Result<(Format, Source), E>>,
    E: Error,
{
    f
}
