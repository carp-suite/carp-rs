use std::{ffi::OsString, process::exit};

use clap::{
    error::{Error, ErrorKind},
    Parser,
};

use super::{format::Format, source::Source, ConfigurationInputs};

#[derive(Parser, Debug)]
#[command(
    author,
    version,
    about,
    long_about = "CARP is a reverse proxy and HTTP cache made to make caching deterministic via the use of \"caching rules\".\n\
                  These caching rules allow the cache of a route to be kept alive or invalidated deterministically via hitting\n\
                  another route or TTL"
)]
struct ClapInputs {
    #[arg(long = "yaml", help = "Parse input as YAML format")]
    pub yaml: bool,
    #[arg(long = "json", help = "Parse input as JSON format")]
    pub json: bool,
    #[arg(
        help = "Base directory where the \"settings\", \"cache\" files and \"routes\" directory will be found"
    )]
    pub base_dir: String,
}

pub(self) async fn get_configuration_inputs<R, S>(input: R) -> Result<(Format, Source), Error>
where
    R: IntoIterator<Item = S>,
    S: Into<OsString> + Clone,
{
    let ClapInputs {
        json,
        yaml,
        base_dir,
    } = match ClapInputs::try_parse_from(input) {
        Ok(inputs) => inputs,
        Err(err)
            if err.kind() == ErrorKind::DisplayHelp
                || err.kind() == ErrorKind::DisplayVersion
                || err.kind() == ErrorKind::DisplayHelpOnMissingArgumentOrSubcommand =>
        {
            err.print()?;
            exit(0)
        }
        Err(err) => return Err(err),
    };
    let (format_input, format) = if json {
        (Format::JSON, "json")
    } else if yaml {
        (Format::YAML, "yaml")
    } else {
        unreachable!()
    };
    let source_input = Source::Filesystem(base_dir.into(), format);

    Ok((format_input, source_input))
}

pub fn clap_configuration_inputs<R, S>() -> impl ConfigurationInputs<R, Error = Error>
where
    R: IntoIterator<Item = S>,
    S: Into<OsString> + Clone,
{
    get_configuration_inputs::<R, S>
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn it_should_get_yaml_parameter_from_input() {
        // Given
        let input = "carp --yaml .".split(' ').map(OsString::from);
        let configuration_inputs = clap_configuration_inputs();

        // When
        let (format, source) = configuration_inputs
            .get_configuration_inputs(input)
            .await
            .unwrap();

        // Then
        assert_eq!(format, Format::YAML);
        assert_eq!(source, Source::Filesystem(".".into(), "yaml"));
    }

    #[tokio::test]
    async fn it_should_get_json_parameter_from_input() {
        // Given
        let input = "carp --json .".split(' ').map(OsString::from);
        let configuration_inputs = clap_configuration_inputs();

        // When
        let (format, source) = configuration_inputs
            .get_configuration_inputs(input)
            .await
            .unwrap();

        // Then
        assert_eq!(format, Format::JSON);
        assert_eq!(source, Source::Filesystem(".".into(), "json"));
    }
}
