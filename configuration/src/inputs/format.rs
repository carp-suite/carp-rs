#[repr(u8)]
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Format {
    JSON,
    YAML,
}
