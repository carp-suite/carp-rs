use futures::Future;
use tokio::io::AsyncRead;

use self::{configuration_sources::ConfigurationSources, route_entry::RouteEntry};

pub mod configuration_sources;
pub mod fs;
pub mod route_entry;

pub trait ConfigurationSource {
    type SettingsSource: AsyncRead;
    type CachesSource: AsyncRead;
    type RoutesSources: IntoIterator;

    type Error: std::error::Error;
    type Future: Future<
        Output = Result<
            ConfigurationSources<Self::SettingsSource, Self::CachesSource, Self::RoutesSources>,
            Self::Error,
        >,
    >
    where
        <Self::RoutesSources as IntoIterator>::Item: RouteEntry;

    fn get_sources(self) -> Self::Future
    where
        <Self::RoutesSources as IntoIterator>::Item: RouteEntry;
}

impl<Settings, Caches, Routes, F, E, Fut> ConfigurationSource for F
where
    F: FnOnce() -> Fut,
    E: std::error::Error,
    Fut: Future<Output = Result<ConfigurationSources<Settings, Caches, Routes>, E>>,
    Settings: AsyncRead,
    Caches: AsyncRead,
    Routes: IntoIterator,
    Routes::Item: RouteEntry,
{
    type SettingsSource = Settings;
    type CachesSource = Caches;
    type RoutesSources = Routes;
    type Error = E;
    type Future = Fut;

    fn get_sources(self) -> Self::Future {
        (self)()
    }
}
