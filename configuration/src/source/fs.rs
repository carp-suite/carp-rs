use std::{
    ffi::OsStr,
    path::{Path, PathBuf},
};

use tokio::fs::{read_dir, File};

use super::{
    configuration_sources::ConfigurationSources, route_entry::RouteEntry, ConfigurationSource,
};

pub struct FsRouteEntry(PathBuf, File);

impl RouteEntry for FsRouteEntry {
    type Path = String;
    type Source = File;

    fn into_parts(self) -> (Self::Path, Self::Source) {
        (self.0.into_os_string().into_string().unwrap(), self.1)
    }
}

pub(crate) async fn try_get_fs_configuration_sources<P, F>(
    base_dir: P,
    format: F,
) -> Result<ConfigurationSources<File, File, Vec<FsRouteEntry>>, tokio::io::Error>
where
    P: AsRef<Path>,
    F: AsRef<str>,
{
    let base_dir = base_dir.as_ref();
    let settings_name = format!("settings.{}", format.as_ref());
    let caches_name = format!("caches.{}", format.as_ref());
    let settings_file_path = base_dir.join(settings_name.as_str());
    let caches_file_path = base_dir.join(caches_name.as_str());
    let routes_dir = base_dir.join("routes");
    let mut route_entries_paths = vec![];
    let mut routes = vec![];

    let settings_file = File::open(settings_file_path).await?;
    let caches_file = File::open(caches_file_path).await?;
    let mut routes_entries = read_dir(routes_dir.as_path()).await?;

    while let Some(entry) = routes_entries.next_entry().await? {
        route_entries_paths.push(entry.path());
    }

    loop {
        let Some(entry_path) = route_entries_paths.pop() else {
            break;
        };

        if !entry_path.is_file() {
            routes_entries = read_dir(entry_path).await?;
            while let Some(entry) = routes_entries.next_entry().await? {
                route_entries_paths.push(entry.path());
            }
        } else if entry_path.extension() == Some(OsStr::new(format.as_ref())) {
            let route_path = entry_path.strip_prefix(routes_dir.as_path()).unwrap();

            routes.push(FsRouteEntry(
                route_path.to_owned(),
                File::open(entry_path).await?,
            ))
        }
    }

    Ok(ConfigurationSources {
        settings: settings_file,
        caches: caches_file,
        routes,
    })
}

pub fn fs_source<P, F>(
    base_dir: P,
    format: F,
) -> impl ConfigurationSource<
    Error = tokio::io::Error,
    SettingsSource = File,
    CachesSource = File,
    RoutesSources = Vec<FsRouteEntry>,
>
where
    P: AsRef<Path>,
    F: AsRef<str>,
{
    move || try_get_fs_configuration_sources(base_dir, format)
}

#[cfg(test)]
mod tests {
    use tokio::io::AsyncReadExt;

    use super::*;

    #[tokio::test]
    async fn it_should_return_settings_caches_and_routes_json() {
        // Given
        let expected_settings_contents = include_str!("../../test/files/settings.json");
        let expected_cache_contents = include_str!("../../test/files/caches.json");
        let expected_routes_index_contents = include_str!("../../test/files/routes/index.json");
        let expected_routes_subroute_index_contents =
            include_str!("../../test/files/routes/subroute/index.json");
        let expected_routes_subroute_post_contents =
            include_str!("../../test/files/routes/subroute/post.json");

        // When
        let mut sources =
            try_get_fs_configuration_sources(PathBuf::from("test/files"), "json".to_owned())
                .await
                .unwrap();

        // Then
        let mut settings_contents = String::new();
        sources
            .settings
            .read_to_string(&mut settings_contents)
            .await
            .unwrap();

        assert_eq!(settings_contents, expected_settings_contents);

        let mut caches_contents = String::new();
        sources
            .caches
            .read_to_string(&mut caches_contents)
            .await
            .unwrap();

        assert_eq!(caches_contents, expected_cache_contents);

        let mut routes: Vec<FsRouteEntry> = sources.routes.into_iter().collect();

        assert_eq!(routes.len(), 3);

        let mut routes_index_contents = String::new();
        routes[0]
            .1
            .read_to_string(&mut routes_index_contents)
            .await
            .unwrap();

        assert_eq!(routes_index_contents, expected_routes_index_contents);

        let mut routes_subroute_index_contents = String::new();
        routes[1]
            .1
            .read_to_string(&mut routes_subroute_index_contents)
            .await
            .unwrap();

        assert_eq!(
            routes_subroute_index_contents,
            expected_routes_subroute_index_contents
        );

        let mut routes_subroute_post_contents = String::new();
        routes[2]
            .1
            .read_to_string(&mut routes_subroute_post_contents)
            .await
            .unwrap();

        assert_eq!(
            routes_subroute_post_contents,
            expected_routes_subroute_post_contents
        );
    }
}
