use tokio::io::AsyncRead;

pub trait RouteEntry {
    type Path: AsRef<str>;
    type Source: AsyncRead;

    fn into_parts(self) -> (Self::Path, Self::Source);
}
