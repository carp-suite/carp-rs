pub struct ConfigurationSources<Settings, Caches, Routes> {
    pub routes: Routes,
    pub settings: Settings,
    pub caches: Caches,
}
