use std::future::{ready, Ready};

use serde::de;
use thiserror::Error;

use crate::models::{Caches, RouteDef, Settings};

use super::ConfigurationDeserializer;

#[derive(Error, Debug)]
#[error("Could not parse JSON configuration \"{0}\"")]
pub struct Error(&'static str, #[source] serde_json::Error);

trait SectionDef {
    const SECTION: &'static str;
}

impl SectionDef for Settings {
    const SECTION: &'static str = "settings";
}

impl SectionDef for Caches {
    const SECTION: &'static str = "caches";
}

impl SectionDef for RouteDef {
    const SECTION: &'static str = "route";
}

pub(self) fn json_configuration_deserializer<T>(source: &[u8]) -> Ready<Result<T, Error>>
where
    T: de::DeserializeOwned + SectionDef,
{
    ready(serde_json::from_slice(source).map_err(|err| Error(T::SECTION, err)))
}

pub fn json_deserializer() -> impl ConfigurationDeserializer<Error = Error> {
    (
        json_configuration_deserializer,
        json_configuration_deserializer,
        json_configuration_deserializer,
    )
}

#[cfg(test)]
mod tests {
    use futures::future::join_all;

    use crate::{
        deserializer::{json::json_deserializer, ConfigurationDeserializer},
        models::{CachePluginEntry, RoutePluginEntry, Routes},
    };
    use std::collections::HashMap;

    use crate::models::{Cache, CacheUsage, HttpMethod, Plugin, Route, Settings, Value};

    #[tokio::test]
    async fn it_deserializes_valid_settings_json() {
        // Given
        let buffer: &[u8] = br#"
            {
                 "host": "127.0.0.1",
                 "port": 8080,
                 "base_path": "/api",
                 "http_version": 2,
                 "cert": "/opt/ec/file",
                 "key": "/opt/ec/key",
                 "plugins": {
                    "My Name": {
                        "url": "some path",
                        "configuration": {
                            "my_string_prop": "value",
                            "my_number_prop": -3,
                            "my_array_prop": [
                                "some_value"
                            ],
                            "my_map_prop": {
                                "map_prop": "prop_value"
                            }
                        }
                    }
                }
            }
            "#;
        let deserializer = json_deserializer();

        // When
        let deserialization_result: Result<Settings, _> =
            deserializer.deserialize_settings(buffer).await;

        // Assert
        let result = deserialization_result.unwrap();
        assert_eq!(
            result,
            Settings {
                host: "127.0.0.1".to_owned(),
                port: 8080,
                base_path: "/api".to_owned(),
                http_version: 2,
                cert: Some("/opt/ec/file".to_owned()),
                key: Some("/opt/ec/key".to_owned()),
                plugins: HashMap::from_iter(vec![(
                    "My Name".to_owned(),
                    Plugin {
                        url: "some path".to_owned(),
                        configuration: Some(HashMap::from_iter(vec![
                            (
                                "my_string_prop".to_owned(),
                                Value::String("value".to_owned())
                            ),
                            ("my_number_prop".to_owned(), Value::Number(-3)),
                            (
                                "my_array_prop".to_owned(),
                                Value::Array(vec![Value::String("some_value".to_owned())])
                            ),
                            (
                                "my_map_prop".to_owned(),
                                Value::Map(HashMap::from_iter(vec![(
                                    "map_prop".to_owned(),
                                    Value::String("prop_value".to_owned())
                                )]))
                            )
                        ]))
                    }
                )])
            }
        );
    }

    #[tokio::test]
    async fn it_deserializes_valid_caches_json() {
        // Given
        let buffer: &[u8] = br#"
            [
                {
                    "name": "My Cache",
                    "ttl": 36400,
                    "dependencies": [
                        "Other other Cache"
                    ]
                },
                {
                    "name": "My Cache 2",
                    "ttl": 36401,
                    "dependencies": [
                        "Other Cache"
                    ],
                    "plugins": [
                        { "jwt-auth": "override" },
                        "metrics-collection"
                    ]
                }
            ]
            "#;
        let deserializer = json_deserializer();

        // When
        let deserialization_result: Result<Vec<Cache>, _> =
            deserializer.deserialize_caches(buffer).await;

        // Assert
        let result = deserialization_result.unwrap();
        assert_eq!(
            result,
            vec![
                Cache {
                    name: "My Cache".into(),
                    ttl: Some(36400),
                    dependencies: vec!["Other other Cache".into()],
                    plugins: None
                },
                Cache {
                    name: "My Cache 2".into(),
                    ttl: Some(36401),
                    dependencies: vec!["Other Cache".into()],
                    plugins: Some(vec![
                        CachePluginEntry::OverridePlugin(HashMap::from_iter(vec![(
                            "jwt-auth".to_owned(),
                            "override".to_owned()
                        )])),
                        CachePluginEntry::PluginName("metrics-collection".to_owned())
                    ])
                },
            ]
        );
    }

    #[tokio::test]
    async fn it_deserializes_valid_routes_jsons() {
        // Given
        let route_1: &[u8] = br#"
            {
                "GET": {
                    "host": "http://127.0.0.1:5500/route/1",
                    "cache": {
                        "name": "My Cache 1",
                        "route parameters": [
                            "id"
                        ],
                        "query parameters": [
                            "format"
                        ],
                        "headers": [
                            "Authorization"
                        ]
                    },
                    "invalidates": [
                        "Other Cache 1"
                    ]
                },
                "PUT": {
                    "host": "http://127.0.0.1:5500/route/1",
                    "cache": {
                        "name": "My Cache 1",
                        "route parameters": [
                            "id"
                        ],
                        "query parameters": [
                            "format"
                        ],
                        "headers": [
                            "Authorization"
                        ]
                    },
                    "invalidates": [
                        "Other Cache 1"
                    ],
                    "plugins": {
                        "request_start": [
                            "jwt-auth"
                        ],
                        "request_end": [
                            "metrics-collection"
                        ]
                    }
                }
            }
            "#;

        let route_2: &[u8] = br#"
            {
                "POST": {
                    "host": "http://127.0.0.1:6500/route/2",
                    "cache": {
                        "name": "My Cache 2",
                        "route parameters": [
                            "user"
                        ],
                        "query parameters": [
                            "name"
                        ],
                        "headers": [
                            "Content-Type"
                        ]
                    },
                    "invalidates": [
                        "Other Cache 2"
                    ]
                }
            }
            "#;

        let routes = &[route_1, route_2];
        let deserializer = json_deserializer();

        // When
        let deserialization_result: Result<Routes, _> = join_all(
            routes
                .iter()
                .copied()
                .map(|src| deserializer.deserialize_routes(src)),
        )
        .await
        .into_iter()
        .collect();

        // Assert
        let result = deserialization_result.unwrap();
        assert_eq!(
            result,
            vec![
                HashMap::from_iter(vec![
                    (
                        HttpMethod::GET,
                        Route {
                            host: "http://127.0.0.1:5500/route/1".into(),
                            cache: Some(CacheUsage {
                                name: "My Cache 1".into(),
                                route_parameters: vec!["id".into()],
                                query_parameters: vec!["format".into()],
                                headers: vec!["Authorization".into()]
                            }),
                            invalidates: vec!["Other Cache 1".into()],
                            plugins: None
                        }
                    ),
                    (
                        HttpMethod::PUT,
                        Route {
                            host: "http://127.0.0.1:5500/route/1".into(),
                            cache: Some(CacheUsage {
                                name: "My Cache 1".into(),
                                route_parameters: vec!["id".into()],
                                query_parameters: vec!["format".into()],
                                headers: vec!["Authorization".into()]
                            }),
                            invalidates: vec!["Other Cache 1".into()],
                            plugins: Some(RoutePluginEntry {
                                request_start: vec!["jwt-auth".to_owned(),],
                                request_end: vec!["metrics-collection".to_owned()],
                            })
                        }
                    )
                ]),
                HashMap::from_iter(vec![(
                    HttpMethod::POST,
                    Route {
                        host: "http://127.0.0.1:6500/route/2".into(),
                        cache: Some(CacheUsage {
                            name: "My Cache 2".into(),
                            route_parameters: vec!["user".into()],
                            query_parameters: vec!["name".into()],
                            headers: vec!["Content-Type".into()]
                        }),
                        invalidates: vec!["Other Cache 2".into()],
                        plugins: None
                    }
                )])
            ]
        );
    }
}
