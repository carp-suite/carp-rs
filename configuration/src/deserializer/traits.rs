// use std::collections::HashMap;

// use crate::configuration::{deserializer::ConfigurationDeserializer, models::{Settings, Caches, HttpMethod, Route}};

// pub trait SettingsConfigurationDeserializer<'a>: ConfigurationDeserializer<Settings<'a>>
// {
//     fn deserialize_settings(&self, buf: &[u8]) -> <Self as ConfigurationDeserializer<Settings<'a>>>::Future {
//         self.deserialize(buf)
//     }
// }

// impl<'a, T> SettingsConfigurationDeserializer<'a> for T
// where T: ConfigurationDeserializer<Settings<'a>>  {}

// pub trait CachesConfigurationDeserializer<'a>: ConfigurationDeserializer<Caches<'a>>
// {
//     fn deserialize_caches(&self, buf: &[u8]) -> <Self as ConfigurationDeserializer<Caches<'a>>>::Future {
//         self.deserialize(buf)
//     }
// }

// impl<'a, T> CachesConfigurationDeserializer<'a> for T
// where T: ConfigurationDeserializer<Caches<'a>>  {}

// pub trait RoutesConfigurationDeserializer<'a>: ConfigurationDeserializer<HashMap<HttpMethod, Route<'a>>>
// {
//     fn deserialize_routes(&self, buf: &[u8]) -> <Self as ConfigurationDeserializer<HashMap<HttpMethod, Route<'a>>>>::Future {
//         self.deserialize(buf)
//     }
// }

// impl<'a, T> RoutesConfigurationDeserializer<'a> for T
// where T: ConfigurationDeserializer<HashMap<HttpMethod, Route<'a>>>  {}
