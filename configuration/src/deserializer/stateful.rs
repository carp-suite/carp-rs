use futures::Future;

use crate::models::{Caches, RouteDef, Settings};

use super::ConfigurationDeserializer;

impl<T, E, F1, F2, F3, Fut1, Fut2, Fut3> ConfigurationDeserializer for (T, F1, F2, F3)
where
    F1: for<'a> Fn(&'a [u8], &'a T) -> Fut1,
    F2: for<'a> Fn(&'a [u8], &'a T) -> Fut2,
    F3: for<'a> Fn(&'a [u8], &'a T) -> Fut3,
    Fut1: Future<Output = Result<Settings, E>>,
    Fut2: Future<Output = Result<Caches, E>>,
    Fut3: Future<Output = Result<RouteDef, E>>,
    E: std::error::Error,
{
    type Error = E;
    type SettingsFuture = Fut1;
    type CachesFuture = Fut2;
    type RoutesFuture = Fut3;

    fn deserialize_settings<'a, R>(&'a self, reader: &'a R) -> Self::SettingsFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.1(reader.as_ref(), &self.0)
    }

    fn deserialize_caches<'a, R>(&'a self, reader: &'a R) -> Self::CachesFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.2(reader.as_ref(), &self.0)
    }

    fn deserialize_routes<'a, R>(&'a self, reader: &'a R) -> Self::RoutesFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.3(reader.as_ref(), &self.0)
    }
}
