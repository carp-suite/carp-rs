use std::future::Future;

use super::models::{Caches, RouteDef, Settings};

pub mod json;
pub mod stateful;
pub mod traits;
pub mod yaml;

pub trait ConfigurationDeserializer {
    type Error: std::error::Error;
    type SettingsFuture: Future<Output = Result<Settings, Self::Error>>;
    type CachesFuture: Future<Output = Result<Caches, Self::Error>>;
    type RoutesFuture: Future<Output = Result<RouteDef, Self::Error>>;

    fn deserialize_settings<'a, R>(&'a self, reader: &'a R) -> Self::SettingsFuture
    where
        R: AsRef<[u8]> + ?Sized;

    fn deserialize_caches<'a, R>(&'a self, reader: &'a R) -> Self::CachesFuture
    where
        R: AsRef<[u8]> + ?Sized;

    fn deserialize_routes<'a, R>(&'a self, reader: &'a R) -> Self::RoutesFuture
    where
        R: AsRef<[u8]> + ?Sized;
}

impl<E, F1, F2, F3, Fut1, Fut2, Fut3> ConfigurationDeserializer for (F1, F2, F3)
where
    F1: for<'a> Fn(&'a [u8]) -> Fut1,
    F2: for<'a> Fn(&'a [u8]) -> Fut2,
    F3: for<'a> Fn(&'a [u8]) -> Fut3,
    Fut1: Future<Output = Result<Settings, E>>,
    Fut2: Future<Output = Result<Caches, E>>,
    Fut3: Future<Output = Result<RouteDef, E>>,
    E: std::error::Error,
{
    type Error = E;
    type SettingsFuture = Fut1;
    type CachesFuture = Fut2;
    type RoutesFuture = Fut3;

    fn deserialize_settings<'a, R>(&'a self, reader: &'a R) -> Self::SettingsFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.0(reader.as_ref())
    }

    fn deserialize_caches<'a, R>(&'a self, reader: &'a R) -> Self::CachesFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.1(reader.as_ref())
    }

    fn deserialize_routes<'a, R>(&'a self, reader: &'a R) -> Self::RoutesFuture
    where
        R: AsRef<[u8]> + ?Sized,
    {
        self.2(reader.as_ref())
    }
}
