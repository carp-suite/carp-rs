use thiserror::Error;

use models::settings::{
    error::SettingsError, http_version::error::HttpVersionNumberError, plugin::error::PluginError,
};

#[derive(Error, Debug)]
pub enum ConfigurationError<D, S, I>
where
    S: std::error::Error,
    D: std::error::Error,
    I: std::error::Error,
{
    #[error(transparent)]
    SourcesError(S),
    #[error(transparent)]
    DeserializerError(D),
    #[error(transparent)]
    InputsError(I),
    #[error(transparent)]
    UnhandledError(#[from] Box<dyn std::error::Error + Sync + Send>),
    #[error(transparent)]
    SettingsConstructionError(#[from] SettingsError),
    #[error(transparent)]
    HttpVersionParsingError(#[from] HttpVersionNumberError),
    #[error("Could not parse plugin #{0}")]
    PluginConstructionError(usize, #[source] PluginError),
    #[error("Could not parse URL #{0}")]
    UrlParseError(String, #[source] url::ParseError),
}
