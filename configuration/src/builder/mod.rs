use derive_builder::Builder;
use futures::{future::join_all, Future};
use std::{error::Error, path::PathBuf};
use tokio::io::AsyncReadExt;

use models::{
    cache,
    route::{self, cache_usage::CacheUsage, http_method},
    settings::{self, http_version::HttpVersion, plugin},
};

use crate::models::CachePluginEntry;

use self::error::ConfigurationError;

use super::{
    deserializer::ConfigurationDeserializer,
    inputs::{format::Format, source::Source, ConfigurationInputs},
    models::{Caches, Settings},
    source::{
        configuration_sources::ConfigurationSources, route_entry::RouteEntry, ConfigurationSource,
    },
};

pub mod error;

#[derive(Builder)]
#[builder(pattern = "owned")]
pub struct Configuration<FS, FD, I> {
    source: FS,
    deserializer: FD,
    inputs: I,
}

impl<FS, S, FD, DE, D, I> Configuration<FS, FD, I>
where
    FS: FnOnce(Source) -> S,
    S: ConfigurationSource,
    FD: FnOnce(Format) -> D,
    DE: Error,
    D: ConfigurationDeserializer<Error = DE>,
    <S::RoutesSources as IntoIterator>::Item: RouteEntry,
    <<S::RoutesSources as IntoIterator>::Item as RouteEntry>::Source: Unpin,
    <<S::RoutesSources as IntoIterator>::Item as RouteEntry>::Path: Unpin,
    S::SettingsSource: Unpin,
    S::CachesSource: Unpin,
{
    pub async fn get_models_deserializer_futures<II>(
        self,
        inputs: II,
    ) -> Result<
        (
            impl Future<Output = Result<settings::Settings, ConfigurationError<DE, S::Error, I::Error>>>,
            impl Future<Output = Result<Vec<cache::Cache>, ConfigurationError<DE, S::Error, I::Error>>>,
            impl Future<Output = Result<Vec<route::Route>, ConfigurationError<DE, S::Error, I::Error>>>,
        ),
        ConfigurationError<DE, S::Error, I::Error>,
    >
    where
        I: ConfigurationInputs<II>,
    {
        let (format, source) = self
            .inputs
            .get_configuration_inputs(inputs)
            .await
            .map_err(ConfigurationError::InputsError)?;

        let deserializer = (self.deserializer)(format);
        let sources_factory = (self.source)(source);

        let mut sources: ConfigurationSources<
            S::SettingsSource,
            S::CachesSource,
            S::RoutesSources,
        > = sources_factory
            .get_sources()
            .await
            .map_err(ConfigurationError::SourcesError)?;

        let mut settings_buffer = vec![];
        sources
            .settings
            .read_to_end(&mut settings_buffer)
            .await
            .map_err(|err| ConfigurationError::UnhandledError(Box::new(err)))?;
        let settings_deserialization_future =
            deserializer.deserialize_settings(&mut settings_buffer);

        let settings_future = async move {
            let Settings {
                host,
                port,
                base_path,
                http_version,
                cert,
                key,
                plugins,
            } = settings_deserialization_future
                .await
                .map_err(ConfigurationError::DeserializerError)?;
            let plugins: Vec<plugin::Plugin> = plugins
                .into_iter()
                .enumerate()
                .map(|(i, (plugin_name, plugin_settings))| {
                    plugin::Plugin::try_from((
                        plugin_settings.url.as_str(),
                        plugin_name,
                        plugin_settings.configuration,
                    ))
                    .map_err(|e| ConfigurationError::PluginConstructionError(i, e))
                })
                .collect::<Result<Vec<plugin::Plugin>, ConfigurationError<_, _, _>>>()?;
            let proto = cert.as_ref().map(|_| "https").unwrap_or("http");
            let base_url = format!(
                "{}://{}:{}/{}",
                proto,
                host,
                port,
                base_path.trim_start_matches('/')
            );
            let version = HttpVersion::try_from(http_version as usize)
                .map_err(ConfigurationError::HttpVersionParsingError)?;
            let key: Option<PathBuf> = key.map(Into::into);
            let cert: Option<PathBuf> = cert.map(Into::into);
            let settings =
                settings::Settings::try_from((base_url.as_str(), version, cert, key, plugins))?;

            Ok(settings)
        };

        let mut caches_buffer = Vec::new();

        sources
            .caches
            .read_to_end(&mut caches_buffer)
            .await
            .map_err(|err| ConfigurationError::UnhandledError(Box::new(err)))?;

        let caches_deserializer_future = deserializer.deserialize_caches(&caches_buffer);

        let caches_future = async move {
            let deserialized_caches: Caches = caches_deserializer_future
                .await
                .map_err(ConfigurationError::DeserializerError)?;
            let caches = deserialized_caches
                .into_iter()
                .map(|cache| {
                    let (override_plugins, plugins): (Vec<Vec<String>>, Vec<Vec<String>>) = cache
                        .plugins
                        .into_iter()
                        .flat_map(|plugins| {
                            plugins.into_iter().map(|entry| match entry {
                                CachePluginEntry::OverridePlugin(names) => {
                                    (vec![], names.keys().cloned().collect())
                                }
                                CachePluginEntry::PluginName(name) => (vec![name], vec![]),
                            })
                        })
                        .unzip();
                    let (override_plugins, plugins) = (
                        override_plugins.into_iter().flatten().collect(),
                        plugins.into_iter().flatten().collect(),
                    );

                    models::cache::Cache::from((
                        cache.name,
                        cache.ttl,
                        cache.dependencies,
                        plugins,
                        override_plugins,
                    ))
                })
                .collect();

            Ok(caches)
        };

        let route_sources = join_all(sources.routes.into_iter().map(|entry| async move {
            let mut buffer = vec![];
            let (url, mut source) = entry.into_parts();

            source
                .read_to_end(&mut buffer)
                .await
                .map_err(|err| {
                    ConfigurationError::<DE, S::Error, I::Error>::UnhandledError(Box::new(err))
                })
                .map(|_| (url, buffer))
        }))
        .await
        .into_iter()
        .collect::<Result<Vec<(
            <<<S as ConfigurationSource>::RoutesSources as IntoIterator>::Item as RouteEntry>::Path,
            Vec<u8>,
        )>, ConfigurationError<_, _, _>>>()?;

        let routes_future = async move {
            let mut routes = Vec::new();

            for (url, source) in route_sources {
                let deserialized_routes = deserializer
                    .deserialize_routes(&source)
                    .await
                    .map_err(ConfigurationError::DeserializerError)?;

                for (method, route) in deserialized_routes {
                    let method: http_method::HttpMethod = method.into();
                    let cache_usage = route.cache.map(|cache| {
                        let route_parameters = cache.route_parameters;
                        let query_parameters = cache.query_parameters;
                        let headers = cache.headers;
                        let name = cache.name;

                        CacheUsage::from((name, route_parameters, query_parameters, headers))
                    });
                    let url = format!("{}/{}", route.host, url.as_ref().trim_start_matches('/'));
                    routes.push(
                        models::route::Route::try_from((
                            url.as_str(),
                            method,
                            cache_usage,
                            route.invalidates,
                            route.plugins.map(|plugin| {
                                models::route::plugin_request::PluginRequest::from((
                                    plugin.request_start,
                                    plugin.request_end,
                                ))
                            }),
                        ))
                        .map_err(|err| ConfigurationError::UrlParseError(url, err))?,
                    );
                }
            }

            Ok(routes)
        };

        Ok((settings_future, caches_future, routes_future))
    }
}

#[cfg(test)]
mod tests {

    use super::ConfigurationBuilder;
    use crate::{
        deserializer::json::json_deserializer,
        inputs::{fn_inputs, format::Format, source::Source},
        models::Value,
        source::{configuration_sources::ConfigurationSources, route_entry::RouteEntry},
    };
    use models::{
        cache::Cache,
        route::{cache_usage::CacheUsage, http_method::HttpMethod, Route},
        settings::{http_version::HttpVersion, plugin::Plugin, Settings},
    };
    use std::{collections::HashMap, convert::Infallible};

    #[tokio::test]
    async fn it_builds_configuration_correctly() {
        // Given
        struct MockRouteEntry<'a>(&'a [u8], &'a str);
        impl<'a> RouteEntry for MockRouteEntry<'a> {
            type Path = &'a str;
            type Source = &'a [u8];

            fn into_parts(self) -> (&'a str, &'a [u8]) {
                (self.1, self.0)
            }
        }
        let configuration = ConfigurationBuilder::default()
            .inputs(fn_inputs(|()| async {
                Ok::<_, Infallible>((
                    Format::JSON,
                    Source::Filesystem(".".to_owned().into(), "json"),
                ))
            }))
            .deserializer(|_| json_deserializer())
            .source(|_| {
                || async {
                    Ok::<_, Infallible>(ConfigurationSources {
                        settings: br#"
                    {
                        "host": "127.0.0.1",
                        "port": 8080,
                        "base_path": "/api",
                        "http_version": 2,
                        "plugins": {
                            "My Name": {
                                "url": "file:///some/path",
                                "configuration": {}
                            }
                        }
                    }
                    "# as &[u8],
                        caches: br#"
                    [
                        {
                            "name": "My Cache",
                            "ttl": 36400,
                            "dependencies": [
                                "Other other Cache"
                            ],
                            "plugins": [
                                { "redis": "override" },
                                "metrics-collection"
                            ]
                        }
                    ]
                    "# as &[u8],
                        routes: [MockRouteEntry(
                            br#"
                        {
                            "POST": {
                                "host": "http://127.0.0.1:6500/route/2",
                                "cache": {
                                    "name": "My Cache 2",
                                    "route parameters": [],
                                    "query parameters": [],
                                    "headers": []
                                },
                                "invalidates": [
                                    "Other Cache 2"
                                ]
                            }
                        }
                        "#,
                            "/base/path",
                        )],
                    })
                }
            })
            .build()
            .unwrap();

        // When
        let (settings, caches, routes) = configuration
            .get_models_deserializer_futures(())
            .await
            .unwrap();
        let settings = settings.await.unwrap();
        let caches = caches.await.unwrap();
        let routes = routes.await.unwrap();

        // Then
        let expected_plugin = Plugin::try_from((
            "file:///some/path",
            "My Name".to_owned(),
            Some(HashMap::<String, Value>::new()),
        ))
        .unwrap();
        let expected_settings = Settings::try_from((
            "http://127.0.0.1:8080/api",
            HttpVersion::V2,
            None,
            None,
            vec![expected_plugin],
        ))
        .unwrap();
        assert_eq!(settings, expected_settings);

        let expected_cache = Cache::from((
            "My Cache".into(),
            Some(36400),
            vec!["Other other Cache".into()],
            vec!["redis".into()],
            vec!["metrics-collection".into()],
        ));
        assert_eq!(caches, vec![expected_cache]);

        let expected_cache_usage =
            CacheUsage::from(("My Cache 2".to_owned(), vec![], vec![], vec![]));
        let expected_route = Route::try_from((
            "http://127.0.0.1:6500/route/2/base/path",
            HttpMethod::POST,
            Some(expected_cache_usage),
            vec!["Other Cache 2".to_owned()],
            None,
        ))
        .unwrap();

        assert_eq!(routes, vec![expected_route]);
    }
}
