# CARP

An extensible **reverse proxy** and **HTTP cache** following
[caching rules](#cache-rules) to guarantee business-related caching. 

**NOTE: CARP is still at a very early development stage, the configuration model, the extensibility process/contract and the usage will possibly change**

## Usage

Clone and build the binary file using `cargo`.

Run _CARP_ with configuration written in YAML.
```bash
carp --yaml ./path/to/ROOT/directory
```

Or if using docker, clone and build the `carp` image

```bash
docker run carp -v /path/to/ROOT/directory:/carp/config --json /carp/config 
```

Or run _CARP_ with configuration written in JSON.
```bash
carp --json ./path/to/ROOT/directory
```

Or if using docker, clone and build the `carp` image

```bash
docker run carp -v /path/to/ROOT/directory:/carp/config --json /carp/config 
```

## Features

- Configure routing and caching via JSON or YAML [see here](#configuration).
- Supports plugins using WASM (Wasmtime) and gRPC [see here](#extensibility).
- Supports HTTP/1.1 and HTTP/2.
- Supports HTTPS.
- Uses Memory Mapped Files to store cached responses.

## Caching rules

Caching rules can be defined as preconditions and/or postconditions that occur during an HTTP request that determine if:

- The response must be fetched from the origin server and subsequently cached.
- A cached response must be returned.

The current way of caching using the HTTP Protocol relies on the `Cache-Control` header, which in turns uses two properties to determine the validity of a cache: `max-age` and `expires`. Although useful for static content, content that varies following complex business rules might prove difficult to cache because it's validity **does not depend on time**, but on the **current state of the business process** being carried over.

This is where **caching rules** come in, based on memoization, they provide a way to determine when a response should be returned from the cache using rules that pertain to the business being automated and not on time. 

For example, let's say that we want to cache a resource obtainable through **route** `/resource`, if this resource never changes, using the traditional way of HTTP caching might prove easier. However, let's consider that we have two users, then using the traditional way proves ineffective, given that we first have to validate who is making the request before caching. Let's suppose we're using `Bearer Token` **Authorization Scheme**, then we could `memoize` the response based on the contents of the `Authorization` header, thus, we've now cached the response but not based on time, but in the parameters of the request, and in this case, to the business rule that each user gets a different resource on the same **route** `/resource`

In _CARP_ this definition was implemented using three mechanics:

- Extractors: Configured in the `cache` section of the [routing configuration](#routes), they indicate what parts of the request will be used for `memoizing`, which means if we get two different requests but with the same values extracted by these extractors, then the same response will be returned. Currently, there are three types of extractors:
    
    1. Query parameters.
    2. Headers.
    3. Route parameters.

- Invalidations: Configured on the `invalidates` section of the [routing configuration](#routes), this determines which **caches** must be invalidated (cleared of all responses they hold) when the current **route** is hit. Also, caches can define **dependencies** between each other via the `dependencies` section of the [caching configuration](#caches). For example, let's say that cache $A$ is dependent on cache $B$ (from now on, we'll say $A \rightarrow B$) then if cache B is invalidated, then cache A is invalidated as well.

- Extensibility: Both **routes** and **caches** can be configured to use plugins under the sections `plugins` of the [routes](#routes) and [caches](#caches). For routes, a plugin can:

    - Modify a request before reaching reaching the `extractors`.
    - Return an early response without.
    - Do nothing (good for side effects).

    For caches, a plugin can:
    
    - Return an early response (in bytes chunks)
    - Do nothing (good for side effects)


## Configuration

The configuration of _CARP_ is split in three sections:

- Settings: Used for configuring the web server like the binding port, binding IP Address, etc.
- Routes: Used for configuring which routes will _CARP_ listen to, as well as the caching mechanism the route will use.
- Caches: Used for configuring the caches that _CARP_ will use, as well as the dependencies of invalidation and other properties such as TTL. 

This sections must be under a same directory which we'll call `ROOT`.

### Settings

Located in `ROOT/settings.(json|yaml)`

| Property name | Example |           Description                                         |
|---------------|---------|---------------------------------------------------------------|
| `host`        |`0.0.0.0`| Specifies the IP Address                                      |
| `port`        |`80`     | Specifies the TCP Port                                        |
| `base_path`   |`/api`   | Adds the specified slug to all the configured routes URL      |
| `http_version`|`1`, `2` | Specifies the HTTP Version                                    |
| `cert`        | `/path/to/cert.pem` | Specifies the path where the SSL certificate is located (optional, used with `key` to enable HTTPS) |
| `key`         | `/path/to/key.pem` | Specifies the path where the SSL certificate's signing key is located (optional, used with `cert` to enable HTTPS) |
| `plugin.<plugin_name>.url` | `grpc://192.168.0.1`, `wasm://path/to/wasm/file` | Specifies the URL to connect to (if gRPC plugin) or the location of the WASM file (if WASM plugin) |
| `plugin.<plugin_name>.configuration` | plugin specific configuration object | Object specific to the plugin which is used as parameter for the `Configure` method |

### Routes

Located in `ROOT/routes/**/*.(json|yaml)`. The path of the file relative to the `ROOT/routes` directory will be used as the URL of the route i.e a route file located in `ROOT/routes/a/b/c.json` maps to route `a/b/c`. If the file name is `index`, then the last segment of the URL is the directory name, i.e a route file located in `ROOT/routes/a/b/index.json` maps to route `a/b`. If the file name starts with `:`, then it will be interpreted as a **route parameter**.

| Property name | Example |           Description                                         |
|---------------|---------|---------------------------------------------------------------|
| `host`        |`http://192.168.192.1:5000`| Specifies the remote server to which the request will be proxied. |
| `invalidates`        |`['Other cache']`     | Specifies caches that will be invalidated once this route is hit. |
| `cache.name` | `Cache 1` | Specifies the cache specified in [cache configuration](#caches) that will be used for caching the responses of this route |
| `cache.route parameters` | `['id', 'type']` | Specifies the route parameters that will be extracted from the HTTP request in order to memoize it |
| `cache.headers` | `['Accepts', 'Authorization']` | Specifies the headers that will be extracted from the HTTP request in order to memoize it |
| `cache.query parameters` | `['format', 'search']` | Specifies the query parameters that will be extracted from the HTTP request in order to memoize it |
| `plugins.request_start` | `['plugin A']` | Specifies the plugins that will have their method `Request Start` method called when this route is hit |
| `plugins.request_end` | `['plugin B']` | Specifies the plugins that will have their method `Request End` method called when this route is hit |


### Caches

Located in `ROOT/caches.(json|yaml)`

| Property name | Example   |           Description                                         |
|---------------|-----------|---------------------------------------------------------------|
| `name`        | `Cache 1` | Name of the cache, used for referencing purposes on routes or invalidations |
| `ttl`       | `16000`     | Time (in seconds) that the cached responses will live for, before being removed and fethed again from the origin server |
| `dependencies` | `['Cache 2', 'Cache 3']` | Caches that will invalidate this cache if they get invalidated |
| `plugins.<plugin_number>.<plugin_name>` | |  Specifies the plugins that do not override the storage mechanism that this cache will use | 
| `plugins.<plugin_number>.overrides` | `plugin 1` |  Specifies the plugins that override the storage mechanism that this cache will use | 


## Extensibility

TODO DOC.
