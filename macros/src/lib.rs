mod format;
use format::write_format::write_format;
use proc_macro::TokenStream;

#[proc_macro]
pub fn create_for(item: TokenStream) -> TokenStream {
    write_format(item.into()).into()
}
