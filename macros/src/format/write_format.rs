use super::create_for_info::CreateForInfo;
use proc_macro2::TokenStream as TokenStream2;
use quote::quote;
use syn::Path;

fn generate_type_parameters(len: usize, prefix: TokenStream2) -> Vec<TokenStream2> {
    (0..len)
        .into_iter()
        .map(|i| {
            let t = prefix.clone();
            let res = format!("{}_{}", t, i);

            res.parse().unwrap()
        })
        .collect()
}

fn generate_variables(len: usize) -> Vec<TokenStream2> {
    (0..len)
        .into_iter()
        .map(|i| {
            let ident = quote! { n };
            let res = format!("{}_{}", ident, i);

            res.parse().unwrap()
        })
        .collect()
}

fn generate_parameters(len: usize, types: &Vec<TokenStream2>) -> Vec<TokenStream2> {
    let mut first_parameter = vec![quote! { format: configuration::inputs::format::Format }];

    first_parameter.extend((0..len).into_iter().map(|i| {
        let ident = quote! { n };
        let ident = format!("{}_{}", ident, i);
        let ident: TokenStream2 = ident.parse().unwrap();

        let t = types.get(i).unwrap();
        quote! { #ident : #t }
    }));

    first_parameter
}

fn create_match_tree_call<'a>(
    mappings: impl Iterator<Item = &'a Path>,
    var_names: &Vec<TokenStream2>,
    function_call: TokenStream2,
    error_type: TokenStream2,
    error_prefix: TokenStream2,
    length: usize,
) -> TokenStream2 {
    if length == 1 {
        let path = mappings.into_iter().next().unwrap();
        let var = var_names.get(0).unwrap();
        return quote! {
            match format {
                #path => #var.#function_call
            }
        };
    }

    if length == 2 {
        let mut iter = mappings.into_iter();
        let path1 = iter.next().unwrap();
        let path2 = iter.next().unwrap();
        let var1 = var_names.get(0).unwrap();
        let var2 = var_names.get(1).unwrap();
        return quote! {
            match format {
                #path1 => #var1.#function_call.map_err(#error_type::Err_0).right_future(),
                #path2 => #var2.#function_call.map_err(#error_type::Err_1).left_future()
            }
        };
    }

    let arms: Vec<_> = mappings
        .into_iter()
        .enumerate()
        .scan(None, |ctx, (i, path)| {
            let var_name = var_names.get(i).unwrap();
            let error_branch = format!("{}_{}", error_prefix, i);

            if i == length - 1 {
                return Some(quote! {
                    #path => #var_name.#function_call.right_future().map_err(#error_type::#error_branch)
                });
            }

            match ctx {
                None => {
                    *ctx = Some(quote! { right_future() });
                    Some(quote! {
                        #path => #var_name.#function_call.map_err(#error_type::#error_branch).left_future()
                    })
                }
                Some(rights_call) => {
                    let rights_call = rights_call.clone();
                    *ctx = Some(quote! { #rights_call.right_future() });
                    Some(quote! {
                        #path => #var_name.#function_call.map_err(#error_type::#error_branch).#rights_call.left_future()
                    })
                }
            }

        })
        .collect();

    quote! {
        match format {
            #(#arms,)*
        }
    }
}

fn write_block(
    deserializer_inputs: impl Iterator<Item = TokenStream2>,
    fn_generics: Vec<TokenStream2>,
    fn_parameters: Vec<TokenStream2>,
    err_generics: Vec<TokenStream2>,
    err_type: TokenStream2,
    err_prefix: &str,
    settings_match: TokenStream2,
    caches_match: TokenStream2,
    routes_match: TokenStream2,
    length: usize,
) -> TokenStream2 {
    let vars = generate_variables(length);
    let err_variants = err_generics.iter().enumerate().map(|(i, t)| {
        let variant_name = format!("{}_{}", err_prefix, i);
        let variant_name: TokenStream2 = variant_name.parse().unwrap();
        quote! {
            #[error(transparent)]
            #variant_name(#t)
        }
    });
    let fn_generics_constraints = fn_generics
        .iter()
        .map(|t| quote! { #t: configuration::deserializer::ConfigurationDeserializer });
    let fn_generics_associated_error = fn_generics.iter().map(|t| quote! { #t::Error });
    quote! {
        {
            use futures::{TryFutureExt, FutureExt};

            #[derive(thiserror::Error, Debug)]
            pub enum #err_type<#(#err_generics,)*> {
                #(#err_variants,)*
            }

            pub fn generate_deserializer<#(#fn_generics,)*>(#(#fn_parameters,)*) -> impl configuration::deserializer::ConfigurationDeserializer<Error = #err_type<#(#fn_generics_associated_error,)*>>
                where #(#fn_generics_constraints,)*
            {
                ((#(#vars,)*),
                 move |buf: &[u8], (#(#vars,)*): &(#(#fn_generics,)*)| {
                    #settings_match
                 },
                 move |buf: &[u8], (#(#vars,)*): &(#(#fn_generics,)*)| {
                    #caches_match
                 },
                 move |buf: &[u8], (#(#vars,)*): &(#(#fn_generics,)*)| {
                    #routes_match
                 },
                )
            }

            |format| generate_deserializer(format, #(#deserializer_inputs,)*)
        }
    }
}
pub fn write_format(item: TokenStream2) -> TokenStream2 {
    let create_for_info: CreateForInfo = syn::parse2(item).expect("Unable to parse mappings");
    let l = create_for_info.map.len();
    let fn_generics = generate_type_parameters(l, quote! {T});
    let err_generics = generate_type_parameters(l, quote! {E});
    let parameters = generate_parameters(create_for_info.map.len(), &fn_generics);
    let var_names = generate_variables(l);
    let deserialize_settings_match = create_match_tree_call(
        create_for_info.map.iter().map(|(p, _)| p),
        &var_names,
        quote! { deserialize_settings(buf) },
        quote! { E },
        quote! { Err },
        l,
    );
    let deserialize_caches_match = create_match_tree_call(
        create_for_info.map.iter().map(|(p, _)| p),
        &var_names,
        quote! { deserialize_caches(buf) },
        quote! { E },
        quote! { Err },
        l,
    );
    let deserialize_routes_match = create_match_tree_call(
        create_for_info.map.iter().map(|(p, _)| p),
        &var_names,
        quote! { deserialize_routes(buf) },
        quote! { E },
        quote! { Err },
        l,
    );
    let deserializer_inputs = create_for_info
        .map
        .iter()
        .map(|(_, input)| quote! { #input });

    write_block(
        deserializer_inputs,
        fn_generics,
        parameters,
        err_generics,
        quote! { E },
        "Err",
        deserialize_settings_match,
        deserialize_caches_match,
        deserialize_routes_match,
        l,
    )
    .into()
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn it_generates_code_successfuly_for_block() {
        // Given
        let input = quote! {
            Format::JSON => 1 + 1,
            Format::YAML => 2 + 2
        };
        let expected = quote! {
            {
                use futures::{TryFutureExt, FutureExt};

               #[derive(thiserror::Error, Debug)]
               pub enum E<E_0, E_1,>{
                   #[error(transparent)]
                   Err_0(E_0),
                   #[error(transparent)]
                   Err_1(E_1),
               }

               pub fn generate_deserializer<T_0, T_1,>(format: configuration::inputs::format::Format, n_0: T_0, n_1: T_1,) -> impl configuration::deserializer::ConfigurationDeserializer<Error = E<T_0::Error, T_1::Error, >>
                   where T_0: configuration::deserializer::ConfigurationDeserializer,
                         T_1: configuration::deserializer::ConfigurationDeserializer,
                {
                    ((n_0, n_1,),
                     move |buf: &[u8], (n_0, n_1,): &(T_0, T_1,)| {
                         match format {
                            Format::JSON => n_0
                                .deserialize_settings(buf)
                                .map_err(E::Err_0)
                                .right_future(),
                            Format::YAML => n_1
                                .deserialize_settings(buf)
                                .map_err(E::Err_1)
                                .left_future()
                         }
                     },
                     move |buf: &[u8], (n_0, n_1,): &(T_0, T_1,)| {
                         match format {
                            Format::JSON => n_0
                                .deserialize_caches(buf)
                                .map_err(E::Err_0)
                                .right_future(),
                            Format::YAML => n_1
                                .deserialize_caches(buf)
                                .map_err(E::Err_1)
                                .left_future()
                         }
                     },
                     move |buf: &[u8], (n_0, n_1,): &(T_0, T_1,)| {
                         match format {
                            Format::JSON => n_0
                                .deserialize_routes(buf)
                                .map_err(E::Err_0)
                                .right_future(),
                            Format::YAML => n_1
                                .deserialize_routes(buf)
                                .map_err(E::Err_1)
                                .left_future()
                         }
                     },
                    )
                }

               |format| generate_deserializer(format,1+1,2+2,)
            }
        };

        // When
        let ast = write_format(input);

        // Then
        assert_eq!(ast.to_string(), expected.to_string());
        // assert!(true);
    }
}
