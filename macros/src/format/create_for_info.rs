use syn::{parse::Parse, Arm, Expr, Path};

#[derive(Debug)]
pub struct CreateForInfo {
    pub map: Vec<(Path, Box<Expr>)>,
}

impl Parse for CreateForInfo {
    fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
        let content = input;
        let mut map = Vec::new();
        while let Ok(arm) = content.parse::<Arm>() {
            let pattern = arm.pat;
            let path = match pattern {
                syn::Pat::Path(path) => path,
                _ => unreachable!(),
            };
            let body = arm.body;
            map.push((path.path, body));
        }
        Ok(CreateForInfo { map })
    }
}
#[cfg(test)]
mod tests {
    use quote::quote;

    use super::*;

    #[test]
    fn it_parses_create_format_info_successfuly() {
        // Given
        let tokens = quote! {
            Format::JSON => 1 + 1,
            Format::YAML => 2 + 2
        };
        let expr1 = quote!(1 + 1);
        let expr2 = quote!(2 + 2);
        let json_variant = quote!(Format::JSON);
        let yaml_variant = quote!(Format::YAML);

        // When
        let info: CreateForInfo = syn::parse2(tokens).unwrap();

        // Then
        assert_eq!(info.map.len(), 2);
        assert_eq!(
            info.map[0],
            (
                syn::parse2::<Path>(json_variant).unwrap(),
                Box::new(syn::parse2::<Expr>(expr1).unwrap())
            )
        );
        assert_eq!(
            info.map[1],
            (
                syn::parse2::<Path>(yaml_variant).unwrap(),
                Box::new(syn::parse2::<Expr>(expr2).unwrap())
            )
        );
    }
}
