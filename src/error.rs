use http::uri::Authority;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Provided target {0} in route {1} does not contain host")]
    MissingHost(String, String),
    #[error("Provided target {0} in route {1} does not contain port")]
    MissingPort(String, String),
    #[error("Failed parsing authority of target {0} in route {1}")]
    AuthorityParseError(
        String,
        String,
        #[source] <Authority as TryFrom<String>>::Error,
    ),
    #[error("Provided scheme in target {0} of route {1} is not supported. Please use \"http\" or \"https\"")]
    SchemeNotSupported(String, String),
    #[error("Plugin with name {0} used by route {1} was not defined")]
    RoutePluginNotFound(String, String),
    #[error("Plugin with name {0} used by cache {1} was not defined")]
    CachePluginNotFound(String, String),
}
