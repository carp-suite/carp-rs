#![feature(impl_trait_in_assoc_type)]

use bytes::Bytes;
use configuration::builder::ConfigurationBuilder;
use configuration::inputs::format::Format;
use configuration::{
    deserializer::{json::json_deserializer, yaml::yaml_deserializer},
    inputs::{clap::clap_configuration_inputs, source::Source},
    source::fs::fs_source,
};
use error::Error;
use futures::future::try_join_all;
use futures::TryFutureExt;
use http::uri::Authority;
use itertools::Itertools;
use logging::LogService;
use macros::create_for;
use models::cache::Cache;
use outputs::deserializer::output_deserializer;
use outputs::inputs::output_inputs;
use outputs::pipeline_driver::output_driver;
use outputs::sources::output_sources;
use pipeline::caching::registry::error::Error as CacheRegistryError;
use pipeline::caching::registry::CacheFactory;
use pipeline::caching::storage::ext::CacheStorageExt;
use pipeline::caching::CacheManager;
use pipeline::http_client::ext::HttpClientExt;
use pipeline_cache_manager::cache_manager::identification::rayon::RayonCacheIdentification;
use pipeline_cache_manager::cache_manager::identification::uuidv5::Uuidv5Identification;
use pipeline_cache_manager::cache_manager::storage::fmmap::FmmapBackendStore;
use pipeline_driver::hyper::either::Either;
use pipeline_driver::hyper::hyper_driver;
use pipeline_request_handler::http_client::hyper::HyperClientService;
use pipeline_request_handler::http_client::scheme::Scheme;
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use tokio::io::stderr;
use tokio::runtime::Handle;
use validation::validate_servers;

pub mod error;
pub mod logging;
pub mod outputs;
pub mod runtime;
pub mod validation;

pub async fn run_carp(handle: Handle) -> Result<(), anyhow::Error> {
    let configuration = ConfigurationBuilder::default()
        .inputs(output_inputs(1, 2, clap_configuration_inputs()))
        .deserializer(create_for! {
            Format::JSON => json_deserializer(),
            Format::YAML => yaml_deserializer(),
        })
        .source(|source: Source| match source {
            Source::Filesystem(base_dir, format) => {
                output_sources(2, 2, fs_source(base_dir, format))
            }
        })
        .build()?;
    let (settings, caches, routes) = configuration
        .get_models_deserializer_futures(env::args())
        .await?;

    let (settings, caches, mut routes) = output_deserializer(settings, caches, routes).await?;
    let base_path = settings.base_path().to_owned();
    let complete_address = settings.complete_address().to_owned();

    validate_servers(&mut routes, &base_path).await?;

    // Instantiate plugin drivers.
    let plugin_drivers =
        try_join_all(settings.plugins().iter().map(|plugin| {
            pipeline_plugin_manager::driver::Driver::try_from_plugin(plugin.clone())
        }))
        .await?
        .into_iter()
        .map(Arc::new)
        .collect::<Vec<_>>();

    // Validate all plugins needed by the caches are present.
    caches
        .iter()
        .flat_map(|cache| {
            cache
                .override_plugins()
                .iter()
                .chain(cache.plugins().iter())
                .map(|pn| (cache.name(), pn))
        })
        .map(|(cache, plugin_name)| {
            plugin_drivers
                .iter()
                .find(|driver| driver.name() == plugin_name)
                .ok_or_else(|| Error::CachePluginNotFound(plugin_name.to_owned(), cache.to_owned()))
        })
        .collect::<Result<Vec<_>, Error>>()?;

    // Construct map with cache name and the override plugins they point to.
    let override_plugins_cache_map = Arc::new(
        caches
            .iter()
            .flat_map(|cache| {
                cache.override_plugins().iter().map(|plugin| {
                    (
                        cache.name().to_string(),
                        plugin_drivers
                            .iter()
                            .find(|driver| driver.name() == plugin)
                            .unwrap()
                            .clone(),
                    )
                })
            })
            .into_group_map()
            .into_iter()
            .map(|(cache, plugin_name)| (cache, Arc::<[_]>::from(plugin_name)))
            .collect::<HashMap<_, _>>(),
    );

    // Construct map with cache name and the normal plugins they point to.
    let plugins_cache_map = Arc::new(
        caches
            .iter()
            .flat_map(|cache| {
                cache.plugins().iter().map(|plugin| {
                    (
                        cache.name().to_string(),
                        plugin_drivers
                            .iter()
                            .find(|driver| driver.name() == plugin)
                            .unwrap()
                            .clone(),
                    )
                })
            })
            .into_group_map()
            .into_iter()
            .map(|(cache, plugin_name)| (cache, Arc::<[_]>::from(plugin_name)))
            .collect::<HashMap<_, _>>(),
    );

    let cache_registry = pipeline::caching::registry::make(
        caches
            .into_iter()
            .map(|cache| {
                let override_plugins_cache_map = Arc::clone(&override_plugins_cache_map);
                let plugins_cache_map = Arc::clone(&plugins_cache_map);
                CacheFactory {
                    cache,
                    factory: move |c: &Cache, id: &[u8]| {
                        let override_plugins = override_plugins_cache_map
                            .get(c.name())
                            .cloned()
                            .unwrap_or_else(|| Arc::new([]));
                        let plugins = plugins_cache_map
                            .get(c.name())
                            .cloned()
                            .unwrap_or_else(|| Arc::new([]));
                        let cache_name = c.name().to_string();
                        let id = Bytes::copy_from_slice(id);

                        FmmapBackendStore::try_new(c.ttl(), cache_name.clone(), id.clone()).map_ok(
                            move |inner_storage| {
                                if override_plugins.is_empty() && plugins.is_empty() {
                                    return inner_storage.left_storage();
                                }

                                pipeline_plugin_manager::cache_storage::PluginCacheStorage::new(
                                    id.to_vec().into(),
                                    cache_name.into(),
                                    override_plugins,
                                    plugins,
                                    inner_storage,
                                )
                                .right_storage()
                            },
                        )
                    },
                }
            })
            .collect(),
    )?;
    let route_handler = pipeline::request_handler::make(
        routes,
        handle,
        |route| {
            let scheme = Scheme::try_from(route.target()).map_err(|_| {
                Error::SchemeNotSupported(
                    route.target().to_string(),
                    route.target().path().to_string(),
                )
            })?;
            let authority = Authority::try_from(format!(
                "{host}:{port}",
                host = route.target().host_str().ok_or_else(|| Error::MissingHost(
                    route.target().as_str().to_owned(),
                    route.target().path().to_string()
                ))?,
                port = route
                    .target()
                    .port_or_known_default()
                    .ok_or_else(|| Error::MissingPort(
                        route.target().as_str().to_owned(),
                        route.target().path().to_string()
                    ))?
            ))
            .map_err(|err| {
                Error::AuthorityParseError(
                    route.target().host_str().unwrap().to_owned(),
                    route.target().path().to_owned(),
                    err,
                )
            })?;

            if let Some(plugins) = route.plugins() {
                let request_start_handlers = plugins
                    .request_start()
                    .iter()
                    .map(|plugin| {
                        plugin_drivers
                            .iter()
                            .cloned()
                            .find(|driver| driver.name() == plugin)
                            .ok_or_else(|| {
                                Error::RoutePluginNotFound(
                                    plugin.to_owned(),
                                    route.target().path().to_string(),
                                )
                            })
                    })
                    .collect::<Result<Vec<_>, Error>>()?;
                let request_end_handlers = plugins
                    .request_end()
                    .iter()
                    .map(|plugin| {
                        plugin_drivers
                            .iter()
                            .cloned()
                            .find(|driver| driver.name() == plugin)
                            .ok_or_else(|| {
                                Error::RoutePluginNotFound(
                                    plugin.to_owned(),
                                    route.target().path().to_string(),
                                )
                            })
                    })
                    .collect::<Result<Vec<_>, Error>>()?;

                if request_start_handlers.is_empty() && request_end_handlers.is_empty() {
                    return Ok::<_, Error>(
                        HyperClientService::new(scheme, authority).left_client(),
                    );
                }

                Ok::<_, Error>(
                    pipeline_plugin_manager::request_handler::PluginRequestHandler::new(
                        HyperClientService::new(scheme, authority),
                        request_start_handlers,
                        request_end_handlers,
                    )
                    .right_client(),
                )
            } else {
                Ok::<_, Error>(HyperClientService::new(scheme, authority).left_client())
            }
        },
        |route, cache_usage, invalidates| {
            let storages_to_invalidate = cache_registry
                .try_get_storage_managers_to_invalidate(invalidates.iter().map(|s| s.as_str()))?;

            if let Some(cache_usage) = cache_usage {
                let route_parameters = cache_usage.route_parameters().iter().cloned();
                let query_parameters = cache_usage.query_parameters().iter().cloned();
                let headers = cache_usage.headers().iter().cloned();
                let identification = Uuidv5Identification::new(
                    route.target().path().as_bytes(),
                    RayonCacheIdentification::new(route_parameters, query_parameters, headers),
                );
                let storage_manager =
                    cache_registry.try_get_storage_manager(cache_usage.cache())?;

                Ok::<_, CacheRegistryError>(CacheManager::new_with_identification(
                    identification,
                    storage_manager,
                    storages_to_invalidate,
                ))
            } else {
                Ok::<_, CacheRegistryError>(CacheManager::new(storages_to_invalidate))
            }
        },
    )?;
    let driver_handler = output_driver(
        hyper_driver(settings)?,
        move |either| {
            let addr = match either {
                Either::Left(addr) => addr.remote_addr(),
                Either::Right(addr) => {
                    let (io, _connection) = addr.get_ref();
                    io.remote_addr()
                }
            };

            LogService::new(addr, route_handler.clone())
        },
        &complete_address,
        stderr(),
    )
    .await?;
    driver_handler.wait().await?;
    Ok(())
}
