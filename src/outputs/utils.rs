use std::{
    borrow::{Borrow, Cow},
    fmt::Display,
    time::Duration,
};

use console::{style, Color};
use futures::{
    future::{select, Either},
    Future,
};
use indicatif::{ProgressBar, ProgressStyle};
use tokio::time::sleep;

const XCHAR: char = '✗';

async fn tick_forever(pb: &ProgressBar, each: Duration) {
    loop {
        sleep(each).await;
        pb.tick();
    }
}

pub async fn tick_while_waiting_and_update_progress<F, Fut, FM, M, A, E, ProgressBar>(
    progress_bar: ProgressBar,
    tick_duration: Duration,
    progress_step: usize,
    max_step: usize,
    future_factory: F,
    message_factory: FM,
) -> Fut::Output
where
    F: FnOnce() -> Fut,
    Fut: Future<Output = Result<A, E>>,
    FM: FnOnce(&Fut::Output) -> M,
    Cow<'static, str>: From<M>,
    M: Display,
    ProgressBar: Borrow<indicatif::ProgressBar>,
{
    let progress_bar = progress_bar.borrow();
    let tick_future = tick_forever(progress_bar, tick_duration);
    let future = future_factory();

    tokio::pin!(tick_future);
    tokio::pin!(future);

    match select(tick_future, future).await {
        Either::Left(_) => unreachable!(),
        Either::Right((output, _)) => {
            let message = message_factory(&output);

            match output {
                Ok(v) => {
                    let progress = format!("[{}/{}]", progress_step, max_step);
                    let style = ProgressStyle::with_template(&format!(
                        "{} [{{elapsed}}] {{msg}}",
                        style(progress).bold().dim().fg(Color::Color256(0xf0))
                    ))
                    .expect("Invalid style");
                    progress_bar.set_style(style);
                    progress_bar.finish_with_message(message);
                    Ok(v)
                }
                Err(err) => {
                    let progress = format!("[{}/{}]", XCHAR, max_step);
                    let err_style = ProgressStyle::with_template(&format!(
                        "{} {{msg}}",
                        style(progress).bold().red().bright()
                    ))
                    .expect("Invalid style");
                    progress_bar.set_style(err_style);
                    let message = style(message).bold().red().bright().to_string();
                    progress_bar.finish_with_message(message);
                    Err(err)
                }
            }
        }
    }
}
