use std::time::Duration;

use futures::{future::try_join3, Future};
use indicatif::{MultiProgress, ProgressBar};
use models::{cache::Cache, route::Route, settings::Settings};

use super::utils::tick_while_waiting_and_update_progress;

pub async fn output_deserializer<E, FS, FC, FR>(
    future_settings: FS,
    future_caches: FC,
    future_routes: FR,
) -> Result<(Settings, Vec<Cache>, Vec<Route>), E>
where
    FS: Future<Output = Result<Settings, E>>,
    FC: Future<Output = Result<Vec<Cache>, E>>,
    FR: Future<Output = Result<Vec<Route>, E>>,
{
    let mpb = MultiProgress::new();
    let spb = mpb.add(ProgressBar::new_spinner());
    let cpb = mpb.add(ProgressBar::new_spinner());
    let rpb = mpb.add(ProgressBar::new_spinner());

    spb.set_message("Deserializing settings...");

    let settings_future = tick_while_waiting_and_update_progress(
        &spb,
        Duration::from_millis(20),
        1,
        3,
        || async move { future_settings.await },
        |res| match res {
            Ok(_) => "Deserialized settings successfully!",
            Err(_) => {
                mpb.remove(&cpb);
                mpb.remove(&rpb);
                "Failed to deserialize settings!\n"
            }
        },
    );

    cpb.set_message("Deserializing caches...");

    let caches_future = tick_while_waiting_and_update_progress(
        &cpb,
        Duration::from_millis(20),
        2,
        3,
        || async move { future_caches.await },
        |res| match res {
            Ok(_) => "Deserialized caches successfully!",
            Err(_) => {
                mpb.remove(&spb);
                mpb.remove(&rpb);
                "Failed to deserialize caches!\n"
            }
        },
    );

    rpb.set_message("Deserializing routes...");

    let routes_future = tick_while_waiting_and_update_progress(
        &rpb,
        Duration::from_millis(20),
        3,
        3,
        || async move { future_routes.await },
        |res| match res {
            Ok(_) => "Deserialized routes successfully!\n",
            Err(_) => {
                mpb.remove(&spb);
                mpb.remove(&cpb);
                "Failed to deserialize caches!\n"
            }
        },
    );

    try_join3(settings_future, caches_future, routes_future).await
}
