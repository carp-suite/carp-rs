use anyhow;
use console::style;
use pipeline::driver::PipelineDriver;
use tokio::io::{AsyncWrite, AsyncWriteExt};

const UCHAR: char = '✓';

pub async fn output_driver<Driver, Return, MakeHandler, Writer>(
    driver: Driver,
    service: MakeHandler,
    address: &str,
    mut output: Writer,
) -> anyhow::Result<Return>
where
    Driver: PipelineDriver<MakeHandler, Return>,
    Driver::Error: Sync + Send + 'static,
    Writer: AsyncWrite + Unpin,
{
    let result = driver.start(service).await?;
    let format_addr = style(address).bold().blue();
    let uchar_format = style(UCHAR.to_string()).bold().green();
    output
        .write(format!("{uchar_format} Started server on address {format_addr}\n").as_bytes())
        .await?;
    Ok(result)
}
