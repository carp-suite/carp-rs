use std::time::Duration;

use configuration::source::{route_entry::RouteEntry, ConfigurationSource};
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle, TermLike};

use crate::outputs::utils::tick_while_waiting_and_update_progress;

pub fn output_sources_with_term<S, T>(
    step_number: usize,
    stages: usize,
    terminal: T,
    source: S,
) -> impl ConfigurationSource<
    SettingsSource = S::SettingsSource,
    CachesSource = S::CachesSource,
    RoutesSources = S::RoutesSources,
    Error = S::Error,
>
where
    T: TermLike + 'static,
    S: ConfigurationSource,
    <S::RoutesSources as IntoIterator>::Item: RouteEntry,
{
    let spinner_style = ProgressStyle::default_spinner();
    let progress_bar =
        ProgressBar::with_draw_target(None, ProgressDrawTarget::term_like(Box::new(terminal)))
            .with_style(spinner_style);
    move || {
        progress_bar.set_message("Obtaining sources...");

        async move {
            tick_while_waiting_and_update_progress(
                &progress_bar,
                Duration::from_millis(20),
                step_number,
                stages,
                || source.get_sources(),
                |res| match res {
                    Ok(_) => "Obtained configuration sources successfully!\n",
                    Err(_) => "Failed to obtain sources!\n",
                },
            )
            .await
        }
    }
}

pub fn output_sources<S>(
    step_number: usize,
    stages: usize,
    source: S,
) -> impl ConfigurationSource<
    SettingsSource = S::SettingsSource,
    CachesSource = S::CachesSource,
    RoutesSources = S::RoutesSources,
    Error = S::Error,
>
where
    S: ConfigurationSource,
    <S::RoutesSources as IntoIterator>::Item: RouteEntry,
{
    move || async move {
        let progress_bar = ProgressBar::new_spinner();
        progress_bar.set_message("Obtaining sources...");

        tick_while_waiting_and_update_progress(
            &progress_bar,
            Duration::from_millis(20),
            step_number,
            stages,
            || source.get_sources(),
            |res| match res {
                Ok(_) => "Obtained configuration sources successfuly!\n",
                Err(_) => "Failed to obtain sources!\n",
            },
        )
        .await
    }
}
