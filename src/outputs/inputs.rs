use std::time::Duration;

use configuration::inputs::{source::Source, ConfigurationInputs};
use console::style;
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle, TermLike};

use crate::outputs::utils::tick_while_waiting_and_update_progress;

pub fn output_inputs_with_term<T, A, I>(
    step_number: usize,
    stages: usize,
    terminal: T,
    inputs: I,
) -> impl ConfigurationInputs<A, Error = I::Error>
where
    T: TermLike + 'static,
    I: ConfigurationInputs<A>,
{
    let spinner_style = ProgressStyle::default_spinner();
    let progress_bar =
        ProgressBar::with_draw_target(None, ProgressDrawTarget::term_like(Box::new(terminal)))
            .with_style(spinner_style);

    move |i| async move {
        progress_bar.set_message("Obtaining inputs...");

        tick_while_waiting_and_update_progress(
            &progress_bar,
            Duration::from_millis(20),
            step_number,
            stages,
            || inputs.get_configuration_inputs(i),
            |res| match res {
                Ok((_, Source::Filesystem(base_path, f))) => format!(
                    "Instructed to use {} format and obtain files from {}",
                    style(f.to_uppercase()).bold().bright(),
                    style(base_path.display()).bold().bright()
                ),
                Err(_) => "Could not obtain inputs".to_owned(),
            },
        )
        .await
    }
}

pub fn output_inputs<A, I>(
    step_number: usize,
    stages: usize,
    inputs: I,
) -> impl ConfigurationInputs<A, Error = I::Error>
where
    I: ConfigurationInputs<A>,
{
    let progress_bar = ProgressBar::new_spinner();

    move |i| async move {
        progress_bar.set_message("Obtaining inputs...");

        tick_while_waiting_and_update_progress(
            &progress_bar,
            Duration::from_millis(20),
            step_number,
            stages,
            || inputs.get_configuration_inputs(i),
            |res| match res {
                Ok((_, Source::Filesystem(base_path, f))) => format!(
                    "Instructed to use {} format and obtain files from {}",
                    style(f.to_uppercase()).bold().bright(),
                    style(base_path.display()).bold().bright()
                ),
                Err(_) => "Could not obtain inputs".to_owned(),
            },
        )
        .await
    }
}
