use std::{error::Error, net::SocketAddr};

use flexstr::SharedStr;
use futures::Future;
use http::Request;
use intrinsics::unlikely;
use pipeline::request_handler::PipelineRequestHandler;
use tokio::time::Instant;

pub struct LogService<Inner> {
    client_addr: SocketAddr,
    inner: Inner,
}

impl<Inner> LogService<Inner> {
    pub fn new(client_addr: SocketAddr, inner: Inner) -> Self {
        LogService { client_addr, inner }
    }
}

impl<RequestBody, ResponseBody, InnerService> PipelineRequestHandler<RequestBody, ResponseBody>
    for LogService<InnerService>
where
    InnerService: PipelineRequestHandler<RequestBody, ResponseBody>,
    InnerService::Error: Error + Send + Sync + 'static,
    RequestBody: 'static,
    ResponseBody: 'static,
{
    type Error = InnerService::Error;
    type Future = impl Future<Output = <InnerService::Future as Future>::Output>;

    #[inline]
    fn handle(&mut self, req: Request<RequestBody>) -> Self::Future {
        let method = SharedStr::from_ref(req.method());
        let path = SharedStr::from_ref(req.uri().path());
        let now = Instant::now();
        let socket_addr = self.client_addr;
        let response_future = self.inner.handle(req);

        async move {
            let response_result = response_future.await;
            let elapsed = now.elapsed();
            if unlikely(response_result.is_err()) {
                let err = response_result.as_ref().err().unwrap();
                let error_string = anyhow::format_err!("{:#}", err);
                log::error!(
                    "{client_addr} - - {error_string} - - {elapsed}ms",
                    client_addr = socket_addr.ip(),
                    elapsed = elapsed.as_millis(),
                );
            } else {
                let response = response_result.as_ref().ok().unwrap();
                log::info!(
                    "{client_addr} - - {method} {path} - - {status} {elapsed}ms",
                    client_addr = socket_addr.ip(),
                    elapsed = elapsed.as_millis(),
                    status = response.status()
                );
            }

            response_result
        }
    }
}
