use std::time::Duration;

use anyhow::anyhow;
use console::style;
use futures::future::try_join_all;
use indicatif::{MultiProgress, ProgressBar};
use models::route::Route;
use regex::Regex;
use tokio::net::TcpStream;

use crate::outputs::utils::tick_while_waiting_and_update_progress;
const UCHAR: char = '✓';

async fn validate_connection(route: &Route, route_number: usize) -> Result<(), anyhow::Error> {
    let host = route
        .target()
        .host_str()
        .ok_or_else(|| anyhow!("Missing host in route #{route_number}"))?;
    let port = route
        .target()
        .port_or_known_default()
        .ok_or_else(|| anyhow!("Missing port in route #{route_number}"))?;
    TcpStream::connect(format!("{host}:{port}")).await?;
    Ok(())
}

pub async fn validate_servers(routes: &mut [Route], prefix: &str) -> Result<(), anyhow::Error> {
    let mp = MultiProgress::new();
    let regex = Regex::new(r#"/\w*\.\w+"#).unwrap();
    let len = routes.len();
    try_join_all(routes.iter_mut().enumerate().map(|(i, route)| {
        let pb = mp.add(ProgressBar::new_spinner());
        let url = route.target().clone();
        tick_while_waiting_and_update_progress(
            pb,
            Duration::from_millis(10),
            i + 1,
            len,
            move || validate_connection(route, i + 1),
            move |output| match output {
                Ok(_) => format!(
                    "Connected to {host}:{port}!",
                    host = url.host_str().unwrap(),
                    port = url.port_or_known_default().unwrap(),
                ),
                Err(err) => format!(
                    "Could not connect to {host}:{port}: {err}\n",
                    host = url.host_str().unwrap(),
                    port = url.port_or_known_default().unwrap(),
                    err = err
                ),
            },
        )
    }))
    .await?;

    println!(" ");

    for route in routes {
        let url = route.target().clone();
        let path = format!(
            "{}/{}",
            prefix.trim_end_matches("/"),
            url.path().trim_start_matches("/")
        );
        route
            .target_mut()
            .set_path(regex.replace(&path, "").as_ref());
        let url = route.target().clone();

        println!(
            "{format_check} ROUTE {method:?} {format_path} -> {host}:{port}{format_path}",
            method = route.http_method(),
            format_check = style(UCHAR.to_string()).bold().green(),
            format_path = style(url.path()).bright().bold(),
            host = style(url.host_str().unwrap()).bright().bold(),
            port = style(url.port_or_known_default().unwrap()).bold().bright()
        );
    }

    println!("");

    Ok(())
}
