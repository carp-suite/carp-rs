use tokio::runtime::{Builder, Handle, Runtime};

pub trait ApplicationRuntime {
    fn driver_handle(&self) -> Handle;
    fn request_handler_handle(&self) -> Handle;
}

pub struct MultipleRuntime(Runtime, Runtime);

impl MultipleRuntime {
    pub fn try_new() -> anyhow::Result<Self> {
        let cpu_count = num_cpus::get();
        let base_runtime = Builder::new_multi_thread()
            .enable_all()
            .worker_threads(cpu_count / 4)
            .global_queue_interval(51)
            .event_interval(21)
            .build()?;
        let route_handling_runtime = Builder::new_multi_thread()
            .enable_all()
            .worker_threads(3 * cpu_count / 4)
            .event_interval(21)
            .global_queue_interval(51)
            .build()?;

        Ok(MultipleRuntime(base_runtime, route_handling_runtime))
    }
}

impl ApplicationRuntime for MultipleRuntime {
    fn driver_handle(&self) -> Handle {
        self.0.handle().clone()
    }

    fn request_handler_handle(&self) -> Handle {
        self.1.handle().clone()
    }
}
