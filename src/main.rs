use carp::{
    run_carp,
    runtime::{ApplicationRuntime, MultipleRuntime},
};
use jemallocator::Jemalloc;
use tracing_subscriber::EnvFilter;

#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

fn main() -> anyhow::Result<()> {
    let (writer, _guard) = tracing_appender::non_blocking(std::io::stdout());
    tracing_subscriber::fmt()
        .with_writer(writer)
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    let runtime = MultipleRuntime::try_new()?;
    runtime
        .driver_handle()
        .block_on(run_carp(runtime.request_handler_handle()))?;
    Ok(())
}
