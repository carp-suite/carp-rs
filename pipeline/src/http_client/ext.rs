use super::HttpClient;
use either::Either;
use futures::{Future, FutureExt, TryFutureExt};
use http::{Request, Response};

pub trait HttpClientExt<ReqBody, ResBody>: HttpClient<ReqBody, ResBody> {
    fn right_client<L>(self) -> Either<L, Self>
    where
        Self: Sized,
        L: HttpClient<ReqBody, ResBody>,
    {
        Either::Right::<L, _>(self)
    }

    fn left_client<R>(self) -> Either<Self, R>
    where
        Self: Sized,
        R: HttpClient<ReqBody, ResBody>,
    {
        Either::Left::<_, R>(self)
    }
}

impl<Client, ReqBody, ResBody> HttpClientExt<ReqBody, ResBody> for Client where
    Client: HttpClient<ReqBody, ResBody>
{
}

impl<RightClient, LeftClient, ReqBody, ResBody> HttpClient<ReqBody, ResBody>
    for Either<LeftClient, RightClient>
where
    RightClient: HttpClient<ReqBody, ResBody>,
    LeftClient: HttpClient<ReqBody, ResBody>,
{
    type Error = Either<LeftClient::Error, RightClient::Error>;
    type BeforeRequestFuture =
        impl Future<Output = Result<Either<Request<ReqBody>, Response<ResBody>>, Self::Error>>;
    type AfterRequestFuture = impl Future<Output = Result<Response<ResBody>, Self::Error>>;
    type Future = impl Future<Output = Result<Response<ResBody>, Self::Error>>;

    #[inline]
    fn request(&self, request: Request<ReqBody>) -> Self::Future {
        match self {
            Either::Left(client) => client.request(request).map_err(Either::Left).left_future(),
            Either::Right(client) => client
                .request(request)
                .map_err(Either::Right)
                .right_future(),
        }
    }

    #[inline]
    fn before_request(&self, request: Request<ReqBody>) -> Self::BeforeRequestFuture {
        match self {
            Either::Left(client) => client
                .before_request(request)
                .map_err(Either::Left)
                .left_future(),
            Either::Right(client) => client
                .before_request(request)
                .map_err(Either::Right)
                .right_future(),
        }
    }

    #[inline]
    fn after_request(&self, response: Response<ResBody>) -> Self::AfterRequestFuture {
        match self {
            Either::Left(client) => client
                .after_request(response)
                .map_err(Either::Left)
                .left_future(),
            Either::Right(client) => client
                .after_request(response)
                .map_err(Either::Right)
                .right_future(),
        }
    }
}
