use either::Either;
use futures::Future;
use http::{Request, Response};

pub mod ext;

pub trait HttpClient<ReqBody, ResBody> {
    type Error;
    type Future: Future<Output = Result<Response<ResBody>, Self::Error>>;
    type BeforeRequestFuture: Future<
        Output = Result<Either<Request<ReqBody>, Response<ResBody>>, Self::Error>,
    >;
    type AfterRequestFuture: Future<Output = Result<Response<ResBody>, Self::Error>>;

    fn before_request(&self, req: Request<ReqBody>) -> Self::BeforeRequestFuture;
    fn request(&self, request: Request<ReqBody>) -> Self::Future;
    fn after_request(&self, res: Response<ResBody>) -> Self::AfterRequestFuture;
}
