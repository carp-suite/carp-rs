use http::Uri;
use std::sync::Arc;
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct RequestContext(Arc<Uuid>, Arc<Uri>);

impl RequestContext {
    pub fn new(id: Arc<Uuid>, uri: Uri) -> Self {
        Self(id, Arc::new(uri))
    }

    pub fn id(&self) -> Arc<Uuid> {
        Arc::clone(&self.0)
    }
    pub fn uri(&self) -> Arc<Uri> {
        Arc::clone(&self.1)
    }
}
