use std::{error::Error as StdError, future::Future};
pub trait PipelineDriver<MakePipelineRequestHandler, Return> {
    type RequestBody;
    type Error: StdError;
    type Future: Future<Output = Result<Return, Self::Error>>;

    fn start(self, maker: MakePipelineRequestHandler) -> Self::Future;
}
