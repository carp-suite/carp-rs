use http::{Method, Request, Response};
use matchit::Router;
use models::route::{cache_usage::CacheUsage, Route};
use std::collections::HashMap;
use std::future::Future;
use std::{error::Error as StdError, sync::Arc};
use tokio::runtime::Handle;

use crate::request_handler::http_method::is_actionable_method;
use crate::{
    caching::{storage::CacheStorage, CacheManager},
    http_client::HttpClient,
};

use self::http_method::from_http_method;
use self::{
    error::SetupError,
    handler::{RequestHandler, RoutedClient},
};

pub mod error;
pub mod handler;
pub mod http_method;
pub mod matchit_params;
pub mod tower;

pub trait PipelineRequestHandler<RequestBody, ResponseBody> {
    type Error;
    type Future: Future<Output = Result<Response<ResponseBody>, Self::Error>>;

    fn handle(&mut self, request: Request<RequestBody>) -> Self::Future;
}

pub fn make<
    ClientMaker,
    CacheMaker,
    ClientError,
    CacheError,
    Client,
    Id,
    Identification,
    Factory,
    Storage,
    ReqBody,
    ResBody,
>(
    routes: Vec<Route>,
    handle: Handle,
    client_maker: ClientMaker,
    cache_maker: CacheMaker,
) -> Result<
    RequestHandler<Client, Id, Identification, Factory, Storage>,
    SetupError<ClientError, CacheError>,
>
where
    ClientMaker: for<'a> Fn(&'a Route) -> Result<Client, ClientError>,
    CacheMaker:
        for<'a> Fn(
            &'a Route,
            Option<&'a CacheUsage>,
            &'a [String],
        )
            -> Result<CacheManager<Id, Identification, Factory, Storage>, CacheError>,
    Client: HttpClient<ReqBody, ResBody> + Send + Sync + 'static,
    Client::Error: Send + Sync + StdError + 'static,
    Client::Future: Send,
    Storage: CacheStorage<ResBody, Client::Future>,
    Storage::Error: StdError,
{
    let mut router: Router<
        HashMap<Method, RoutedClient<Client, Id, Identification, Factory, Storage>>,
    > = Router::new();

    for route in routes {
        let url = route.target();
        let fragments_url = format!("{}/*fragments", url.path());
        let client = Arc::new(
            client_maker(&route)
                .map_err(|err| SetupError::ClientCreationError(url.path().to_owned(), err))?,
        );

        if router.at(url.path()).ok().is_some() {
            // Update main path allowed methods
            {
                let client_map = router.at_mut(url.path()).unwrap().value;
                for method in from_http_method(route.http_method()) {
                    let cache_manager = cache_maker(
                        &route,
                        route.cache_usage(),
                        route.invalidates(),
                    )
                    .map_err(|err| {
                        if let Some(cache_usage) = route.cache_usage() {
                            SetupError::CacheCreationError(
                                url.path().to_owned(),
                                cache_usage.cache().to_owned(),
                                err,
                            )
                        } else {
                            SetupError::InvalidationsPreparationError(url.path().to_owned(), err)
                        }
                    })?;

                    client_map.insert(
                        method.clone(),
                        RoutedClient {
                            client: Arc::clone(&client),
                            cache_manager: if is_actionable_method(&method) {
                                Some(cache_manager)
                            } else {
                                None
                            },
                        },
                    );
                }
            }

            // Update fragments path allowed methods
            {
                let client_map = router.at_mut(fragments_url.as_str()).unwrap().value;

                for method in from_http_method(route.http_method()) {
                    let cache_manager = cache_maker(
                        &route,
                        route.cache_usage(),
                        route.invalidates(),
                    )
                    .map_err(|err| {
                        if let Some(cache_usage) = route.cache_usage() {
                            SetupError::CacheCreationError(
                                url.path().to_owned(),
                                cache_usage.cache().to_owned(),
                                err,
                            )
                        } else {
                            SetupError::InvalidationsPreparationError(url.path().to_owned(), err)
                        }
                    })?;

                    client_map.insert(
                        method.clone(),
                        RoutedClient {
                            client: Arc::clone(&client),
                            cache_manager: if is_actionable_method(&method) {
                                Some(cache_manager)
                            } else {
                                None
                            },
                        },
                    );
                }
            }
        } else {
            let url = url.path();
            let methods = from_http_method(route.http_method());

            let routed_clients = methods
                .iter()
                .map(|method| {
                    let cache_manager =
                        cache_maker(&route, route.cache_usage(), route.invalidates())
                            .map(|cache_manager| {
                                if is_actionable_method(&method) {
                                    Some(cache_manager)
                                } else {
                                    None
                                }
                            })
                            .map_err(|err| {
                                if let Some(cache_usage) = route.cache_usage() {
                                    SetupError::CacheCreationError(
                                        url.to_owned(),
                                        cache_usage.cache().to_owned(),
                                        err,
                                    )
                                } else {
                                    SetupError::InvalidationsPreparationError(url.to_owned(), err)
                                }
                            })?;

                    Ok((
                        method.clone(),
                        RoutedClient {
                            client: client.clone(),
                            cache_manager,
                        },
                    ))
                })
                .collect::<Result<_, _>>()?;

            router
                .insert(url, routed_clients)
                .expect("Should not have any insertion errors");

            let routed_clients = methods
                .iter()
                .map(|method| {
                    let cache_manager =
                        cache_maker(&route, route.cache_usage(), route.invalidates())
                            .map(|cache_manager| {
                                if is_actionable_method(&method) {
                                    Some(cache_manager)
                                } else {
                                    None
                                }
                            })
                            .map_err(|err| {
                                if let Some(cache_usage) = route.cache_usage() {
                                    SetupError::CacheCreationError(
                                        url.to_owned(),
                                        cache_usage.cache().to_owned(),
                                        err,
                                    )
                                } else {
                                    SetupError::InvalidationsPreparationError(url.to_owned(), err)
                                }
                            })?;

                    Ok((
                        method.clone(),
                        RoutedClient {
                            client: client.clone(),
                            cache_manager,
                        },
                    ))
                })
                .collect::<Result<_, _>>()?;
            router
                .insert(&fragments_url, routed_clients)
                .expect("Should not have any insertion errors");
        }
    }

    Ok(RequestHandler::new(router, handle))
}
