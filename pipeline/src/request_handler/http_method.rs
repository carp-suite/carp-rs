use http::Method;
use models::route::http_method::HttpMethod;

pub fn from_http_method(method: HttpMethod) -> Vec<Method> {
    match method {
        HttpMethod::GET => vec![Method::GET, Method::OPTIONS],
        HttpMethod::POST => vec![Method::POST, Method::OPTIONS],
        HttpMethod::PUT => vec![Method::PUT, Method::OPTIONS],
        HttpMethod::PATCH => vec![Method::PATCH, Method::OPTIONS],
        HttpMethod::DELETE => vec![Method::DELETE, Method::OPTIONS],
        HttpMethod::WILDCARD => vec![
            Method::GET,
            Method::POST,
            Method::PUT,
            Method::PATCH,
            Method::DELETE,
            Method::OPTIONS,
            Method::HEAD,
            Method::CONNECT,
            Method::TRACE,
        ],
    }
}

pub fn is_actionable_method(http_method: &Method) -> bool {
    match http_method {
        &Method::OPTIONS | &Method::HEAD | &Method::CONNECT | &Method::TRACE => false,
        _ => true,
    }
}
