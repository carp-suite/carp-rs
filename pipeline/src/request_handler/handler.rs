use std::{collections::HashMap, error::Error as StdError, sync::Arc};

use crate::{
    caching::{
        identification::CacheIdentification, storage::CacheStorage, CacheManager,
        StorageManagerState,
    },
    context::RequestContext,
    request_handler::PipelineRequestHandler,
};
use futures::{future::try_join, Future, TryFutureExt};
use http::{Method, Request, Response, StatusCode};
use intrinsics::{likely, unlikely};
use matchit::Router;
use models::cache::Cache;
use tokio::runtime::Handle;
use uuid::Uuid;

use super::{error::Error, matchit_params::MatchitParams};
use crate::http_client::HttpClient;

pub struct RoutedClient<Client, Id, Identification, Factory, Storage> {
    pub(crate) client: Arc<Client>,
    pub(crate) cache_manager: Option<CacheManager<Id, Identification, Factory, Storage>>,
}

pub struct RequestHandler<Client, Id, Identification, Factory, Storage> {
    router:
        Arc<Router<HashMap<Method, RoutedClient<Client, Id, Identification, Factory, Storage>>>>,
    runtime_handle: Arc<Handle>,
}

impl<Client, Id, Identification, Factory, Storage> Clone
    for RequestHandler<Client, Id, Identification, Factory, Storage>
{
    fn clone(&self) -> Self {
        RequestHandler {
            router: self.router.clone(),
            runtime_handle: self.runtime_handle.clone(),
        }
    }
}

impl<Client, Id, Identification, Factory, Storage>
    RequestHandler<Client, Id, Identification, Factory, Storage>
{
    pub fn new(
        router: Router<HashMap<Method, RoutedClient<Client, Id, Identification, Factory, Storage>>>,
        runtime_handle: Handle,
    ) -> Self {
        RequestHandler {
            router: Arc::new(router),
            runtime_handle: Arc::new(runtime_handle),
        }
    }
}

impl<
        RequestBody,
        ResponseBody,
        Client,
        Id,
        Identification,
        FactoryFut,
        FactoryError,
        Factory,
        Storage,
    > PipelineRequestHandler<RequestBody, ResponseBody>
    for RequestHandler<Client, Id, Identification, Factory, Storage>
where
    Client: HttpClient<RequestBody, ResponseBody> + Send + Sync + 'static,
    Client::Error: Send + StdError + 'static,
    Client::AfterRequestFuture: Send + 'static,
    Client::BeforeRequestFuture: Send + 'static,
    Client::Future: Send + 'static,
    ResponseBody: Send + Default + 'static,
    RequestBody: Send + 'static,
    Identification: CacheIdentification<RequestBody, Id = Id>,
    Factory: for<'a> Fn(&'a Cache, &'a [u8]) -> FactoryFut + Send + Sync + 'static,
    FactoryFut: Future<Output = Result<Storage, FactoryError>> + Sync + Send + 'static,
    FactoryError: StdError + Send + 'static,
    Id: Eq + std::hash::Hash + AsRef<[u8]> + Send + Sync + 'static,
    Storage: CacheStorage<ResponseBody, Client::Future> + Send + Sync + 'static,
    Storage::Error: Send + StdError + 'static,
    Storage::LoadOrStoreFuture: Send + 'static,
    Storage::InvalidateFuture: Send + 'static,
{
    type Error = Error<Client::Error, Storage::Error, FactoryError>;
    type Future = impl Future<Output = Result<Response<ResponseBody>, Self::Error>>;

    /// Function that delegates to the matched client the request.
    /// If the method is mismatched, the response will have status 405.
    /// If the inner client is not found, the response will have status 404.
    #[inline]
    fn handle(&mut self, mut req: Request<RequestBody>) -> Self::Future {
        let router = Arc::clone(&self.router);
        let runtime_handle = Arc::clone(&self.runtime_handle);
        let path = req.uri().path().to_string();

        async move {
            let match_option = router.at(&path).ok();
            if likely(match_option.is_some()) {
                let match_option = match_option.unwrap();
                let params = match_option.params;
                let target = match_option.value;

                if unlikely(!target.contains_key(req.method())) {
                    return Ok(Response::builder()
                        .status(StatusCode::METHOD_NOT_ALLOWED)
                        .body(ResponseBody::default())
                        .unwrap());
                }

                let target = target.get(req.method()).unwrap();
                let cache_manager = target.cache_manager.as_ref();
                let client = Arc::clone(&target.client);
                let (tx, rx) = tokio::sync::oneshot::channel();

                let id = Arc::new(Uuid::new_v4());
                let uri = req.uri().clone();
                let ctx = RequestContext::new(id, uri);

                req.extensions_mut().insert(ctx.clone());
                let req = match client.before_request(req).await.map_err(Error::Client)? {
                    either::Either::Left(mut req) => {
                        req.extensions_mut().insert::<RequestContext>(ctx.clone());
                        req
                    }
                    either::Either::Right(mut res) => {
                        res.extensions_mut().insert(ctx.clone());
                        let response = client.after_request(res).await.map_err(Error::Client)?;

                        return Ok(response);
                    }
                };

                if let Some((
                    cache_manager_handle,
                    StorageManagerState {
                        identification,
                        storage_manager,
                    },
                )) = cache_manager.and_then(|cache_manager| {
                    cache_manager
                        .storage_identification
                        .as_ref()
                        .map(|storage_manager| (cache_manager.handle(), storage_manager))
                }) {
                    let id = tokio::task::block_in_place(|| {
                        identification.identify(MatchitParams::new(&params), &req)
                    });

                    let storage = storage_manager
                        .get_or_create(id)
                        .await
                        .map_err(Error::Factory)?;

                    runtime_handle.spawn(async move {
                        let response_result = || async move {
                            let (_, mut response) = try_join(
                                cache_manager_handle.invalidate().map_err(Error::Storage),
                                tokio::task::unconstrained(
                                    storage.load_or_store(client.request(req)),
                                )
                                .map_err(Error::Storage),
                            )
                            .await?;
                            response.extensions_mut().insert(ctx);

                            let response = client
                                .after_request(response)
                                .await
                                .map_err(Error::Client)?;

                            Ok(response)
                        };

                        tx.send(response_result().await).ok();
                        Ok::<_, Error<Client::Error, Storage::Error, FactoryError>>(())
                    });
                } else {
                    let cache_manager_handle = cache_manager.map(CacheManager::handle);
                    runtime_handle.spawn(async move {
                        let response_result = || async move {
                            let mut response =
                                if let Some(cache_manager_handle) = cache_manager_handle {
                                    let (_, response) = try_join(
                                        cache_manager_handle.invalidate().map_err(Error::Storage),
                                        tokio::task::unconstrained(client.request(req))
                                            .map_err(Error::Client),
                                    )
                                    .await?;

                                    response
                                } else {
                                    tokio::task::unconstrained(client.request(req))
                                        .map_err(Error::Client)
                                        .await?
                                };
                            response.extensions_mut().insert(ctx);

                            let response = client
                                .after_request(response)
                                .await
                                .map_err(Error::Client)?;

                            Ok::<_, Error<Client::Error, Storage::Error, FactoryError>>(response)
                        };

                        tx.send(response_result().await).ok();
                        Ok::<_, Error<Client::Error, Storage::Error, FactoryError>>(())
                    });
                }

                rx.await?
            } else {
                Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(ResponseBody::default())
                    .unwrap())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::convert::Infallible;

    use super::*;
    use crate::caching::{identification::iter::Iter, storage::storage_manager::StorageManager};
    use either::Either;
    use futures::future::{ready, Ready};
    use hyper::{
        body::{self, Bytes},
        Body,
    };
    use mockall::mock;
    use thiserror::Error;

    struct BytesId;
    impl CacheIdentification<Body> for BytesId {
        type Id = Bytes;
        fn identify<'k, 'v, Params>(&self, _: Params, _: &http::Request<Body>) -> Bytes
        where
            Params: Iter<'k, 'v>,
        {
            Bytes::from("not found")
        }
    }

    #[derive(Debug, Error)]
    #[error("Mock error")]
    struct Error;

    mock! {
        Client {}

        impl HttpClient<Body, Body> for Client {
            type Error = Error;
            type Future = std::pin::Pin<Box<dyn std::future::Future<Output = Result<Response<Body>, Error>> + Send + Sync>>;
            type BeforeRequestFuture = std::pin::Pin<Box<dyn Future<Output = Result<either::Either<Request<Body>, Response<Body>>, Self::Error>> + Send>>;
            type AfterRequestFuture = std::pin::Pin<Box<dyn Future<Output = Result<Response<Body>, Self::Error>> + Send>>;

            fn before_request(&self, req: Request<Body>) -> <Self as HttpClient<Body, Body>>::BeforeRequestFuture;
            fn request(&self, req: http::Request<Body>) -> <Self as HttpClient<Body, Body>>::Future;
            fn after_request(&self, res: Response<Body>) -> <Self as HttpClient<Body, Body>>::AfterRequestFuture;
        }
    }

    mock! {
        Storage {}

        impl CacheStorage<Body, std::pin::Pin<Box<dyn Future<Output = Result<Response<Body>, Error>> + Send + Sync>>> for Storage {
            type Error = Error;
            type LoadOrStoreFuture = std::pin::Pin<Box<dyn Future<Output = Result<Response<Body>, Error>> + Send + Sync>>;
            type InvalidateFuture = std::pin::Pin<Box<dyn Future<Output = Result<(), Error>> + Send + Sync>>;

            fn load_or_store(&self, request_future: std::pin::Pin<Box<dyn Future<Output = Result<Response<Body>, Error>> + Send + Sync>>) -> std::pin::Pin<Box<dyn Future<Output = Result<Response<Body>, Error>> + Send + Sync>>;
            fn invalidate(&self) -> std::pin::Pin<Box<dyn Future<Output = Result<(), Error>> + Send + Sync>>;
        }
    }

    type MockFactory = Box<
        dyn for<'a> Fn(&'a Cache, &'a [u8]) -> Ready<Result<MockStorage, Infallible>> + Send + Sync,
    >;

    #[tokio::test]
    async fn it_returns_not_found_response() {
        // Given

        let mut handler: RequestHandler<MockClient, Bytes, BytesId, MockFactory, MockStorage> =
            RequestHandler::new(Router::new(), Handle::current());
        let request = Request::builder()
            .uri("http://localhost/api")
            .body(Body::empty())
            .unwrap();

        // When
        let response = handler.handle(request).await.unwrap();

        // Then
        assert_eq!(response.status(), 404);
        assert!(body::to_bytes(response.into_body())
            .await
            .unwrap()
            .is_empty(),);
    }

    #[tokio::test]
    async fn it_returns_method_not_allowed_response() {
        // Given
        let request = http::Request::builder()
            .uri("http://localhost/api")
            .body(Body::empty())
            .unwrap();
        let client = MockClient::new();
        let routed_clients = HashMap::from_iter([(
            Method::POST,
            RoutedClient {
                client: Arc::new(client),
                cache_manager: Some(CacheManager::new(vec![])),
            },
        )]);
        let mut router = Router::new();
        router.insert("/api", routed_clients).unwrap();

        let mut handler: RequestHandler<MockClient, Bytes, BytesId, MockFactory, MockStorage> =
            RequestHandler::new(router, Handle::current());

        // When
        let response = handler.handle(request).await.unwrap();

        // Then
        assert_eq!(response.status(), 405);
        assert!(body::to_bytes(response.into_body())
            .await
            .unwrap()
            .is_empty(),);
    }

    #[tokio::test]
    async fn it_returns_response_from_given_client() {
        // Given
        fn create_mock_invalidate_storage(
            _: &Cache,
            _: &[u8],
        ) -> Ready<Result<MockStorage, Infallible>> {
            let mut storage = MockStorage::new();
            storage
                .expect_invalidate()
                .once()
                .returning(|| Box::pin(async move { Ok(()) }));

            ready(Ok(storage))
        }
        let mut client = MockClient::new();
        client
            .expect_before_request()
            .once()
            .returning(|req| Box::pin(async { Ok(Either::Left(req)) }));
        client
            .expect_after_request()
            .once()
            .returning(|res| Box::pin(async { Ok(res) }));
        client.expect_request().once().return_once(|_| {
            Box::pin(async {
                Ok(Response::builder()
                    .status(200)
                    .body(Body::from("Hello World"))
                    .unwrap())
            })
        });
        let cache = Cache::from(("My Cache 1".into(), None, vec![], vec![], vec![]));
        let routed_clients = HashMap::from_iter([(
            Method::GET,
            RoutedClient {
                client: Arc::new(client),
                cache_manager: Some(CacheManager::new(vec![Arc::new(StorageManager::new(
                    cache,
                    Box::new(create_mock_invalidate_storage) as MockFactory,
                ))])),
            },
        )]);
        let mut router = Router::new();
        router.insert("/api", routed_clients).unwrap();
        let mut handler: RequestHandler<MockClient, Bytes, BytesId, MockFactory, MockStorage> =
            RequestHandler::new(router, Handle::current());
        let request = Request::builder()
            .uri("http://localhost/api")
            .body(Body::empty())
            .unwrap();

        // When
        let response = handler.handle(request).await.unwrap();

        // Then
        assert_eq!(response.status(), 200);
        assert_eq!(
            body::to_bytes(response.into_body()).await.unwrap().to_vec(),
            b"Hello World"
        );
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn it_returns_cached_response_from_given_client() {
        // Given
        fn create_mock_storage(_: &Cache, _: &[u8]) -> Ready<Result<MockStorage, Infallible>> {
            let mut storage = MockStorage::new();
            storage.expect_load_or_store().once().returning(move |_| {
                Box::pin(async move {
                    Ok(Response::builder()
                        .status(200)
                        .body(Body::from("Hello World Cached"))
                        .unwrap())
                })
            });

            ready(Ok(storage))
        }
        fn create_mock_invalidate_storage(
            _: &Cache,
            _: &[u8],
        ) -> Ready<Result<MockStorage, Infallible>> {
            let mut storage = MockStorage::new();
            storage
                .expect_invalidate()
                .once()
                .returning(|| Box::pin(async move { Ok(()) }));

            ready(Ok(storage))
        }
        let mut client = MockClient::new();
        client
            .expect_before_request()
            .once()
            .returning(|req| Box::pin(async { Ok(Either::Left(req)) }));
        client
            .expect_after_request()
            .once()
            .returning(|res| Box::pin(async { Ok(res) }));
        client.expect_request().once().returning(|_| {
            Box::pin(async {
                Ok(Response::builder()
                    .status(200)
                    .body(Body::from("Hello World"))
                    .unwrap())
            })
        });
        let cache_1 = Cache::from(("My Cache 1".into(), None, vec![], vec![], vec![]));
        let cache_2 = Cache::from(("My Cache 2".into(), None, vec![], vec![], vec![]));
        let invalidate_storage_manager = Arc::new(StorageManager::new(
            cache_2,
            Box::new(create_mock_invalidate_storage) as MockFactory,
        ));
        //// Create at least one cache storage
        invalidate_storage_manager
            .get_or_create(Bytes::default())
            .await
            .unwrap();
        let routed_clients = HashMap::from_iter([(
            Method::GET,
            RoutedClient {
                client: Arc::new(client),
                cache_manager: Some(CacheManager::new_with_identification(
                    BytesId,
                    Arc::new(StorageManager::new(
                        cache_1,
                        Box::new(create_mock_storage) as MockFactory,
                    )),
                    vec![invalidate_storage_manager],
                )),
            },
        )]);
        let mut router = Router::new();
        router.insert("/api", routed_clients).unwrap();
        let mut handler: RequestHandler<MockClient, Bytes, BytesId, MockFactory, MockStorage> =
            RequestHandler::new(router, Handle::current());
        let request = Request::builder()
            .uri("http://localhost/api")
            .body(Body::empty())
            .unwrap();

        // When
        let cached_response = handler.handle(request).await.unwrap();

        // Then
        assert_eq!(cached_response.status(), 200);
        assert_eq!(
            body::to_bytes(cached_response.into_body())
                .await
                .unwrap()
                .to_vec(),
            b"Hello World Cached"
        );
    }
}
