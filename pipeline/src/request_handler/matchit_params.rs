use crate::caching::identification::iter::Iter;
use matchit::{Params, ParamsIter};

#[repr(transparent)]
pub struct MatchitParams<'a, 'k, 'v>(&'a Params<'k, 'v>);

impl<'a, 'k, 'v> MatchitParams<'a, 'k, 'v> {
    pub fn new(params: &'a Params<'k, 'v>) -> Self {
        MatchitParams(params)
    }
}

impl<'a, 'k, 'v> Iter<'k, 'v> for MatchitParams<'a, 'k, 'v>
where
    'a: 'k,
    'a: 'v,
    Self: 'a,
{
    type Iterator = ParamsIter<'a, 'k, 'v>;

    fn iter(&self) -> Self::Iterator {
        self.0.iter()
    }
}
