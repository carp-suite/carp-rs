use std::marker::PhantomData;

use super::PipelineRequestHandler;
use http::{Request, Response};
use tower::Service;

#[repr(transparent)]
pub struct PipelineService<Inner, ResponseBody>(Inner, PhantomData<ResponseBody>);

/// PipelineRequestHandler -> Service transformations

pub trait PipelineRequestHandlerServiceExt<RequestBody, ResponseBody>:
    PipelineRequestHandler<RequestBody, ResponseBody>
where
    Self: Sized,
{
    fn to_service(self) -> PipelineService<Self, ResponseBody>;
}

impl<P, RequestBody, ResponseBody> PipelineRequestHandlerServiceExt<RequestBody, ResponseBody> for P
where
    P: PipelineRequestHandler<RequestBody, ResponseBody>,
{
    #[inline]
    fn to_service(self) -> PipelineService<Self, ResponseBody> {
        PipelineService(self, PhantomData)
    }
}

impl<P, RequestBody, ResponseBody> Service<Request<RequestBody>>
    for PipelineService<P, ResponseBody>
where
    P: PipelineRequestHandler<RequestBody, ResponseBody>,
{
    type Response = Response<ResponseBody>;
    type Error = P::Error;
    type Future = P::Future;

    #[inline]
    fn poll_ready(
        &mut self,
        _cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        std::task::Poll::Ready(Ok(()))
    }

    #[inline]
    fn call(&mut self, req: Request<RequestBody>) -> Self::Future {
        self.0.handle(req)
    }
}
