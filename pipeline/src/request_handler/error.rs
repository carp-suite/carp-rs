use std::error::Error as StdError;
use thiserror::Error;
use tokio::task::JoinError;

#[derive(Debug, Error)]
pub enum SetupError<ClientCreationError, CacheCreationError> {
    #[error("Error while creating client for route {0}")]
    ClientCreationError(String, #[source] ClientCreationError),
    #[error("Error while preparing invalidations for route {0}")]
    InvalidationsPreparationError(String, #[source] CacheCreationError),
    #[error("Error while preparing cache {0} for route {1}")]
    CacheCreationError(String, String, #[source] CacheCreationError),
}

#[derive(Debug, Error)]
pub enum Error<ClientError, StorageError, FactoryError>
where
    ClientError: StdError,
    StorageError: StdError,
    FactoryError: StdError,
{
    #[error("Error processing request")]
    Client(#[source] ClientError),
    #[error("Error retrieving request from cache")]
    Storage(#[source] StorageError),
    #[error(transparent)]
    JoinError(#[from] JoinError),
    #[error(transparent)]
    ReceiveError(#[from] tokio::sync::oneshot::error::RecvError),
    #[error("Error creating storage")]
    Factory(#[source] FactoryError),
}
