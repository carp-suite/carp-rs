use std::sync::Arc;

use self::{handle::CacheManagerHandle, storage::storage_manager::StorageManager};

pub mod handle;
pub mod identification;
pub mod registry;
pub mod storage;

pub struct StorageManagerState<Id, Identification, Factory, Storage> {
    pub storage_manager: Arc<StorageManager<Id, Factory, Storage>>,
    pub identification: Identification,
}

pub struct CacheManager<Id, Identification, Factory, Storage> {
    pub storage_identification: Option<StorageManagerState<Id, Identification, Factory, Storage>>,
    pub storages_to_invalidate: Arc<Vec<Arc<StorageManager<Id, Factory, Storage>>>>,
}

impl<Id, Identification, Factory, Storage> CacheManager<Id, Identification, Factory, Storage> {
    pub fn new_with_identification(
        identification: Identification,
        storage_manager: Arc<StorageManager<Id, Factory, Storage>>,
        storages_to_invalidate: Vec<Arc<StorageManager<Id, Factory, Storage>>>,
    ) -> Self {
        CacheManager {
            storage_identification: Some(StorageManagerState {
                storage_manager,
                identification,
            }),
            storages_to_invalidate: Arc::new(storages_to_invalidate),
        }
    }

    pub fn new(storages_to_invalidate: Vec<Arc<StorageManager<Id, Factory, Storage>>>) -> Self {
        CacheManager {
            storage_identification: None,
            storages_to_invalidate: Arc::new(storages_to_invalidate),
        }
    }

    pub fn handle(&self) -> CacheManagerHandle<Id, Factory, Storage> {
        CacheManagerHandle::new(self.storages_to_invalidate.clone())
    }
}
