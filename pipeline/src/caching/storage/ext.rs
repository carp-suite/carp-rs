use either::Either;
use futures::{Future, FutureExt, TryFutureExt};

use super::CacheStorage;

pub trait CacheStorageExt<ResponseBody, ResFut>: CacheStorage<ResponseBody, ResFut> {
    fn right_storage<L>(self) -> Either<L, Self>
    where
        Self: Sized,
        L: CacheStorage<ResponseBody, ResFut>,
    {
        Either::Right::<L, _>(self)
    }

    fn left_storage<R>(self) -> Either<Self, R>
    where
        Self: Sized,
        R: CacheStorage<ResponseBody, ResFut>,
    {
        Either::Left::<_, R>(self)
    }
}

impl<Storage, ResponseBody, ResFut> CacheStorageExt<ResponseBody, ResFut> for Storage where
    Storage: CacheStorage<ResponseBody, ResFut>
{
}

impl<LeftStorage, RightStorage, ResponseBody, ResFut> CacheStorage<ResponseBody, ResFut>
    for Either<LeftStorage, RightStorage>
where
    LeftStorage: CacheStorage<ResponseBody, ResFut>,
    RightStorage: CacheStorage<ResponseBody, ResFut>,
{
    type Error = Either<LeftStorage::Error, RightStorage::Error>;
    type LoadOrStoreFuture =
        impl Future<Output = Result<http::Response<ResponseBody>, Self::Error>>;
    type InvalidateFuture = impl Future<Output = Result<(), Self::Error>>;

    #[inline]
    fn load_or_store(&self, response_future: ResFut) -> Self::LoadOrStoreFuture {
        match self {
            Either::Left(left) => left
                .load_or_store(response_future)
                .map_err(Either::Left)
                .left_future(),
            Either::Right(right) => right
                .load_or_store(response_future)
                .map_err(Either::Right)
                .right_future(),
        }
    }

    #[inline]
    fn invalidate(&self) -> Self::InvalidateFuture {
        match self {
            Either::Left(left) => left.invalidate().map_err(Either::Left).left_future(),
            Either::Right(right) => right.invalidate().map_err(Either::Right).right_future(),
        }
    }
}
