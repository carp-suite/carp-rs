use std::{collections::HashMap, sync::Arc};

use futures::{future::try_join_all, Future};
use models::cache::Cache;
use parking_lot::RwLock;

use super::CacheStorage;

pub struct StorageManager<Id, Factory, Backend> {
    cache: Cache,
    factory: Factory,
    pub(crate) existing_caches: RwLock<HashMap<Id, Arc<Backend>>>,
}

impl<Id, Factory, Backend> StorageManager<Id, Factory, Backend> {
    pub fn new(cache: Cache, factory: Factory) -> Self {
        StorageManager {
            cache,
            factory,
            existing_caches: RwLock::new(HashMap::new()),
        }
    }

    pub fn cache(&self) -> &Cache {
        &self.cache
    }
}

impl<Id, FactoryFut, FactoryError, Factory, Backend> StorageManager<Id, Factory, Backend>
where
    Factory: for<'a> Fn(&'a Cache, &'a [u8]) -> FactoryFut,
    FactoryFut: Future<Output = Result<Backend, FactoryError>> + Send + Sync + 'static,
    Id: Eq + std::hash::Hash + AsRef<[u8]>,
{
    pub async fn get_or_create(&self, id: Id) -> Result<Arc<Backend>, FactoryError> {
        loop {
            let existing_caches = self.existing_caches.read();
            if let Some(backend) = existing_caches.get(&id) {
                return Ok(backend.clone());
            } else {
                drop(existing_caches);
                match self.existing_caches.try_write() {
                    Some(mut existing_caches) => {
                        let backend = (self.factory)(&self.cache, id.as_ref()).await?;
                        let backend = Arc::new(backend);
                        existing_caches.insert(id, backend.clone());
                        return Ok(backend);
                    }
                    None => continue,
                }
            }
        }
    }
}

impl<Id, Factory, Backend> StorageManager<Id, Factory, Backend> {
    pub async fn invalidate<ResBody, ResFut>(&self) -> Result<(), Backend::Error>
    where
        Backend: CacheStorage<ResBody, ResFut>,
    {
        let existing_caches = self.existing_caches.read();

        try_join_all(
            existing_caches
                .iter()
                .map(|(_, backend)| backend.invalidate()),
        )
        .await?;

        drop(existing_caches);

        let mut existing_caches = self.existing_caches.write();
        existing_caches.clear();

        Ok(())
    }
}
