use futures::Future;
use http::Response;
pub mod ext;
pub mod storage_manager;

pub trait CacheStorage<ResponseBody, ResFut> {
    type Error;
    type LoadOrStoreFuture: Future<Output = Result<Response<ResponseBody>, Self::Error>>;
    type InvalidateFuture: Future<Output = Result<(), Self::Error>>;

    fn load_or_store(&self, response_future: ResFut) -> Self::LoadOrStoreFuture;
    fn invalidate(&self) -> Self::InvalidateFuture;
}
