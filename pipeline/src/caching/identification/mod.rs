use http::Request;
use iter::Iter;
pub mod iter;

pub trait CacheIdentification<ReqBody> {
    type Id: Eq + std::hash::Hash;

    fn identify<'k, 'v, Params>(&self, params: Params, req: &Request<ReqBody>) -> Self::Id
    where
        Params: Iter<'k, 'v>;
}
