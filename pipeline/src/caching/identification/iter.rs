pub trait Iter<'k, 'v> {
    type Iterator: Iterator<Item = (&'k str, &'v str)> + Send + Sync;

    fn iter(&self) -> Self::Iterator;
}
