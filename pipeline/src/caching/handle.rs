use std::sync::Arc;

use futures::future::try_join_all;

use super::storage::{storage_manager::StorageManager, CacheStorage};

pub struct CacheManagerHandle<Id, Factory, Storage>(
    Arc<Vec<Arc<StorageManager<Id, Factory, Storage>>>>,
);

impl<Id, Storage, Factory> CacheManagerHandle<Id, Factory, Storage> {
    pub fn new(managers: Arc<Vec<Arc<StorageManager<Id, Factory, Storage>>>>) -> Self {
        CacheManagerHandle(managers)
    }

    pub async fn invalidate<ResBody, ResFut>(&self) -> Result<(), Storage::Error>
    where
        Storage: CacheStorage<ResBody, ResFut>,
    {
        try_join_all(
            self.0
                .iter()
                .map(|storage_manager| storage_manager.invalidate()),
        )
        .await?;

        Ok(())
    }
}

