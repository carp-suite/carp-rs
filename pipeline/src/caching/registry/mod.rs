use std::{collections::HashSet, hash::Hash, sync::Arc};

use self::error::Error;
use crate::caching::storage::storage_manager::StorageManager;
use models::cache::Cache;
use petgraph::{
    algo::{all_simple_paths, toposort},
    prelude::DiGraph,
    stable_graph::NodeIndex,
    visit::IntoNodeReferences,
    Direction,
};

pub mod error;

struct ArcNode<Id, Factory, Backend>(pub Arc<StorageManager<Id, Factory, Backend>>);

impl<Id, Factory, Backend> PartialEq for ArcNode<Id, Factory, Backend> {
    fn eq(&self, other: &Self) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }
}

impl<Id, Factory, Backend> Eq for ArcNode<Id, Factory, Backend> {}

impl<Id, Factory, Backend> Hash for ArcNode<Id, Factory, Backend> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_usize(Arc::as_ptr(&self.0) as usize);
    }
}

pub struct CacheRegistry<Id, Factory, Backend> {
    graph: DiGraph<Arc<StorageManager<Id, Factory, Backend>>, ()>,
    indices: Option<Vec<(NodeIndex, Vec<String>)>>,
}

impl<Id, Factory, Backend> CacheRegistry<Id, Factory, Backend> {
    pub fn new() -> Self {
        CacheRegistry {
            graph: DiGraph::new(),
            indices: Some(Vec::new()),
        }
    }

    pub fn insert(&mut self, cache: Cache, factory: Factory) {
        let dependencies = cache.dependencies().to_owned();
        let idx = self
            .graph
            .add_node(Arc::new(StorageManager::new(cache, factory)));
        let indices = self.indices.as_mut().unwrap();

        indices.push((idx, dependencies));
    }

    pub fn caches<'a>(&'a self) -> impl Iterator<Item = &'a StorageManager<Id, Factory, Backend>> {
        self.graph
            .node_references()
            .map(|(_, cache)| cache.as_ref())
    }

    pub fn establish_relationships(&mut self) -> Result<(), Error> {
        for (idx_a, dependencies) in self.indices.as_ref().unwrap().iter() {
            for dependency in dependencies.iter() {
                let (idx_b, _) = self
                    .graph
                    .node_references()
                    .find(|(_, c)| c.cache().name() == dependency)
                    .ok_or_else(|| {
                        Error::DependencyNotFound(
                            self.graph[*idx_a].cache().name().to_owned(),
                            dependency.to_owned(),
                        )
                    })?;

                self.graph.add_edge(*idx_a, idx_b, ());
            }
        }

        self.indices = None;

        toposort(&self.graph, None).map_err(|err| {
            let path = self
                .graph
                .neighbors(err.node_id())
                .find_map(|n| {
                    all_simple_paths::<Vec<NodeIndex>, _>(&self.graph, n, err.node_id(), 0, None)
                        .next()
                        .map(|indices| {
                            indices
                                .into_iter()
                                .map(|idx| self.graph[idx].cache().name().to_owned())
                                .collect()
                        })
                })
                .unwrap();

            Error::Cycle(path)
        })?;

        Ok(())
    }

    pub fn try_get_storage_manager(
        &self,
        cache_name: &str,
    ) -> Result<Arc<StorageManager<Id, Factory, Backend>>, Error> {
        let Some((_, node)) = self.graph.node_references().find(|(_, node)| node.cache().name() == cache_name) else {
            return Err(Error::CacheNotFound(cache_name.to_owned()));
        };

        Ok(node.clone())
    }

    pub fn try_get_storage_managers_to_invalidate<'a>(
        &self,
        cache_names: impl Iterator<Item = &'a str>,
    ) -> Result<Vec<Arc<StorageManager<Id, Factory, Backend>>>, Error> {
        let mut managers_ids = cache_names
            .map(|cache_name| {
                self.graph
                    .node_references()
                    .find_map(|(id, node)| {
                        if node.cache().name() == cache_name {
                            Some(id)
                        } else {
                            None
                        }
                    })
                    .ok_or_else(|| Error::CacheNotFound(cache_name.to_owned()))
            })
            .collect::<Result<Vec<_>, _>>()?;
        let mut managers = HashSet::with_capacity(managers_ids.len());

        loop {
            let Some(id) = managers_ids.pop() else {
                break;
            };

            managers.insert(ArcNode(self.graph[id].clone()));

            for nid in self.graph.neighbors_directed(id, Direction::Outgoing) {
                managers_ids.push(nid);
            }
        }

        Ok(managers
            .into_iter()
            .map(|ArcNode(manager)| manager)
            .collect())
    }
}

pub struct CacheFactory<Factory> {
    pub cache: Cache,
    pub factory: Factory,
}

pub fn make<Id, Factory, Backend>(
    caches: Vec<CacheFactory<Factory>>,
) -> Result<CacheRegistry<Id, Factory, Backend>, Error> {
    let mut cache_registry = CacheRegistry::new();

    for cache in caches {
        cache_registry.insert(cache.cache, cache.factory);
    }

    cache_registry.establish_relationships()?;
    Ok(cache_registry)
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use models::route::cache_usage::CacheUsage;

    use super::*;
    #[test]
    fn it_inserts_cache_correctly() {
        // Given
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();
        let cache = Cache::from((
            "Cache 1".to_owned(),
            Some(16000),
            vec!["Cache 2".to_owned()],
            vec![],
            vec![],
        ));

        // When
        sut.insert(cache, ());

        // Then
        assert_eq!(sut.caches().next().unwrap().cache().name(), "Cache 1");
    }

    #[test]
    fn it_establishes_relationships_successfully() {
        // Given
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();
        let cache_a = Cache::from((
            "Cache 1".to_owned(),
            Some(16000),
            vec!["Cache 2".to_owned()],
            vec![],
            vec![],
        ));
        let cache_b = Cache::from(("Cache 2".to_owned(), None, vec![], vec![], vec![]));
        sut.insert(cache_a, ());
        sut.insert(cache_b, ());

        // When
        let result = sut.establish_relationships();

        // Then
        assert!(result.is_ok());
    }

    #[test]
    fn it_fails_when_there_is_a_cache_cycle() {
        // Given
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();
        let cache_a = Cache::from((
            "Cache 1".to_owned(),
            None,
            vec!["Cache 2".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_b = Cache::from((
            "Cache 2".to_owned(),
            None,
            vec!["Cache 3".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_c = Cache::from((
            "Cache 3".to_owned(),
            None,
            vec!["Cache 1".to_owned()],
            vec![],
            vec![],
        ));
        let cache_d = Cache::from(("Cache 4".to_owned(), None, vec![], vec![], vec![]));

        sut.insert(cache_a, ());
        sut.insert(cache_b, ());
        sut.insert(cache_c, ());
        sut.insert(cache_d, ());

        // When
        let cycle = sut.establish_relationships().unwrap_err();

        // Then
        assert_eq!(
            cycle,
            Error::Cycle(HashSet::from_iter(vec![
                "Cache 1".to_owned(),
                "Cache 2".to_owned(),
                "Cache 3".to_owned()
            ]))
        );
    }

    #[test]
    fn it_fails_when_cache_is_missing() {
        // Given
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();
        let cache_a = Cache::from((
            "Cache 1".to_owned(),
            None,
            vec!["Cache 4".to_owned()],
            vec![],
            vec![],
        ));

        sut.insert(cache_a, ());

        // When
        let missing = sut.establish_relationships().unwrap_err();

        // Then
        assert_eq!(
            missing,
            Error::DependencyNotFound("Cache 1".to_owned(), "Cache 4".to_owned())
        );
    }

    #[test]
    fn it_generates_cache_manager_with_correct_caches_to_invalidate_and_manager() {
        // Given
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();
        let cache_a = Cache::from((
            "Cache 1".to_owned(),
            None,
            vec!["Cache 2".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_b = Cache::from((
            "Cache 2".to_owned(),
            None,
            vec!["Cache 3".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_c = Cache::from(("Cache 3".to_owned(), None, vec![], vec![], vec![]));
        let cache_d = Cache::from(("Cache 4".to_owned(), None, vec![], vec![], vec![]));
        let cache_e = Cache::from(("Cache 5".to_owned(), None, vec![], vec![], vec![]));
        let cache_usage = CacheUsage::from(("Cache 1".to_string(), vec![], vec![], vec![]));

        sut.insert(cache_a, ());
        sut.insert(cache_b, ());
        sut.insert(cache_c, ());
        sut.insert(cache_d, ());
        sut.insert(cache_e, ());

        sut.establish_relationships().unwrap();
        // When
        let cache_storage = sut.try_get_storage_manager(cache_usage.cache()).unwrap();

        // Then
        assert_eq!("Cache 1", cache_storage.cache().name());
    }

    #[test]
    fn it_finds_storage_manager_to_invalidate() {
        // Given
        let cache_a = Cache::from((
            "Cache 1".to_owned(),
            None,
            vec!["Cache 2".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_b = Cache::from((
            "Cache 2".to_owned(),
            None,
            vec!["Cache 3".to_owned(), "Cache 4".to_owned()],
            vec![],
            vec![],
        ));
        let cache_c = Cache::from(("Cache 3".to_owned(), None, vec![], vec![], vec![]));
        let cache_d = Cache::from(("Cache 4".to_owned(), None, vec![], vec![], vec![]));
        let cache_e = Cache::from(("Cache 5".to_owned(), None, vec![], vec![], vec![]));
        let caches_to_invalidate = vec!["Cache 1", "Cache 5"];
        let mut sut: CacheRegistry<(), (), ()> = CacheRegistry::new();

        sut.insert(cache_a, ());
        sut.insert(cache_b, ());
        sut.insert(cache_c, ());
        sut.insert(cache_d, ());
        sut.insert(cache_e, ());
        sut.establish_relationships().unwrap();

        // When
        let storages = sut
            .try_get_storage_managers_to_invalidate(caches_to_invalidate.iter().copied())
            .unwrap();
        let mut storages = storages
            .iter()
            .map(|s| s.cache().name())
            .collect::<Vec<_>>();
        storages.sort();

        // Then
        assert_eq!(
            vec!["Cache 1", "Cache 2", "Cache 3", "Cache 4", "Cache 5"],
            storages
        );
    }
}
