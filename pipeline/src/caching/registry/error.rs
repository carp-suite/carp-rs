use itertools::Itertools;
use std::collections::HashSet;
use thiserror::Error;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum Error {
    #[error("Referenced cache not found: {0}")]
    CacheNotFound(String),
    #[error("The cache {0} depends on {1}, but {1} does not exist")]
    DependencyNotFound(String, String),
    #[error("A cycle was detected {cycle}", cycle = match self {
        Self::Cycle(set) => {
            set.iter().sorted().join(" -> ")
        },
        _ => unreachable!(),
    })]
    Cycle(HashSet<String>),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_serializes_cycle_correctly() {
        // Given
        let mut hashset = HashSet::new();
        hashset.insert("a".to_string());
        hashset.insert("b".to_string());
        hashset.insert("c".to_string());

        let sut = Error::Cycle(hashset);

        // When
        let msg = sut.to_string();

        // Then
        assert_eq!(msg, "A cycle was detected a -> b -> c")
    }
}
