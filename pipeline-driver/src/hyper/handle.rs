use std::pin::Pin;

use futures::Future;
use tokio::{sync::oneshot::Sender, task::JoinHandle};

use super::error::HandlerError;

pub struct HyperServerCloseHandle(JoinHandle<hyper::Result<()>>, Sender<()>);

impl HyperServerCloseHandle {
    pub async fn close(self) -> Result<(), HandlerError> {
        let Ok(()) = self.1.send(()) else {
            return Err(HandlerError::SendFailure)
        };
        self.0.await??;
        Ok(())
    }
}

pub struct HyperServerHandle(
    Pin<Box<dyn Future<Output = hyper::Result<()>> + Send + Sync + 'static>>,
    Sender<()>,
);

impl HyperServerHandle {
    pub fn new(
        future: impl Future<Output = hyper::Result<()>> + Send + Sync + 'static,
        sender: Sender<()>,
    ) -> Self {
        HyperServerHandle(Box::pin(future), sender)
    }

    pub async fn wait(self) -> hyper::Result<()> {
        self.0.await
    }

    pub fn fork(self) -> Result<HyperServerCloseHandle, HandlerError> {
        let handle = tokio::spawn(self.0);
        Ok(HyperServerCloseHandle(handle, self.1))
    }
}
