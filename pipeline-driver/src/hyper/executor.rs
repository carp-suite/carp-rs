use futures::Future;
use hyper::rt::Executor;

#[derive(Clone)]
pub struct UnconstrainedExecutor;

impl<Fut> Executor<Fut> for UnconstrainedExecutor
where
    Fut: Future + Send + Sync + 'static,
    Fut::Output: Send + Sync + 'static,
{
    fn execute(&self, fut: Fut) {
        tokio::spawn(tokio::task::unconstrained(fut));
    }
}
