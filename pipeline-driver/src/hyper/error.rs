use std::net::AddrParseError;

use thiserror::Error;
use tokio::task::JoinError;

#[derive(Error, Debug)]
pub enum HandlerError {
    #[error("Could not send stop signal to web server")]
    SendFailure,
    #[error("Failed to wait for the server to stop")]
    JoinError(#[from] JoinError),
    #[error(transparent)]
    UnhandledHyperError(#[from] hyper::Error),
}

#[derive(Error, Debug)]
pub enum SetupError {
    #[error("The \"host\" field is not a valid IPV4 address")]
    InvalidHost(#[from] AddrParseError),
    #[error("Could not set up TLS protocol for web server with the provided \"cert\" and \"key\" settings")]
    TLSError(#[from] anyhow::Error),
    #[error("Could not bind to TCP Socket")]
    TCPError(#[from] hyper::Error),
    #[error(transparent)]
    UnhandledIOError(#[from] std::io::Error),
}
