use self::either::Either;
use self::executor::UnconstrainedExecutor;
use self::{error::SetupError, handle::HyperServerHandle};
use anyhow::anyhow;
use futures::Future;
use hyper::{
    server::{
        conn::{AddrIncoming, AddrStream},
        Builder,
    },
    service::make_service_fn,
    Server,
};
use models::settings::{http_version::HttpVersion, Settings};
use pipeline::{
    driver::PipelineDriver,
    request_handler::{tower::PipelineRequestHandlerServiceExt, PipelineRequestHandler},
};
use simple_hyper_server_tls::{
    listener_from_pem_files,
    tls_listener::{hyper::WrappedAccept, TlsListener},
    Protocols,
};
use std::marker::PhantomData;
use std::time::Duration;
use std::{
    convert::Infallible,
    error::Error as StdError,
    future::{ready, Ready},
    net::{IpAddr, SocketAddr},
    str::FromStr,
};
use tokio::net::TcpListener;
use tokio::sync::oneshot;
use tokio_rustls::{server::TlsStream, TlsAcceptor};

pub mod either;
pub(self) mod error;
pub(self) mod executor;
pub(self) mod handle;
pub mod tls;

enum HyperBuilder {
    TCP(Builder<AddrIncoming, UnconstrainedExecutor>),
    TLS(Builder<TlsListener<WrappedAccept<AddrIncoming>, TlsAcceptor>, UnconstrainedExecutor>),
}

pub struct HyperDriver<Body>(HyperBuilder, PhantomData<Body>);

pub fn hyper_driver<Body>(settings: Settings) -> Result<HyperDriver<Body>, SetupError> {
    let ip_addr = IpAddr::from_str(settings.host())?;
    let socket_addr = SocketAddr::from((ip_addr, settings.port()));
    let protocols: Protocols = if settings.http_version_number() == HttpVersion::V1 {
        Protocols::HTTP1
    } else {
        Protocols::HTTP2
    };
    match settings.tls() {
        Some((key, cert)) => {
            let tls = listener_from_pem_files(cert, key, protocols, &socket_addr)
                .map_err(|err| anyhow!("{}", err))?;
            let builder = Server::builder(tls);
            let builder = if protocols == Protocols::HTTP1 {
                builder.http1_only(true)
            } else {
                builder.http2_only(true)
            }
            .executor(UnconstrainedExecutor)
            .http1_max_buf_size(10 * 1024 * 1024);

            Ok(HyperDriver(HyperBuilder::TLS(builder), PhantomData))
        }
        None => {
            let listener = std::net::TcpListener::bind(socket_addr)?;
            listener.set_nonblocking(true)?;
            let listener = TcpListener::from_std(listener)?;
            let listener = AddrIncoming::from_listener(listener)?;
            let builder = Server::builder(listener);
            let builder = (if protocols == Protocols::HTTP1 {
                builder.http1_only(true)
            } else {
                builder.http2_only(true)
            })
            .executor(UnconstrainedExecutor)
            .tcp_nodelay(true)
            .tcp_keepalive(Some(Duration::from_secs(90)));

            Ok(HyperDriver(HyperBuilder::TCP(builder), PhantomData))
        }
    }
}

impl<Body, MP, P> PipelineDriver<MP, HyperServerHandle> for HyperDriver<Body>
where
    for<'a> MP:
        FnMut(Either<&'a AddrStream, &'a TlsStream<AddrStream>>) -> P + Send + Sync + 'static,
    P: PipelineRequestHandler<hyper::Body, Body> + Send + Sync + 'static,
    P::Error: StdError + Send + Sync + 'static,
    P::Future: Future + Send + Sync + 'static,
    Body: hyper::body::HttpBody + Send + Sync + 'static,
    Body::Error: StdError + Send + Sync + 'static,
    Body::Data: Send + Sync + 'static,
{
    type Error = Infallible;
    type RequestBody = Body;
    type Future = Ready<Result<HyperServerHandle, Infallible>>;

    fn start(self, mut maker: MP) -> Self::Future {
        match self.0 {
            HyperBuilder::TCP(builder) => {
                let server = builder.serve(make_service_fn(
                    #[inline]
                    move |arg| ready(Ok::<_, Infallible>(maker(Either::Left(arg)).to_service())),
                ));
                let (tx, rx) = oneshot::channel::<()>();
                let server = server.with_graceful_shutdown(async move {
                    rx.await.unwrap();
                });
                ready(Ok(HyperServerHandle::new(server, tx)))
            }
            HyperBuilder::TLS(builder) => {
                let server = builder.serve(make_service_fn(
                    #[inline]
                    move |arg| ready(Ok::<_, Infallible>(maker(Either::Right(arg)).to_service())),
                ));
                let (tx, rx) = oneshot::channel();
                let server = server.with_graceful_shutdown(async move {
                    rx.await.unwrap();
                });
                ready(Ok(HyperServerHandle::new(server, tx)))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use hyper::{client::HttpConnector, Body, Client, Request, Response, Uri};
    use hyper_rustls::{HttpsConnector, HttpsConnectorBuilder};
    use mockall::mock;
    use rustls::{
        client::{ServerCertVerified, ServerCertVerifier},
        ClientConfig,
    };
    use std::sync::Arc;

    mock! {
        Handler {}

        impl PipelineRequestHandler<Body, Body> for Handler {
            type Error = Infallible;
            type Future = std::pin::Pin<Box<dyn std::future::Future<Output = Result<Response<Body>, Self::Error>> + Send + Sync>>;

            fn handle(&mut self, req: Request<Body>) -> <Self as PipelineRequestHandler<Body, Body>>::Future;
        }
    }

    #[tokio::test]
    async fn it_creates_http_builder_according_to_settings() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:5000/api",
            HttpVersion::V1,
            None,
            None,
            vec![],
        ))
        .expect("Failure creating settings");
        let server_handle = hyper_driver(settings)
            .unwrap()
            .start(|_| {
                let mut mock_handler = MockHandler::new();
                mock_handler.expect_handle().times(1).returning(|_| {
                    Box::pin(async move {
                        Ok(Response::builder()
                            .status(200)
                            .body(Body::from("Hello World"))
                            .unwrap())
                    })
                });

                mock_handler
            })
            .await
            .expect("Failure creating hyper driver");
        let close_handle = server_handle
            .fork()
            .expect("Failed to start server in another task");

        // When
        let client: Client<HttpConnector> = Client::builder().build(HttpConnector::new());
        let response = client
            .get(Uri::from_static("http://127.0.0.1:5000"))
            .await
            .unwrap();
        println!("Received response");
        let status = response.status();
        let body = hyper::body::to_bytes(response)
            .await
            .expect("Cannot receive response bytes");

        close_handle.close().await.expect("Failed to stop server");

        // Then
        assert_eq!(
            String::from_utf8(body.into()).expect("Response body is not UTF-8"),
            "Hello World"
        );
        assert_eq!(status, 200)
    }

    #[tokio::test]
    async fn it_creates_https_builder_according_to_settings() {
        // Given
        struct DummyServerCertVerifier;
        impl ServerCertVerifier for DummyServerCertVerifier {
            fn verify_server_cert(
                &self,
                _end_entity: &rustls::Certificate,
                _intermediates: &[rustls::Certificate],
                _server_name: &rustls::ServerName,
                _scts: &mut dyn Iterator<Item = &[u8]>,
                _ocsp_response: &[u8],
                _now: std::time::SystemTime,
            ) -> Result<rustls::client::ServerCertVerified, rustls::Error> {
                Ok(ServerCertVerified::assertion())
            }
        }
        let settings = Settings::try_from((
            "https://127.0.0.1/api",
            HttpVersion::V1,
            Some("test/certs/cert.pem".into()),
            Some("test/certs/key.pem".into()),
            vec![],
        ))
        .expect("Failure creating settings");
        let server_handle = hyper_driver(settings)
            .unwrap()
            .start(|_| {
                let mut mock_handler = MockHandler::new();
                mock_handler.expect_handle().times(1).returning(|_| {
                    Box::pin(async move {
                        Ok(Response::builder()
                            .status(200)
                            .body(Body::from("Hello World"))
                            .unwrap())
                    })
                });

                mock_handler
            })
            .await
            .expect("Failure creating hyper driver");
        let close_handle = server_handle
            .fork()
            .expect("Failed to start server in another task");

        // When
        let config = ClientConfig::builder()
            .with_safe_defaults()
            .with_custom_certificate_verifier(Arc::new(DummyServerCertVerifier))
            .with_no_client_auth();
        let connector = HttpsConnectorBuilder::new()
            .with_tls_config(config)
            .https_only()
            .enable_http1()
            .build();
        let client: Client<HttpsConnector<HttpConnector>> = Client::builder().build(connector);
        let response = client
            .get(Uri::from_static("https://127.0.0.1:7884/api"))
            .await
            .expect("Failed to get response");
        let status = response.status();
        let body = hyper::body::to_bytes(response)
            .await
            .expect("Cannot receive response bytes");

        close_handle.close().await.expect("Failed to stop server");

        // Then
        assert_eq!(status, 200);
        assert_eq!(
            String::from_utf8(body.into()).expect("Response body is not UTF-8"),
            "Hello World"
        )
    }
}
