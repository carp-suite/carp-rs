use thiserror::Error;

#[derive(Error, Debug)]
pub enum SettingsError {
    #[error(
        "Failed to retrieve TLS certificate and private key: {0} was provided but {1} is missing"
    )]
    CertificateError(&'static str, &'static str),
    #[error("Failed to construct server url from \"host\", \"port\" and \"base_path\" settings")]
    UrlParseError(#[from] url::ParseError),
}
