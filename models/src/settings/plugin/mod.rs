use std::fmt::Debug;

use serde::Serialize;
use url::Url;

use self::error::PluginError;

pub mod error;

#[derive(Debug, PartialEq)]
pub struct Plugin {
    url: Url,
    name: String,
    configuration: Option<String>,
}

impl Plugin {
    pub fn url(&self) -> &Url {
        &self.url
    }
    pub fn name(&self) -> &str {
        self.name.as_str()
    }
    pub fn configuration(&self) -> Option<&str> {
        self.configuration.as_ref().map(String::as_str)
    }
}

impl<'a, T> TryFrom<(&'a str, String, Option<T>)> for Plugin
where
    T: Serialize + Debug,
{
    type Error = PluginError;

    fn try_from(
        (url, name, configuration): (&'a str, String, Option<T>),
    ) -> Result<Self, Self::Error> {
        let url = Url::try_from(url).map_err(PluginError::UrlParseError)?;

        if let Some(configuration) = configuration {
            let configuration = serde_json::to_string(&configuration).map_err(|err| {
                PluginError::ConfigurationSerializationError(format!("{:?}", configuration), err)
            })?;

            Ok(Plugin {
                url,
                name,
                configuration: Some(configuration),
            })
        } else {
            Ok(Plugin {
                url,
                name,
                configuration: None,
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_constructs_plugin_correctly() {
        // Given
        let plugin_tuple = ("file:///some/path", "Plugin".into(), Some(vec![1]));

        // When
        let plugin_result = Plugin::try_from(plugin_tuple);

        // Then
        assert!(plugin_result.is_ok());
    }

    #[test]
    fn it_returns_url_correctly() {
        // Given
        let plugin =
            Plugin::try_from(("file:///some/path", "Plugin".into(), Some(vec![1]))).unwrap();

        // When
        let url = plugin.url();

        // Then
        assert_eq!(url.as_str(), "file:///some/path");
    }

    #[test]
    fn it_returns_name_correctly() {
        // Given
        let plugin =
            Plugin::try_from(("file:///some/path", "Plugin".into(), Some(vec![1]))).unwrap();

        // When
        let name = plugin.name();

        // Then
        assert_eq!(name, "Plugin");
    }

    #[test]
    fn it_configuration_string_correctly() {
        // Given
        let plugin =
            Plugin::try_from(("file:///some/path", "Plugin".into(), Some(vec![1]))).unwrap();

        // When
        let configuration = plugin.configuration();

        // Then
        assert_eq!(configuration, Some("[1]"));
    }
}
