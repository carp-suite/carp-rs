use thiserror::Error;

#[derive(Error, Debug)]
pub enum PluginError {
    #[error("Could not parse plugin URL: {0}")]
    UrlParseError(#[from] url::ParseError),
    #[error("Could not serialize plugin configuration, provided object is {0}")]
    ConfigurationSerializationError(String, serde_json::Error),
}
