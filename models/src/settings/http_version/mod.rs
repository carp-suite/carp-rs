use std::fmt::Debug;

use self::error::HttpVersionNumberError;

pub mod error;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum HttpVersion {
    V1,
    V2,
}

impl<'a> TryFrom<usize> for HttpVersion {
    type Error = HttpVersionNumberError;

    fn try_from(raw_version: usize) -> Result<Self, Self::Error> {
        match raw_version {
            1 => Ok(HttpVersion::V1),
            2 => Ok(HttpVersion::V2),
            _ => Err(HttpVersionNumberError(raw_version)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_parses_version_2_correctly() {
        // Given
        let version_number = 2;

        // When
        let http_version = HttpVersion::try_from(version_number).unwrap();

        // Then
        assert_eq!(http_version, HttpVersion::V2)
    }

    #[test]
    fn it_parses_version_1_correctly() {
        // Given
        let version_number = 1;

        // When
        let http_version = HttpVersion::try_from(version_number).unwrap();

        // Then
        assert_eq!(http_version, HttpVersion::V1)
    }
}
