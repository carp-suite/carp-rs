use thiserror::Error;

#[derive(Error, Debug)]
#[error("Invalid HTTP Version: {0} must be either 1 or 2")]
pub struct HttpVersionNumberError(pub(crate) usize);
