use std::path::{Path, PathBuf};

use url::Url;

use self::{error::SettingsError, http_version::HttpVersion, plugin::Plugin};

pub mod error;
pub mod http_version;
pub mod plugin;

#[derive(Debug, PartialEq)]
pub struct Settings {
    base_address: Url,
    http_version: HttpVersion,
    cert: Option<(PathBuf, PathBuf)>,
    plugins: Vec<Plugin>,
}

impl Settings {
    pub fn port(&self) -> u16 {
        self.base_address.port().unwrap_or(7884)
    }
    pub fn host(&self) -> &str {
        self.base_address.host_str().unwrap_or("0.0.0.0")
    }
    pub fn base_path(&self) -> &str {
        self.base_address.path()
    }
    pub fn http_version_number(&self) -> HttpVersion {
        self.http_version
    }
    pub fn tls(&self) -> Option<(&Path, &Path)> {
        self.cert.as_ref().map(|(k, c)| (c.as_path(), k.as_path()))
    }
    pub fn plugins(&self) -> &[Plugin] {
        self.plugins.as_slice()
    }
    pub fn complete_address(&self) -> &str {
        self.base_address.as_str()
    }
}

impl<'a>
    TryFrom<(
        &'a str,
        HttpVersion,
        Option<PathBuf>,
        Option<PathBuf>,
        Vec<Plugin>,
    )> for Settings
{
    type Error = SettingsError;

    fn try_from(
        (raw_address, http_version, cert, key, plugins): (
            &'a str,
            HttpVersion,
            Option<PathBuf>,
            Option<PathBuf>,
            Vec<Plugin>,
        ),
    ) -> Result<Self, Self::Error> {
        let base_address = Url::try_from(raw_address)?;

        if key.is_none() && cert.is_some() {
            return Err(SettingsError::CertificateError(
                "certificate",
                "private key",
            ));
        }

        if cert.is_none() && key.is_some() {
            return Err(SettingsError::CertificateError(
                "private key",
                "certificate",
            ));
        }

        let cert = cert.zip(key);

        Ok(Settings {
            base_address,
            http_version,
            cert,
            plugins,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_constructs_settings_correctly() {
        // Given
        let settings_tuple = (
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![],
        );

        // When
        let settings_result = Settings::try_from(settings_tuple);

        // Then
        assert_eq!(settings_result.is_ok(), true);
    }

    #[test]
    fn it_returns_base_path_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![],
        ))
        .unwrap();

        // When
        let base_address = settings.base_path();

        // Then
        assert_eq!(base_address, "/api");
    }

    #[test]
    fn it_returns_base_port_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![],
        ))
        .unwrap();

        // When
        let port = settings.port();

        // Then
        assert_eq!(port, 4500);
    }

    #[test]
    fn it_returns_host_number_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![],
        ))
        .unwrap();

        // When
        let host = settings.host();

        // Then
        assert_eq!(host, "127.0.0.1");
    }

    #[test]
    fn it_returns_http_version_number_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![],
        ))
        .unwrap();

        // When
        let http_version = settings.http_version_number();

        // Then
        assert_eq!(http_version, HttpVersion::V2);
    }

    #[test]
    fn it_returns_authority_key_path_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            Some::<PathBuf>("/opt/ec/file".into()),
            Some::<PathBuf>("/opt/ec/key".into()),
            vec![],
        ))
        .unwrap();

        // When
        let (key_path, _) = settings.tls().unwrap();

        // Then
        assert_eq!(key_path, Path::new("/opt/ec/key"));
    }

    #[test]
    fn it_returns_certificate_path_correctly() {
        // Given
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            Some::<PathBuf>("/opt/ec/file".into()),
            Some::<PathBuf>("/opt/ec/key".into()),
            vec![],
        ))
        .unwrap();

        // When
        let (_, cert_path) = settings.tls().unwrap();

        // Then
        assert_eq!(cert_path, Path::new("/opt/ec/file"));
    }

    #[test]
    fn it_returns_plugins_correctly() {
        // Given
        let plugin =
            Plugin::try_from(("file:///some/path", "Plugin".into(), Some(vec![1]))).unwrap();
        let settings = Settings::try_from((
            "http://127.0.0.1:4500/api",
            HttpVersion::V2,
            None,
            None,
            vec![plugin],
        ))
        .unwrap();

        // When
        let plugins = settings.plugins();

        // Then
        assert_eq!(1, plugins.len());
    }
}
