#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum HttpMethod {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
    WILDCARD,
}
