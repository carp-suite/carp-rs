use std::borrow::{Borrow, BorrowMut};

use url::Url;

use self::{cache_usage::CacheUsage, http_method::HttpMethod, plugin_request::PluginRequest};

pub mod cache_usage;
pub mod http_method;
pub mod plugin_request;

#[derive(Clone, Debug, PartialEq)]
pub struct Route {
    target: Url,
    method: HttpMethod,
    cache_usage: Option<CacheUsage>,
    invalidates: Vec<String>,
    plugins: Option<PluginRequest>,
}

impl Route {
    pub fn target(&self) -> &Url {
        self.target.borrow()
    }

    pub fn target_mut(&mut self) -> &mut Url {
        self.target.borrow_mut()
    }

    pub fn cache_usage(&self) -> Option<&CacheUsage> {
        self.cache_usage.as_ref()
    }

    pub fn invalidates(&self) -> &[String] {
        self.invalidates.as_ref()
    }

    pub fn http_method(&self) -> HttpMethod {
        self.method
    }

    pub fn into_parts(self) -> (Url, Option<CacheUsage>, Vec<String>, HttpMethod) {
        (self.target, self.cache_usage, self.invalidates, self.method)
    }

    pub fn plugins(&self) -> Option<&PluginRequest> {
        self.plugins.as_ref()
    }
}

impl<'a>
    TryFrom<(
        &'a str,
        HttpMethod,
        Option<CacheUsage>,
        Vec<String>,
        Option<PluginRequest>,
    )> for Route
{
    type Error = url::ParseError;

    fn try_from(
        (path, method, cache_usage, invalidates, plugins): (
            &'a str,
            HttpMethod,
            Option<CacheUsage>,
            Vec<String>,
            Option<PluginRequest>,
        ),
    ) -> Result<Self, Self::Error> {
        let target = Url::parse(path)?;

        Ok(Route {
            target,
            method,
            cache_usage,
            invalidates,
            plugins,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_constructs_route_correctly() {
        // Given
        let cache_usage = CacheUsage::from(("Cache".to_owned(), vec![], vec![], vec![]));
        let route_tuple = (
            "http://127.0.0.1:4500/api",
            HttpMethod::GET,
            Some(cache_usage),
            vec![],
            None,
        );

        // When
        let route = Route::try_from(route_tuple);

        // Then
        assert!(route.is_ok());
    }

    #[test]
    fn it_returns_target_correctly() {
        // Given
        let route = Route::try_from((
            "http://127.0.0.1:4500/api",
            HttpMethod::GET,
            Some(CacheUsage::from((
                "Cache".to_owned(),
                vec![],
                vec![],
                vec![],
            ))),
            vec![],
            None,
        ))
        .unwrap();

        // When
        let target = route.target();

        // Then
        assert_eq!(&Url::parse("http://127.0.0.1:4500/api").unwrap(), target);
    }

    #[test]
    fn it_returns_invalidates_correctly() {
        // Given
        let route = Route::try_from((
            "http://127.0.0.1:4500/api",
            HttpMethod::GET,
            Some(CacheUsage::from((
                "Cache".to_owned(),
                vec![],
                vec![],
                vec![],
            ))),
            vec!["Caches".to_owned()],
            None,
        ))
        .unwrap();

        // When
        let invalidates = route.invalidates();

        // Then
        assert_eq!(invalidates, ["Caches"]);
    }

    #[test]
    fn it_returns_http_method_correctly() {
        // Given
        let route = Route::try_from((
            "http://127.0.0.1:4500/api",
            HttpMethod::GET,
            Some(CacheUsage::from((
                "Cache".to_owned(),
                vec![],
                vec![],
                vec![],
            ))),
            vec!["Caches".to_owned()],
            None,
        ))
        .unwrap();

        // When
        let http_method = route.http_method();

        // Then
        assert_eq!(http_method, HttpMethod::GET);
    }

    #[test]
    fn it_returns_cache_usage_correctly() {
        // Given
        let route = Route::try_from((
            "http://127.0.0.1:4500/api",
            HttpMethod::GET,
            Some(CacheUsage::from((
                "Cache".to_owned(),
                vec![],
                vec![],
                vec![],
            ))),
            vec!["Caches".to_owned()],
            None,
        ))
        .unwrap();

        // When
        let cache_usage = route.cache_usage();

        // Then
        assert_eq!(cache_usage.map(|cu| cu.cache()), Some("Cache"));
    }
}
