#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PluginRequest {
    request_start: Vec<String>,
    request_end: Vec<String>,
}

impl PluginRequest {
    pub fn request_start(&self) -> &[String] {
        self.request_start.as_ref()
    }

    pub fn request_end(&self) -> &[String] {
        self.request_end.as_ref()
    }
}

impl From<(Vec<String>, Vec<String>)> for PluginRequest {
    fn from((request_start, request_end): (Vec<String>, Vec<String>)) -> Self {
        PluginRequest {
            request_start,
            request_end,
        }
    }
}
