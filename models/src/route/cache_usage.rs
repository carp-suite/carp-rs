use std::borrow::Borrow;

#[derive(Clone, Debug, PartialEq)]
pub struct CacheUsage {
    cache: String,
    route_parameters: Vec<String>,
    query_parameters: Vec<String>,
    headers: Vec<String>,
}

impl CacheUsage {
    pub fn cache(&self) -> &str {
        self.cache.borrow()
    }

    pub fn route_parameters(&self) -> &[String] {
        self.route_parameters.as_ref()
    }

    pub fn query_parameters(&self) -> &[String] {
        self.query_parameters.as_ref()
    }

    pub fn headers(&self) -> &[String] {
        self.headers.as_ref()
    }
}

impl From<(String, Vec<String>, Vec<String>, Vec<String>)> for CacheUsage {
    fn from(
        (cache, route_parameters, query_parameters, headers): (
            String,
            Vec<String>,
            Vec<String>,
            Vec<String>,
        ),
    ) -> Self {
        CacheUsage {
            cache,
            route_parameters,
            query_parameters,
            headers,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_returns_cache_name_correctly() {
        // Given
        let usage = CacheUsage::from(("Cache".into(), vec![], vec![], vec![]));

        // When
        let cache_name = usage.cache();

        // Then
        assert_eq!("Cache", cache_name);
    }

    #[test]
    fn it_returns_route_parameters_correctly() {
        // Given
        let usage = CacheUsage::from(("".into(), vec!["id".into(), "user".into()], vec![], vec![]));

        // When
        let route_parameters = usage.route_parameters();

        // Then
        assert_eq!(["id".to_owned(), "user".to_owned()], route_parameters);
    }

    #[test]
    fn it_returns_query_parameters_correctly() {
        // Given
        let usage = CacheUsage::from(("".into(), vec![], vec!["id".into(), "user".into()], vec![]));

        // When
        let query_parameters = usage.query_parameters();

        // Then
        assert_eq!(["id".to_owned(), "user".to_owned()], query_parameters);
    }

    #[test]
    fn it_returns_headers_correctly() {
        // Given
        let usage = CacheUsage::from(("".into(), vec![], vec![], vec!["id".into(), "user".into()]));

        // When
        let headers = usage.headers();

        // Then
        assert_eq!(["id".to_owned(), "user".to_owned()], headers);
    }
}
