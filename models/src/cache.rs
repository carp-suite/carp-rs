use std::borrow::Borrow;

#[derive(Debug, PartialEq, Clone)]
pub struct Cache {
    name: String,
    ttl: Option<u64>,
    dependencies: Vec<String>,
    override_plugins: Vec<String>,
    plugins: Vec<String>,
}

impl Cache {
    pub fn name(&self) -> &str {
        self.name.borrow()
    }

    pub fn ttl(&self) -> Option<u64> {
        self.ttl
    }

    pub fn dependencies(&self) -> &[String] {
        self.dependencies.as_slice()
    }

    pub fn into_parts(self) -> (String, Option<u64>, Vec<String>) {
        (self.name, self.ttl, self.dependencies)
    }

    pub fn override_plugins(&self) -> &[String] {
        self.override_plugins.as_ref()
    }

    pub fn plugins(&self) -> &[String] {
        self.plugins.as_ref()
    }
}

impl From<(String, Option<u64>, Vec<String>, Vec<String>, Vec<String>)> for Cache {
    fn from(
        (name, ttl, dependencies, override_plugins, plugins): (
            String,
            Option<u64>,
            Vec<String>,
            Vec<String>,
            Vec<String>,
        ),
    ) -> Self {
        Cache {
            name,
            ttl,
            dependencies,
            override_plugins,
            plugins,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_returns_name_correctly() {
        // Given
        let cache: Cache = ("My Cache".into(), Some(0), vec![], vec![], vec![]).into();

        // When
        let name = cache.name();

        // Then
        assert_eq!(name, "My Cache")
    }

    #[test]
    fn it_returns_ttl_correctly() {
        // Given
        let cache: Cache = ("".into(), Some(64200), vec![], vec![], vec![]).into();

        // When
        let ttl = cache.ttl();

        // Then
        assert_eq!(ttl, Some(64200));
    }

    #[test]
    fn it_returns_dependencies_correctly() {
        // Given
        let cache: Cache = ("".into(), None, vec!["Other Cache".into()], vec![], vec![]).into();

        // When
        let dependencies = cache.dependencies();

        // Then
        assert_eq!(dependencies, vec!["Other Cache"])
    }
}
