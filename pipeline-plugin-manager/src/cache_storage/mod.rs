use crate::driver::Driver;
use bytes::Bytes;
use futures::{Future, Stream};
use http::Response;
use pipeline::caching::storage::CacheStorage;
use std::sync::Arc;
use std::{error::Error as StdError, sync::atomic::AtomicBool};

use self::error::Error;

pub mod error;

pub struct PluginCacheStorage<Inner> {
    id: Arc<[u8]>,
    name: Arc<str>,
    overrides: Arc<[Arc<Driver>]>,
    plugins: Arc<[Arc<Driver>]>,
    inner: Arc<Inner>,
    valid: Arc<AtomicBool>,
}

impl<Inner> PluginCacheStorage<Inner> {
    pub fn new(
        id: Arc<[u8]>,
        name: Arc<str>,
        overrides: Arc<[Arc<Driver>]>,
        plugins: Arc<[Arc<Driver>]>,
        inner: Inner,
    ) -> Self {
        PluginCacheStorage {
            id,
            name,
            overrides: overrides.into(),
            plugins: plugins.into(),
            inner: Arc::new(inner),
            valid: Arc::new(AtomicBool::new(false)),
        }
    }
}

impl<Body, ResFut, ResFutErr, Inner> CacheStorage<Body, ResFut> for PluginCacheStorage<Inner>
where
    Inner: CacheStorage<Body, ResFut> + Send + Sync + 'static,
    Body: From<
            Box<
                dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                    + Send
                    + 'static,
            >,
        > + http_body::Body
        + From<Vec<u8>>
        + Send
        + Unpin
        + 'static,
    Bytes: From<Body::Data>,
    Body::Error: StdError + Send + Sync + 'static,
    ResFut: Future<Output = Result<Response<Body>, ResFutErr>>,
    ResFutErr: StdError + Send + Sync + 'static,
    Inner::Error: StdError + Send + Sync + 'static,
{
    type Error = Error<Body::Error, Inner::Error, ResFutErr>;
    type LoadOrStoreFuture = impl Future<Output = Result<Response<Body>, Self::Error>>;
    type InvalidateFuture = impl Future<Output = Result<(), Self::Error>>;

    fn load_or_store(&self, response_future: ResFut) -> Self::LoadOrStoreFuture {
        let overrides = Arc::clone(&self.overrides);
        let plugins = Arc::clone(&self.plugins);
        let id = Arc::clone(&self.id);
        let name = Arc::clone(&self.name);
        let inner = Arc::clone(&self.inner);
        let valid = Arc::clone(&self.valid);

        async move {
            let valid_value = valid.load(std::sync::atomic::Ordering::Acquire);
            for driver in plugins.iter() {
                let driver = driver.clone();
                if let Some(response) = driver
                    .cache_start(&id, &name)
                    .await
                    .map_err(Error::DriverReqBodyError)?
                {
                    return Ok(response);
                }
            }

            let mut response = match (&*overrides, valid_value) {
                (&[], _) => inner
                    .load_or_store(response_future)
                    .await
                    .map_err(Error::InnerError)?,
                (&[_, ..], true) => {
                    for dirver in overrides.iter() {
                        let Some(response) = dirver.cache_start(&id, &name).await.map_err(Error::DriverReqBodyError)? else {
                            break;
                        };

                        return Ok(response);
                    }

                    inner
                        .load_or_store(response_future)
                        .await
                        .map_err(Error::InnerError)?
                }
                (&[_, ..], false) => {
                    let mut response = response_future
                        .await
                        .map_err(Error::ResponseObtentionError)?;

                    for driver in overrides.iter() {
                        driver
                            .cache_end(&id, &mut response)
                            .await
                            .map_err(Error::DriverReqBodyError)?;
                    }

                    valid.store(true, std::sync::atomic::Ordering::Release);

                    response
                }
            };

            for plugin in plugins.iter() {
                plugin
                    .cache_end(&id, &mut response)
                    .await
                    .map_err(Error::DriverReqBodyError)?;
            }

            return Ok(response);
        }
    }

    fn invalidate(&self) -> Self::InvalidateFuture {
        let inner = Arc::clone(&self.inner);
        let valid = Arc::clone(&self.valid);
        async move {
            inner.invalidate().await.map_err(Error::InnerError)?;
            valid.store(false, std::sync::atomic::Ordering::Relaxed);

            Ok(())
        }
    }
}
