use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error<BodyError, InnerError, ResFutError>
where
    BodyError: std::error::Error,
    InnerError: std::error::Error,
    ResFutError: std::error::Error,
{
    #[error("Error occured while calling plugin")]
    DriverReqBodyError(#[source] crate::driver::error::Error<BodyError>),
    #[error("Error occured while storing/loading response")]
    InnerError(#[source] InnerError),
    #[error("Error occured while obtaining response")]
    ResponseObtentionError(#[source] ResFutError),
}
