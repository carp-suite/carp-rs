use self::error::{Error, SetupError};
use either::Either;
use futures::Stream;
use http::{Request, Response, Uri};
use http_body::Body as HttpBody;
use hyper::body::Bytes;
use models::settings::plugin::Plugin;
use std::{error::Error as StdError, sync::Arc};
use tokio::{
    net::UnixStream,
    sync::{mpsc, oneshot},
};
use tokio_stream::wrappers::UnboundedReceiverStream;
use tonic::transport::{Channel, Endpoint};
use tower::service_fn;
use uuid::Uuid;
use wapc::WapcHost;
use wapc_pool::{HostPool, HostPoolBuilder};
use wasmtime_provider::WasmtimeEngineProviderBuilder;

pub mod error;
pub(crate) mod grpc;
pub(crate) mod wasm;

type StreamRequest<T> = mpsc::Sender<grpc::StreamingRequest<T>>;
type StreamSender<T> = mpsc::UnboundedSender<T>;

pub enum DriverType {
    Wasm(HostPool),
    Grpc {
        plugin_client: grpc::main::plugin_client::PluginClient<Channel>,
        request_start_sender: StreamSender<grpc::main::HttpRequest>,
        request_start_stream_request: StreamRequest<grpc::main::RequestStartResult>,
        request_end_sender: StreamSender<grpc::main::HttpResponse>,
        request_end_stream_request: StreamRequest<grpc::main::RequestEndResult>,
        cache_start_sender: StreamSender<grpc::main::CacheRequest>,
        cache_start_stream_request: StreamRequest<grpc::main::CacheStartResult>,
        cache_end_sender: StreamSender<grpc::main::CacheResponse>,
        cache_end_stream_request: StreamRequest<grpc::main::CacheVoidResponse>,
    },
}

pub struct Driver {
    driver_type: DriverType,
    name: String,
}

impl std::fmt::Debug for Driver {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.name)
    }
}

impl Driver {
    pub async fn try_from_plugin(value: &Plugin) -> Result<Self, SetupError> {
        match value.url().scheme() {
            "file" | "" => {
                log::debug!("Using WASM driver plugin {}", value.name());
                let file_contents = tokio::fs::read(value.url().path()).await?;
                let engine = WasmtimeEngineProviderBuilder::new()
                    .module_bytes(&file_contents)
                    .build()
                    .map_err(|err| {
                        SetupError::WasmtimeProviderError(value.name().to_string(), err)
                    })?;
                let configuration_bytes: Option<Arc<[u8]>> = value
                    .configuration()
                    .map(|str| str.as_bytes().to_vec().into());
                let name: Arc<str> = value.name().to_string().into();

                let host = HostPoolBuilder::new()
                    .name(format!("wasm-plugin-{}-pool", value.name()))
                    .factory(move || {
                        let engine = engine.clone();
                        let host = WapcHost::new(Box::new(engine), None).unwrap();

                        if let Some(configuration) = configuration_bytes.clone() {
                            host.call("configure", &*configuration)
                                .map_err(|err| {
                                    SetupError::WapcConfigureError(name.clone().to_string(), err)
                                })
                                .unwrap();
                        }

                        host
                    })
                    .max_threads(2)
                    .build();

                Ok(Driver {
                    name: value.name().to_string(),
                    driver_type: DriverType::Wasm(host),
                })
            }
            "grpc" => {
                let mut client = if value.url().has_host() {
                    grpc::main::plugin_client::PluginClient::connect(value.url().to_string())
                        .await
                        .map_err(|err| {
                            SetupError::GrpcConnectionError(
                                value.url().to_string(),
                                value.name().to_string(),
                                err,
                            )
                        })?
                } else {
                    let unix_path = value.url().path().to_string();
                    let endpoint = Endpoint::try_from("http://[::]:50051")
                        .map_err(|err| {
                            SetupError::GrpcPathError(
                                value.name().to_string(),
                                value.url().to_string(),
                                err,
                            )
                        })?
                        .connect_with_connector(service_fn(move |_| {
                            UnixStream::connect(unix_path.clone())
                        }))
                        .await
                        .map_err(|err| {
                            SetupError::GrpcConnectionError(
                                value.url().to_string(),
                                value.name().to_string(),
                                err,
                            )
                        })?;

                    grpc::main::plugin_client::PluginClient::new(endpoint)
                }
                .max_decoding_message_size(1024 * 1024 * 1024)
                .max_encoding_message_size(1024 * 1024 * 1024);

                if let Some(configuration) = value.configuration() {
                    let configuration_request =
                        tonic::Request::new(grpc::main::ConfigurationRequest {
                            json_string: configuration.to_string(),
                        });
                    client
                        .configure(configuration_request)
                        .await
                        .map_err(|err| {
                            SetupError::GrpcConfigureError(
                                value.name().to_string(),
                                configuration.to_string(),
                                err,
                            )
                        })?;
                }

                log::debug!("Connected to gRPC server request/start");
                let (request_start_sender, request_start_receiver) = mpsc::unbounded_channel();
                let mut server_client = client.clone();
                let request_start_stream_request = grpc::plugin_server(async move {
                    server_client
                        .request_start(UnboundedReceiverStream::new(request_start_receiver))
                        .await
                });

                log::debug!("Connected to gRPC server request/end");
                let (request_end_sender, request_end_receiver) = mpsc::unbounded_channel();
                let mut server_client = client.clone();
                let request_end_stream_request = grpc::plugin_server(async move {
                    server_client
                        .request_end(UnboundedReceiverStream::new(request_end_receiver))
                        .await
                });

                let (cache_start_sender, cache_start_receiver) = mpsc::unbounded_channel();
                let mut server_client = client.clone();
                let cache_start_stream_request = grpc::plugin_server(async move {
                    server_client
                        .cache_start(UnboundedReceiverStream::new(cache_start_receiver))
                        .await
                });

                let (cache_end_sender, cache_end_receiver) = mpsc::unbounded_channel();
                let mut server_client = client.clone();
                let cache_end_stream_request = grpc::plugin_server(async move {
                    server_client
                        .cache_end(UnboundedReceiverStream::new(cache_end_receiver))
                        .await
                });

                Ok(Driver {
                    name: value.name().to_string(),
                    driver_type: DriverType::Grpc {
                        plugin_client: client,
                        request_start_sender,
                        request_start_stream_request,
                        request_end_sender,
                        request_end_stream_request,
                        cache_start_sender,
                        cache_start_stream_request,
                        cache_end_sender,
                        cache_end_stream_request,
                    },
                })
            }
            _ => Err(SetupError::UnknownPluginScheme(
                value.url().scheme().to_string(),
            )),
        }
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub async fn request_start<ReqBody, ResBody>(
        &self,
        id: &Arc<Uuid>,
        uri: &Uri,
        request: &mut Request<ReqBody>,
    ) -> Result<Option<Either<Request<ReqBody>, Response<ResBody>>>, Error<ReqBody::Error>>
    where
        ReqBody: HttpBody
            + From<Vec<u8>>
            + From<Bytes>
            + From<
                Box<
                    dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                        + Send
                        + 'static,
                >,
            > + Unpin,
        ReqBody::Error: StdError + Send + Sync + 'static,
        ResBody: HttpBody
            + From<Vec<u8>>
            + From<Bytes>
            + From<
                Box<
                    dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                        + Send
                        + 'static,
                >,
            > + Unpin,
        ResBody::Error: StdError + Send + Sync + 'static,
    {
        match self.driver_type {
            DriverType::Wasm(ref host) => {
                let (payload, request_body) = wasm::request::to_flatbuffers(&*id, request)
                    .await
                    .map_err(|err| {
                        Error::WasmPluginError(
                            self.name.to_string(),
                            "request_start".to_owned(),
                            err,
                        )
                    })?;
                let mut bytes = host.call("request_start", payload).await?;

                if bytes.is_empty() {
                    *request.body_mut() = ReqBody::from(request_body);
                    return Ok(None);
                }

                // Very weird bug regarding the rust buffer ALWAYS missing 4 bytes at the end of
                // the flatbuffers buffer.
                bytes.extend([0, 0, 0, 0]);

                wasm::request::from_flatbuffers::<_, ReqBody::Error>(bytes.clone(), uri)
                    .map(|req| Some(Either::Left(req)))
                    .or_else(|_| {
                        wasm::response::from_flatbuffers(bytes.clone())
                            .map(|resp| Some(Either::Right(resp)))
                    })
                    .map_err(|err| {
                        Error::WasmPluginError(
                            self.name.to_string(),
                            "request_start".to_owned(),
                            err,
                        )
                    })
            }
            DriverType::Grpc {
                ref request_start_sender,
                ref request_start_stream_request,
                ..
            } => {
                let grpc_request =
                    grpc::request::to_protobuf(&*id, request)
                        .await
                        .map_err(|err| {
                            Error::GrpcPluginError(
                                self.name.to_string(),
                                "request_start".to_owned(),
                                err,
                            )
                        })?;
                let grpc_request_body = grpc_request.body.clone();
                log::debug!("Sending gRPC request {:?}", grpc_request);
                request_start_sender.send(grpc_request).ok();
                let (tx, rx) = oneshot::channel();
                request_start_stream_request
                    .send(grpc::StreamingRequest {
                        id: Arc::clone(id),
                        receiver: tx,
                    })
                    .await
                    .ok();
                let mut request_start_receiver = rx.await.unwrap();

                let id = Arc::clone(id);

                match request_start_receiver.recv().await.transpose()? {
                    Some(grpc::main::RequestStartResult {
                        result: Some(grpc::main::request_start_result::Result::Response(response)),
                    }) => Ok(Some(Either::Right(
                        grpc::response::from_protobuf(
                            response,
                            async_stream::try_stream! {
                                while let Some(fragments) = request_start_receiver.recv().await.transpose()? {
                                    yield match fragments {
                                        grpc::main::RequestStartResult {
                                            result:
                                                Some(grpc::main::request_start_result::Result::Response(
                                                    response,
                                                )),
                                        } if response.id == id.as_bytes() => Some(response.body),
                                        grpc::main::RequestStartResult { result: _ } => {
                                            Err(grpc::error::Error::ProtocolError("HttpResponse"))?
                                        }
                                    }
                                }
                            }
                        )
                        .map_err(|err| {
                            Error::GrpcPluginError(
                                self.name.to_string(),
                                "request_start".to_owned(),
                                err,
                            )
                        })?,
                    ))),
                    Some(grpc::main::RequestStartResult {
                        result: Some(grpc::main::request_start_result::Result::Request(request)),
                    }) => {
                        Ok(Some(Either::Left(
                            grpc::request::from_protobuf(
                                uri,
                                request,
                                async_stream::try_stream! {
                                    while let Some(fragments) = request_start_receiver.recv().await.transpose()? {
                                        yield match fragments {
                                            grpc::main::RequestStartResult {
                                                result:
                                                    Some(grpc::main::request_start_result::Result::Request(
                                                        request,
                                                    )),
                                            } => Some(request.body),
                                            grpc::main::RequestStartResult { result: _ } => {
                                                Err(grpc::error::Error::ProtocolError("HttpRequest"))?
                                            }
                                        }
                                    }
                                }
                            )
                            .map_err(|err| {
                                Error::GrpcPluginError(
                                    self.name.to_string(),
                                    "request_start".to_owned(),
                                    err,
                                )
                            })?,
                        )))
                    }
                    Some(grpc::main::RequestStartResult {
                        result: Some(grpc::main::request_start_result::Result::Void(_)),
                    }) => {
                        *request.body_mut() = ReqBody::from(grpc_request_body);

                        Ok(None)
                    }
                    None | Some(_) => Err(Error::GrpcInvalidChunk(self.name.to_string())),
                }
            }
        }
    }

    pub async fn request_end<Body>(
        &self,
        id: &Arc<Uuid>,
        uri: &Uri,
        response: &mut Response<Body>,
    ) -> Result<Option<Response<Body>>, Error<Body::Error>>
    where
        Body: HttpBody
            + From<
                Box<
                    dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                        + Send
                        + 'static,
                >,
            > + From<Vec<u8>>
            + From<Bytes>
            + Unpin,
        Body::Error: StdError + Send + Sync + 'static,
    {
        match self.driver_type {
            DriverType::Wasm(ref host) => {
                let (payload, response_body) =
                    wasm::response::to_flatbuffers(id, uri.path(), response)
                        .await
                        .map_err(|err| {
                            Error::WasmPluginError(
                                self.name.to_string(),
                                "request_end".to_owned(),
                                err,
                            )
                        })?;
                let mut bytes = host.call("request_end", payload).await?;

                if bytes.is_empty() {
                    *response.body_mut() = Body::from(response_body);
                    return Ok(None);
                }

                // Very weird bug regarding the rust buffer ALWAYS missing 4 bytes at the end of
                // the flatbuffers buffer.
                bytes.extend([0, 0, 0, 0]);

                wasm::response::from_flatbuffers(bytes)
                    .map(Some)
                    .map_err(|err| {
                        Error::WasmPluginError(self.name.to_string(), "request_end".to_owned(), err)
                    })
            }
            DriverType::Grpc {
                ref request_end_sender,
                ref request_end_stream_request,
                ..
            } => {
                let grpc_response = grpc::response::to_protobuf(id, uri.path(), response)
                    .await
                    .map_err(|err| {
                        Error::GrpcPluginError(self.name.to_string(), "request_end".to_owned(), err)
                    })?;
                let grpc_response_body = grpc_response.body.clone();
                request_end_sender.send(grpc_response).ok();
                let (tx, rx) = oneshot::channel();
                request_end_stream_request
                    .send(grpc::StreamingRequest {
                        id: Arc::clone(id),
                        receiver: tx,
                    })
                    .await
                    .ok();
                let mut request_end_receiver = rx.await.unwrap();
                let id = Arc::clone(id);

                match request_end_receiver.recv().await.transpose()? {
                    Some(grpc::main::RequestEndResult {
                        result: Some(grpc::main::request_end_result::Result::Response(response)),
                    }) => {
                        Ok(Some(
                            grpc::response::from_protobuf(
                                response,
                                async_stream::try_stream! {
                                    while let Some(fragments) = request_end_receiver.recv().await.transpose()? {
                                        yield match fragments {
                                            grpc::main::RequestEndResult {
                                                result:
                                                    Some(grpc::main::request_end_result::Result::Response(
                                                        response,
                                                    )),
                                            } if response.id == id.as_bytes() => Some(response.body),
                                            grpc::main::RequestEndResult { result: _ } => {
                                                Err(grpc::error::Error::ProtocolError("HttpResponse"))?
                                            }
                                        }
                                    }
                                }
                            )
                            .map_err(|err| {
                                Error::GrpcPluginError(
                                    self.name.to_string(),
                                    "request_end".to_string(),
                                    err,
                                )
                            })?,
                        ))
                    }
                    Some(grpc::main::RequestEndResult {
                        result: Some(grpc::main::request_end_result::Result::Void(_)),
                    }) => {
                        *response.body_mut() = Body::from(grpc_response_body);
                        Ok(None)
                    }
                    None | Some(_) => Err(Error::GrpcInvalidChunk(self.name.to_string())),
                }
            }
        }
    }

    pub async fn cache_start<Id, Body, Err>(
        &self,
        id: &Id,
        cache_name: &str,
    ) -> Result<Option<Response<Body>>, Error<Err>>
    where
        Id: AsRef<[u8]> + Send + Clone + 'static,
        Body: From<
            Box<
                dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                    + Send
                    + 'static,
            >,
        >,
        Err: StdError + Send + Sync + 'static,
    {
        match self.driver_type {
            DriverType::Wasm(_) => Ok(None),
            DriverType::Grpc {
                ref cache_start_sender,
                ref cache_start_stream_request,
                ..
            } => {
                let request = grpc::main::CacheRequest {
                    cache_name: cache_name.to_string(),
                    id: Id::as_ref(id).to_vec(),
                };
                log::debug!("Sending cache_start request with id {:02X?}", id.as_ref());
                cache_start_sender.send(request).ok();

                let (tx, rx) = oneshot::channel();
                cache_start_stream_request
                    .send(grpc::StreamingRequest {
                        id: Arc::new(Uuid::from_slice(id.as_ref()).unwrap()),
                        receiver: tx,
                    })
                    .await
                    .ok();
                let mut cache_start_receiver = rx.await.unwrap();
                let id = id.clone();
                match cache_start_receiver.recv().await.transpose()? {
                    Some(grpc::main::CacheStartResult {
                        result: Some(grpc::main::cache_start_result::Result::Response(response)),
                    }) if response.id == id.as_ref() => {
                        log::debug!(
                            "Received cache_start response chunk with id {:02X?} and length {}",
                            id.as_ref(),
                            response.response_byte.len()
                        );
                        let stream = async_stream::try_stream! {
                            while let Some(fragments) = cache_start_receiver.recv().await.transpose()? {
                                match fragments {
                                    grpc::main::CacheStartResult {
                                        result:
                                            Some(grpc::main::cache_start_result::Result::Response(
                                                response,
                                            )),
                                    } if response.id == id.as_ref() && !response.response_byte.is_empty() => {
                                        log::debug!("Received cache_start response chunk with id {:02X?}", id.as_ref());
                                        yield response.response_byte
                                    },
                                    grpc::main::CacheStartResult {
                                        result:
                                            Some(grpc::main::cache_start_result::Result::Response(
                                                response,
                                            )),
                                    } if response.id == id.as_ref() && response.response_byte.is_empty() => {
                                        log::debug!("Received cache_start ending chunk with id {:02X?}", id.as_ref());
                                        yield vec![];
                                        break;
                                    },
                                    _ => Err(grpc::error::Error::ProtocolError("CacheResponse"))?
                                }
                            }
                        };
                        Ok(Some(
                            grpc::response::from_cache_response(response, Box::pin(stream))
                                .await
                                .map_err(|err| {
                                    Error::GrpcPluginError(
                                        self.name.to_string(),
                                        "cache_start".to_string(),
                                        err,
                                    )
                                })?,
                        ))
                    }
                    Some(grpc::main::CacheStartResult {
                        result: Some(grpc::main::cache_start_result::Result::Void(_)),
                    }) => Ok(None),
                    None | Some(_) => Err(Error::GrpcInvalidChunk(self.name.to_string())),
                }
            }
        }
    }

    pub async fn cache_end<Id, Body, Err>(
        &self,
        id: &Id,
        response: &mut Response<Body>,
    ) -> Result<(), Error<Err>>
    where
        Id: AsRef<[u8]>,
        Body: http_body::Body + From<Vec<u8>> + Send + Unpin + 'static,
        Bytes: From<Body::Data>,
        Err: StdError,
    {
        match self.driver_type {
            DriverType::Wasm(_) => Ok(()),
            DriverType::Grpc {
                ref cache_end_sender,
                ..
            } => {
                log::debug!("Sending cache_end response with id {:02X?}", id.as_ref());
                let response_body =
                    grpc::response::flush_response_to_stream(id, response, cache_end_sender)
                        .await
                        .map_err(|err| {
                            Error::GrpcPluginError(
                                self.name.to_string(),
                                "cache_end".to_owned(),
                                err,
                            )
                        })?;

                *response.body_mut() = Body::from(response_body);
                Ok(())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use hyper::Body;

    use super::*;

    #[tokio::test]
    async fn it_loads_wasm_plugin_and_calls_request_start() {
        // Given
        let plugin = Plugin::try_from((
            "file:///home/mk_n1/dev/carp-suite/carp/pipeline-plugin-manager/tests/fixtures/carp-jwt-plugin.wasm",
            "carp-jwt-plugin".to_string(),
            Some(HashMap::<&str, &str>::from_iter(vec![
                ("cookieName", "JWT"),
                ("secretKey", "hello"),
            ])),
        ))
        .unwrap();
        let driver = Driver::try_from_plugin(&plugin).await.unwrap();
        let mut request = http::Request::builder()
            .uri("http://www.google.com/api")
            .header("Cookie", "JWT=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.ElsKKULlzGtesThefMuj2_a6KIY9L5i2zDrBLHV-e0M")
            .body(Body::empty())
            .unwrap();
        let id = uuid::uuid!("38222c85-e69c-4e7c-a039-dacbb8cdf81e");
        let uri = request.uri().clone();

        // When
        let overriden_request = driver
            .request_start::<hyper::Body, hyper::Body>(&Arc::new(id), &uri, &mut request)
            .await
            .unwrap()
            .unwrap()
            .unwrap_left();

        // Then
        assert_eq!(overriden_request.headers().get("Authorization").unwrap().to_str().unwrap(), "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.ElsKKULlzGtesThefMuj2_a6KIY9L5i2zDrBLHV-e0M");
        assert!(!overriden_request.headers().contains_key("Cookie"));
        assert_eq!(overriden_request.uri(), request.uri());
        assert_eq!(
            http_body::Body::size_hint(overriden_request.body()).exact(),
            Some(0)
        );
    }

    #[tokio::test]
    async fn it_loads_wasm_plugin_and_calls_request_start_with_malformed_token() {
        // Given
        let plugin = Plugin::try_from((
            "file:///home/mk_n1/dev/carp-suite/carp/pipeline-plugin-manager/tests/fixtures/carp-jwt-plugin.wasm",
            "carp-jwt-plugin".to_string(),
            Some(HashMap::<&str, &str>::from_iter(vec![
                ("cookieName", "JWT"),
                ("secretKey", "hello"),
            ])),
        ))
        .unwrap();
        let driver = Driver::try_from_plugin(&plugin).await.unwrap();
        let mut request = http::Request::builder()
            .uri("http://www.google.com")
            .header("Content-Length", "0")
            .header("Host", "localhost")
            .header("Cookie", "JWT=tokenMalformed")
            .body(Body::empty())
            .unwrap();
        let id = uuid::uuid!("38222c85-e69c-4e7c-a039-dacbb8cdf81e");
        let uri = request.uri().clone();

        // When
        let received_response = driver
            .request_start::<hyper::Body, hyper::Body>(&Arc::new(id), &uri, &mut request)
            .await
            .unwrap()
            .unwrap()
            .unwrap_right();

        // Then
        assert_eq!(received_response.status(), 403);
        assert_eq!(
            hyper::body::to_bytes(received_response.into_body())
                .await
                .unwrap(),
            Bytes::from_static(b"jwt: one part only\xe2\x80\x94payload absent")
        )
    }

    #[tokio::test]
    async fn it_loads_wasm_plugin_and_calls_request_start_without_cookie() {
        // Given
        let plugin = Plugin::try_from((
            "file:///home/mk_n1/dev/carp-suite/carp/pipeline-plugin-manager/tests/fixtures/carp-jwt-plugin.wasm",
            "carp-jwt-plugin".to_string(),
            Some(HashMap::<&str, &str>::from_iter(vec![
                ("cookieName", "JWT1"),
                ("secretKey", "hello"),
            ])),
        ))
        .unwrap();
        let driver = Driver::try_from_plugin(&plugin).await.unwrap();
        let mut request = http::Request::builder()
            .uri("http://www.google.com")
            .header("Content-Length", "0")
            .header("Host", "localhost")
            .header("Cookie", "JWT=tokenMalformed")
            .body(Body::empty())
            .unwrap();
        let id = uuid::uuid!("38222c85-e69c-4e7c-a039-dacbb8cdf81e");
        let uri = request.uri().clone();

        // When
        let received_response = driver
            .request_start::<hyper::Body, hyper::Body>(&Arc::from(id), &uri, &mut request)
            .await
            .unwrap()
            .unwrap()
            .unwrap_right();

        // Then
        assert_eq!(received_response.status(), 403);
        assert_eq!(
            hyper::body::to_bytes(received_response.into_body())
                .await
                .unwrap(),
            Bytes::from_static(b"JWT cookie not found")
        )
    }

    #[tokio::test]
    async fn it_loads_wasm_plugin_and_calls_request_end() {
        use serde::{Deserialize, Serialize};

        #[derive(Serialize, Deserialize, Debug)]
        #[serde(transparent)]
        struct IntOrString(#[serde(with = "either::serde_untagged")] Either<String, u32>);

        // Given
        let plugin = Plugin::try_from((
            "file:///home/mk_n1/dev/carp-suite/carp/pipeline-plugin-manager/tests/fixtures/carp-jwt-plugin.wasm",
            "carp-jwt-plugin".to_string(),
            Some(HashMap::<&str, IntOrString>::from_iter(vec![
                ("cookieName", IntOrString(Either::Left("JWT".to_string()))),
                ("secretKey", IntOrString(Either::Left("hello".to_string()))),
                ("path", IntOrString(Either::Left("/api/login".to_string()))),
                ("status", IntOrString(Either::Right(200)))
            ])),
        ))
        .unwrap();
        let driver = Driver::try_from_plugin(&plugin).await.unwrap();
        let mut response = http::Response::builder()
            .body(Body::from("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.ElsKKULlzGtesThefMuj2_a6KIY9L5i2zDrBLHV-e0M"))
            .unwrap();
        let id = uuid::uuid!("38222c85-e69c-4e7c-a039-dacbb8cdf81e");

        // When
        let mut received_response = driver
            .request_end(
                &Arc::new(id),
                &Uri::try_from("http://localhost/api/login").unwrap(),
                &mut response,
            )
            .await
            .unwrap()
            .unwrap();

        // Then
        assert_eq!(received_response.status(), 200);
        assert_eq!(
            hyper::body::to_bytes(received_response.body_mut())
                .await
                .unwrap(),
            Bytes::from_static(b"")
        );
        assert_eq!(received_response.headers().get("Set-Cookie").unwrap(), "JWT=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.ElsKKULlzGtesThefMuj2_a6KIY9L5i2zDrBLHV-e0M");
    }
}
