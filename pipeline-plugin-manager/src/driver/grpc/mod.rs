use std::pin::pin;
use std::{collections::HashMap, sync::Arc};

use futures::future::Future;
use futures::StreamExt;
use tokio::sync::{mpsc, oneshot};
use uuid::Uuid;

pub mod main {
    tonic::include_proto!("main");
}
pub mod error;
pub mod request;
pub mod response;

pub trait Chunk<T> {
    fn id(&self) -> T;
    fn is_last(&self) -> bool;
}

impl Chunk<Uuid> for main::RequestStartResult {
    fn id(&self) -> Uuid {
        match self.result.as_ref().unwrap() {
            main::request_start_result::Result::Response(resp) => {
                Uuid::from_slice(resp.id.as_ref()).unwrap()
            }
            main::request_start_result::Result::Request(req) => {
                Uuid::from_slice(req.id.as_ref()).unwrap()
            }
            main::request_start_result::Result::Void(void) => {
                Uuid::from_slice(void.id.as_ref()).unwrap()
            }
        }
    }

    fn is_last(&self) -> bool {
        true
    }
}

impl Chunk<Uuid> for main::RequestEndResult {
    fn id(&self) -> Uuid {
        match self.result.as_ref().unwrap() {
            main::request_end_result::Result::Response(resp) => {
                Uuid::from_slice(resp.id.as_ref()).unwrap()
            }
            main::request_end_result::Result::Void(void) => {
                Uuid::from_slice(void.id.as_ref()).unwrap()
            }
        }
    }

    fn is_last(&self) -> bool {
        true
    }
}

impl Chunk<Uuid> for main::CacheStartResult {
    fn id(&self) -> Uuid {
        match self.result.as_ref().unwrap() {
            main::cache_start_result::Result::Response(resp) => {
                Uuid::from_slice(resp.id.as_ref()).unwrap()
            }
            main::cache_start_result::Result::Void(void) => {
                Uuid::from_slice(void.id.as_ref()).unwrap()
            }
        }
    }

    fn is_last(&self) -> bool {
        match self.result.as_ref().unwrap() {
            main::cache_start_result::Result::Response(resp) => resp.response_byte.len() == 0,
            main::cache_start_result::Result::Void(_) => true,
        }
    }
}

impl Chunk<Uuid> for main::CacheVoidResponse {
    fn id(&self) -> Uuid {
        Uuid::default()
    }

    fn is_last(&self) -> bool {
        true
    }
}

pub struct StreamingRequest<T> {
    pub id: Arc<Uuid>,
    pub receiver: oneshot::Sender<mpsc::Receiver<Result<T, tonic::Status>>>,
}

pub fn plugin_server<Fut, T>(future: Fut) -> mpsc::Sender<StreamingRequest<T>>
where
    Fut: Future<Output = Result<tonic::Response<tonic::Streaming<T>>, tonic::Status>>
        + Send
        + 'static,
    T: Chunk<Uuid> + Sync + Send + 'static,
{
    let (tx, mut rx) = mpsc::channel::<StreamingRequest<T>>(100_000);
    let mut mapping = HashMap::<Arc<Uuid>, mpsc::Sender<Result<T, tonic::Status>>>::new();
    tokio::spawn(async move {
        let Ok(streaming) = future.await else {
            return;
        };
        let mut streaming = streaming.into_inner();
        loop {
            let next = streaming.next();
            let recv = pin!(rx.recv());

            tokio::select! {
                chunk_result = next  => match chunk_result {
                    Some(Ok(chunk)) => {
                        let id = chunk.id();
                        let is_last = chunk.is_last();

                        let tx_option = mapping.get(&id);

                        if let Some(tx) = tx_option {
                            tx.send(Ok(chunk)).await.ok();
                        }

                        if is_last {
                            mapping.remove(&id);
                        }

                    }
                    Some(Err(err)) => log::error!("gRPC receive error {:?}", err),
                    None => break
                },
                Some(request) = recv => {
                    let (tx, rx) = mpsc::channel(100_000);
                    mapping.insert(request.id, tx);
                    request.receiver.send(rx).ok();
                }
            }
        }
    });

    tx
}
