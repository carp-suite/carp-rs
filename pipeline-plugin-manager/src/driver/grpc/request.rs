use bytes::Bytes;
use futures::StreamExt;
use std::error::Error as StdError;
use uuid::Uuid;

use super::error::Error;

pub async fn to_protobuf<Body>(
    id: &Uuid,
    request: &mut http::Request<Body>,
) -> Result<super::main::HttpRequest, Error<Body::Error>>
where
    Body: Unpin + http_body::Body,
    Body::Error: StdError + Send + Sync + 'static,
{
    let request = super::main::HttpRequest {
        id: id.as_bytes().to_vec(),
        path: request.uri().path().to_string(),
        method: (match *request.method() {
            http::Method::GET => super::main::Method::Get,
            http::Method::POST => super::main::Method::Post,
            http::Method::PUT => super::main::Method::Put,
            http::Method::DELETE => super::main::Method::Delete,
            http::Method::PATCH => super::main::Method::Patch,
            http::Method::OPTIONS => super::main::Method::Options,
            _ => unreachable!(),
        }) as i32,
        headers: request
            .headers()
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_str().unwrap().to_string()))
            .collect(),
        body: hyper::body::to_bytes(request.body_mut())
            .await
            .map_err(Error::BodyError)?
            .to_vec(),
    };

    Ok(request)
}

pub fn from_protobuf<Err, Body, ChunkStream, Chunk>(
    uri: &http::Uri,
    request: super::main::HttpRequest,
    stream: ChunkStream,
) -> Result<http::Request<Body>, Error<Err>>
where
    Body: From<
        Box<
            dyn futures::Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                + Send
                + 'static,
        >,
    >,
    ChunkStream: futures::Stream<Item = Result<Option<Chunk>, Error<Err>>> + Send + 'static,
    Bytes: From<Chunk>,
    Err: StdError + Send + Sync + 'static,
{
    let mut uri_parts = http::uri::Parts::default();

    uri_parts.scheme = uri.scheme().cloned();
    uri_parts.authority = uri.authority().cloned();
    uri_parts.path_and_query = Some(http::uri::PathAndQuery::try_from(request.path)?);

    let mut http_request = http::Request::builder().uri(http::Uri::try_from(uri_parts)?);
    for (k, v) in request.headers {
        http_request = http_request.header(k, v);
    }
    let first_chunk = futures::stream::once(futures::future::ready(Ok(Bytes::copy_from_slice(
        &request.body,
    ))));
    let subsequent_chunks = stream.flat_map(|part| match part {
        Ok(Some(request_bytes)) => {
            futures::stream::once(futures::future::ready(Ok(Bytes::from(request_bytes))))
                .left_stream()
        }
        Ok(None) => futures::stream::empty().right_stream(),
        Err(err) => futures::stream::once(futures::future::ready(Err(
            Box::new(Error::from(err)) as Box<dyn StdError + Send + Sync + 'static>
        )))
        .left_stream(),
    });

    Ok(http_request
        .body(Body::from(Box::new(first_chunk.chain(subsequent_chunks))))
        .unwrap())
}
