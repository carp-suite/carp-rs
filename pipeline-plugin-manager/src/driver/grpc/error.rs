use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error<BodyError> {
    #[error("Expected chunk of type {0}")]
    ProtocolError(&'static str),
    #[error(transparent)]
    GrpcError(#[from] tonic::Status),
    #[error("Error while getting body from request/response")]
    BodyError(#[source] BodyError),
    #[error("Invalid path in resquest/response received from plugin")]
    InvalidPath(#[from] http::uri::InvalidUri),
    #[error("Invalid uri in resquest/response received from plugin")]
    InvalidUri(#[from] http::uri::InvalidUriParts),
    #[error("Error while parsing body as HTTP response")]
    HttpParseError(#[from] httparse::Error),
    #[error("Received bytes were insufficient to parse HTTP response")]
    InsufficientBytes,
    #[error("Error while sending body chunk over gRPC")]
    ChannelSendError,
}
