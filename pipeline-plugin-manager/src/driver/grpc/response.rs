use bytes::Bytes;
use futures::StreamExt;
use httparse::Status;
use std::{error::Error as StdError, mem::MaybeUninit};
use uuid::Uuid;

use crate::driver::grpc::error::Error;

pub async fn to_protobuf<Body>(
    id: &Uuid,
    path: &str,
    response: &mut http::Response<Body>,
) -> Result<super::main::HttpResponse, Error<Body::Error>>
where
    Body: http_body::Body + Unpin,
    Body::Error: StdError + Send + Sync + 'static,
{
    let response = super::main::HttpResponse {
        id: id.as_bytes().to_vec(),
        status: response.status().as_u16() as i32,
        path: path.to_string(),
        headers: response
            .headers()
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_str().unwrap().to_string()))
            .collect(),
        body: hyper::body::to_bytes(response.body_mut())
            .await
            .map_err(Error::BodyError)?
            .to_vec(),
    };

    Ok(response)
}

pub fn from_protobuf<Err, Body, ChunkStream, Chunk>(
    response: super::main::HttpResponse,
    stream: ChunkStream,
) -> Result<http::Response<Body>, Error<Err>>
where
    Body: From<
        Box<
            dyn futures::Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                + Send
                + 'static,
        >,
    >,
    ChunkStream: futures::Stream<Item = Result<Option<Chunk>, Error<Err>>> + Send + 'static,
    Bytes: From<Chunk>,
    Err: StdError + Send + Sync + 'static,
{
    let mut http_response = http::Response::builder().status(response.status as u16);
    for (k, v) in response.headers {
        http_response = http_response.header(k, v);
    }
    let first_chunk = futures::stream::once(futures::future::ready(Ok(Bytes::copy_from_slice(
        &*response.body,
    ))));
    let subsequent_chunks = stream.flat_map(|part| match part {
        Ok(Some(response_chunks)) => {
            futures::stream::once(futures::future::ready(Ok(Bytes::from(response_chunks))))
                .left_stream()
        }
        Ok(None) => futures::stream::empty().right_stream(),
        Err(err) => futures::stream::once(futures::future::ready(Err(
            Box::new(Error::from(err)) as Box<dyn StdError + Send + Sync + 'static>
        )))
        .left_stream(),
    });

    Ok(http_response
        .body(Body::from(Box::new(first_chunk.chain(subsequent_chunks))))
        .unwrap())
}

pub async fn from_cache_response<Err, Body, ChunkStream, Chunk>(
    mut response: super::main::CacheResponse,
    mut stream: ChunkStream,
) -> Result<http::Response<Body>, Error<Err>>
where
    ChunkStream: futures::Stream<Item = Result<Chunk, Error<Err>>> + Send + Unpin + 'static,
    Chunk: IntoIterator<Item = u8>,
    Bytes: From<Chunk>,
    Body: From<
        Box<
            dyn futures::Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                + Send
                + 'static,
        >,
    >,
    Err: StdError + Send + Sync + 'static,
{
    let (min_size, expected_size) = stream.size_hint();
    let mut buffer = if let Some(expected_size) = expected_size {
        Vec::with_capacity(expected_size)
    } else {
        Vec::with_capacity(min_size)
    };
    let mut output_response_builder = http::Response::builder();

    let mut parser_config = httparse::ParserConfig::default();
    parser_config.allow_spaces_after_header_name_in_responses(true);
    parser_config.allow_multiple_spaces_in_response_status_delimiters(true);
    parser_config.ignore_invalid_headers_in_responses(true);

    buffer.append(&mut response.response_byte);

    while let Some(Ok(chunk)) = stream.next().await {
        buffer.extend(chunk);

        let mut uninit_headers_buf: [MaybeUninit<httparse::Header<'_>>; 100] =
            unsafe { MaybeUninit::uninit().assume_init() };
        let mut parse_response = httparse::Response::new(&mut []);

        match parser_config.parse_response_with_uninit_headers(
            &mut parse_response,
            &buffer,
            &mut uninit_headers_buf,
        )? {
            Status::Partial => {
                if let Some(status) = parse_response.code {
                    output_response_builder = output_response_builder.status(status);
                }
                if let Some(version) = parse_response.version {
                    output_response_builder = output_response_builder.version(match version {
                        1 => http::Version::HTTP_11,
                        2 => http::Version::HTTP_2,
                        _ => unreachable!(),
                    });
                }
                let headers = output_response_builder.headers_mut().unwrap();
                for header in parse_response.headers.iter() {
                    log::debug!("Got header from cache: {:?}", header);
                    if header.name == httparse::EMPTY_HEADER.name {
                        continue;
                    }
                    headers.insert(
                        http::HeaderName::try_from(header.name).unwrap(),
                        http::HeaderValue::from_bytes(header.value).unwrap(),
                    );
                }
            }
            Status::Complete(size) => {
                if let Some(status) = parse_response.code {
                    output_response_builder = output_response_builder.status(status);
                }
                if let Some(version) = parse_response.version {
                    output_response_builder = output_response_builder.version(match version {
                        1 => http::Version::HTTP_11,
                        2 => http::Version::HTTP_2,
                        _ => unreachable!(),
                    });
                }
                let headers = output_response_builder.headers_mut().unwrap();
                for header in parse_response.headers.iter() {
                    log::debug!("Got header from cache: {:?}", header);
                    if header.name == httparse::EMPTY_HEADER.name {
                        continue;
                    }
                    headers.insert(
                        http::HeaderName::try_from(header.name).unwrap(),
                        http::HeaderValue::from_bytes(header.value).unwrap(),
                    );
                }

                let read_body = futures::stream::once(futures::future::ready(Ok(
                    Bytes::copy_from_slice(&buffer[size..]),
                )));
                let body_stream = stream.map(|chunk| match chunk {
                    Ok(chunk) => Ok(Bytes::from(chunk)),
                    Err(err) => Err(Box::new(Error::from(err)) as Box<dyn StdError + Send + Sync>),
                });
                return Ok(output_response_builder
                    .body(Body::from(Box::new(read_body.chain(body_stream))))
                    .unwrap());
            }
        }
    }

    Err(Error::InsufficientBytes)
}

pub async fn flush_response_to_stream<'a, Id, Body, Err>(
    id: &Id,
    response: &'a mut http::Response<Body>,
    channel: &tokio::sync::mpsc::UnboundedSender<super::main::CacheResponse>,
) -> Result<Vec<u8>, Error<Err>>
where
    Id: AsRef<[u8]>,
    Body: http_body::Body + Unpin + 'static,
    Bytes: From<Body::Data>,
    Err: StdError,
{
    let id = id.as_ref().to_vec();
    let mut buffered_body = Vec::new();
    let mut string_buffer =
        String::with_capacity(6 + response.status().as_str().len() + response.headers().len());
    string_buffer.push_str(&format!(
        "{version:?} {status_code} {status}\n",
        version = response.version(),
        status_code = response.status().as_u16(),
        status = response.status().as_str(),
    ));
    for (k, v) in response.headers() {
        string_buffer.push_str(&format!("{}: {}\n", k, v.to_str().unwrap()));
    }
    string_buffer.push('\n');

    channel
        .send(super::main::CacheResponse {
            id: id.clone(),
            response_size: string_buffer.len() as f64,
            response_byte: Vec::from(string_buffer),
        })
        .map_err(|_| Error::ChannelSendError)?;

    while let Some(Ok(chunk)) = response.body_mut().data().await {
        let chunk = Bytes::from(chunk);

        buffered_body.extend(chunk.iter());
        channel
            .send(super::main::CacheResponse {
                id: id.clone(),
                response_size: chunk.len() as f64,
                response_byte: chunk.to_vec(),
            })
            .map_err(|_| Error::ChannelSendError)?;
    }

    channel
        .send(super::main::CacheResponse {
            id,
            response_size: 0.0,
            response_byte: vec![],
        })
        .map_err(|_| Error::ChannelSendError)?;

    Ok(buffered_body)
}
