use thiserror::Error;

#[derive(Error, Debug)]
pub enum SetupError {
    #[error("Failed to read file")]
    FileReadError(#[from] std::io::Error),
    #[error("Failed to build wasmtime provider from plugin {0}")]
    WasmtimeProviderError(String, #[source] wasmtime_provider::errors::Error),
    #[error("Failed to build waPC host from plugin {0}")]
    WapcHostError(String, #[source] wapc::errors::Error),
    #[error("Failed to configure waPC plugin {0}")]
    WapcConfigureError(String, #[source] wapc::errors::Error),
    #[error("Can't build plugin for url starting with {0}")]
    UnknownPluginScheme(String),
    #[error("Can't connect to gRPC host {0} when starting plugin {1}")]
    GrpcConnectionError(String, String, #[source] tonic::transport::Error),
    #[error("Can't configure gRPC plugin {0} with settings: \n {1}")]
    GrpcConfigureError(String, String, #[source] tonic::Status),
    #[error("Can't start gRPC plugin {0}'s client with path {1}")]
    GrpcPathError(String, String, #[source] tonic::transport::Error),
    #[error(transparent)]
    GrpcCallError(#[from] tonic::Status),
}

#[derive(Debug, Error)]
pub enum Error<BodyError>
where
    BodyError: std::error::Error,
{
    #[error(transparent)]
    WapcCallError(#[from] wapc::errors::Error),
    #[error(transparent)]
    GrpcCallError(#[from] tonic::Status),
    #[error("Error while running {0} on WASM plugin {1}")]
    WasmPluginError(String, String, super::wasm::error::Error<BodyError>),
    #[error("Error while running {0} on gRPC plugin {1}")]
    GrpcPluginError(String, String, super::grpc::error::Error<BodyError>),
    #[error("gRPC plugin {0} sent invalid chunk when streaming")]
    GrpcInvalidChunk(String),
}
