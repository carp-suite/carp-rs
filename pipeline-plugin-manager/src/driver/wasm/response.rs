use super::{
    carp_generated::fb::{Response, ResponseArgs},
    error::Error,
};
use crate::driver::wasm::carp_generated::fb::{Header, HeaderArgs};
use http::{HeaderMap, HeaderName, HeaderValue};
use hyper::body::Bytes;
use std::error::Error as StdError;
use uuid::Uuid;

pub fn from_flatbuffers<Body, E>(fb_bytes: Vec<u8>) -> Result<http::Response<Body>, Error<E>>
where
    Body: From<Vec<u8>>,
    E: StdError,
{
    let value = flatbuffers::root::<Response>(&fb_bytes)?;
    let headers = if let Some(headers) = value.headers() {
        HeaderMap::from_iter(
            headers
                .iter()
                .flat_map(|header| header.key().zip(header.value()))
                .map(|(k, v)| {
                    (
                        HeaderName::from_bytes(k.as_bytes()).unwrap(),
                        HeaderValue::from_bytes(v.as_bytes()).unwrap(),
                    )
                }),
        )
    } else {
        HeaderMap::new()
    };
    let status = value.status();
    let body = Body::from(if let Some(body_bytes) = value.body() {
        body_bytes.bytes().to_vec()
    } else {
        vec![]
    });
    let mut request = http::Response::builder()
        .status(status as u16)
        .body(body)
        .unwrap();

    *request.headers_mut() = headers;

    Ok(request)
}

pub async fn to_flatbuffers<Body>(
    id: &Uuid,
    path: &str,
    response: &mut http::Response<Body>,
) -> Result<(Vec<u8>, Bytes), Error<Body::Error>>
where
    Body: http_body::Body + Unpin,
    Body::Error: StdError + Send + Sync + 'static,
{
    let mut flatbuffer_builder = flatbuffers::FlatBufferBuilder::new();
    let mut response_args = ResponseArgs::default();
    response_args.id = Some(flatbuffer_builder.create_vector(id.as_bytes()));
    response_args.status = response.status().as_u16() as i16;
    response_args.path = Some(flatbuffer_builder.create_string(path));
    let headers = response
        .headers()
        .iter()
        .map(|(k, v)| {
            let key = flatbuffer_builder.create_string(k.as_str());
            let value = flatbuffer_builder.create_string(v.to_str().unwrap());

            Header::create(
                &mut flatbuffer_builder,
                &HeaderArgs {
                    key: Some(key),
                    value: Some(value),
                },
            )
        })
        .collect::<Vec<_>>();
    response_args.headers = Some(flatbuffer_builder.create_vector(&headers));
    let body = hyper::body::to_bytes(response.body_mut())
        .await
        .map_err(Error::BodyError)?;
    response_args.body = Some(flatbuffer_builder.create_vector(&body));

    let response = Response::create(&mut flatbuffer_builder, &response_args);
    flatbuffer_builder.finish(response, None);

    Ok((flatbuffer_builder.finished_data().to_vec(), body))
}

#[cfg(test)]
mod tests {
    use super::*;
    use hyper::Body;

    #[tokio::test]
    async fn it_converts_response_to_flatbuffers() {
        // Given
        let mut response = http::Response::builder()
            .status(http::StatusCode::OK)
            .header("Content-Type", "application/json")
            .body(Body::from("Hello World"))
            .unwrap();
        let id = Uuid::new_v4();
        let path = "/helloworld";

        // When
        let (flatbuffers_response_content, response_body) =
            to_flatbuffers(&id, path, &mut response).await.unwrap();

        // Then
        let response_from_flatbuffers =
            flatbuffers::root::<Response>(&flatbuffers_response_content).unwrap();

        assert_eq!(
            response_from_flatbuffers.id().unwrap().bytes(),
            id.as_bytes()
        );
        assert_eq!(response_from_flatbuffers.status(), 200);
        assert_eq!(response_from_flatbuffers.path().unwrap(), path);
        assert_eq!(response_from_flatbuffers.headers().unwrap().len(), 1);
        assert_eq!(
            response_from_flatbuffers
                .headers()
                .unwrap()
                .get(0)
                .key()
                .unwrap(),
            "content-type"
        );
        assert_eq!(
            response_from_flatbuffers
                .headers()
                .unwrap()
                .get(0)
                .value()
                .unwrap(),
            "application/json"
        );
        assert_eq!(response_from_flatbuffers.body().unwrap().len(), 11);
        assert_eq!(
            response_from_flatbuffers.body().unwrap().bytes(),
            b"Hello World"
        );
        assert_eq!(&*response_body, b"Hello World");
    }
    #[tokio::test]
    async fn it_converts_flatbuffers_bytes_to_response() {
        // Given
        let (flatbuffers_response_content, _) = {
            let mut response = http::Response::builder()
                .status(http::StatusCode::OK)
                .header("Content-Type", "application/json")
                .body(Body::from("Hello World"))
                .unwrap();
            let id = Uuid::new_v4();
            let path = "/helloworld";

            to_flatbuffers(&id, path, &mut response).await.unwrap()
        };

        // When
        let mut response: http::Response<Body> =
            from_flatbuffers::<_, hyper::Error>(flatbuffers_response_content).unwrap();

        // Then
        assert_eq!(response.status(), 200);
        assert_eq!(response.headers().len(), 1);
        assert_eq!(
            response.headers().get("content-type").unwrap(),
            "application/json"
        );
        assert_eq!(
            hyper::body::to_bytes(response.body_mut())
                .await
                .unwrap()
                .to_vec(),
            b"Hello World".to_vec()
        );
    }
}
