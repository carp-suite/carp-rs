use super::{
    carp_generated::fb::{Header, HeaderArgs, Request, RequestArgs},
    error::Error,
};
use flatbuffers::FlatBufferBuilder;
use http::{
    uri::{Parts, PathAndQuery},
    HeaderMap, HeaderName, HeaderValue, Method, Uri,
};
use http_body::Body as HttpBody;
use hyper::body::Bytes;
use std::error::Error as StdError;
use uuid::Uuid;

pub fn from_flatbuffers<Body, E>(
    fb_bytes: Vec<u8>,
    uri: &Uri,
) -> Result<http::Request<Body>, Error<E>>
where
    Body: From<Vec<u8>>,
    E: StdError,
{
    let value = flatbuffers::root::<Request>(&fb_bytes)?;
    let path = value.path();
    let mut uri_parts = Parts::default();

    uri_parts.scheme = uri.scheme().cloned();
    uri_parts.authority = uri.authority().cloned();
    uri_parts.path_and_query = Some(PathAndQuery::try_from(path.unwrap_or(""))?);

    let method = match value.method() {
        1 => Method::GET,
        2 => Method::POST,
        3 => Method::PUT,
        4 => Method::PATCH,
        5 => Method::DELETE,
        6 => Method::OPTIONS,
        _ => unreachable!("Hyper has added a new HTTP method"),
    };
    let headers = if let Some(headers) = value.headers() {
        HeaderMap::from_iter(
            headers
                .iter()
                .flat_map(|header| header.key().zip(header.value()))
                .map(|(k, v)| {
                    (
                        HeaderName::from_bytes(k.as_bytes()).unwrap(),
                        HeaderValue::from_bytes(v.as_bytes()).unwrap(),
                    )
                }),
        )
    } else {
        HeaderMap::new()
    };
    let body = Body::from(if let Some(body_bytes) = value.body() {
        body_bytes.bytes().to_vec()
    } else {
        vec![]
    });
    let mut request = http::Request::builder()
        .uri(Uri::from_parts(uri_parts).unwrap())
        .method(method)
        .body(body)
        .unwrap();

    *request.headers_mut() = headers;

    Ok(request)
}

pub async fn to_flatbuffers<Body>(
    id: &Uuid,
    value: &mut http::Request<Body>,
) -> Result<(Vec<u8>, Bytes), Error<Body::Error>>
where
    Body: HttpBody + Unpin,
    Body::Error: StdError + Send + Sync + 'static,
{
    let mut flatbuffer_builder = FlatBufferBuilder::new();
    let mut request_args = RequestArgs::default();
    request_args.id = Some(flatbuffer_builder.create_vector(id.as_bytes()));
    request_args.method = match *value.method() {
        http::method::Method::GET => 1,
        http::method::Method::POST => 2,
        http::method::Method::PATCH => 3,
        http::method::Method::PUT => 4,
        http::method::Method::DELETE => 5,
        http::method::Method::OPTIONS => 6,
        _ => unreachable!("Hyper has added a new HTTP method"),
    };
    request_args.path = Some(flatbuffer_builder.create_string(value.uri().path()));
    let headers = value
        .headers()
        .iter()
        .map(|(k, v)| {
            let key = flatbuffer_builder.create_string(k.as_str());
            let value = flatbuffer_builder.create_string(v.to_str().unwrap());
            Header::create(
                &mut flatbuffer_builder,
                &HeaderArgs {
                    key: Some(key),
                    value: Some(value),
                },
            )
        })
        .collect::<Vec<_>>();
    request_args.headers = Some(flatbuffer_builder.create_vector(&headers));
    let body = hyper::body::to_bytes(value.body_mut())
        .await
        .map_err(Error::BodyError)?;
    request_args.body = Some(flatbuffer_builder.create_vector(&body));
    let request = Request::create(&mut flatbuffer_builder, &request_args);
    flatbuffer_builder.finish(request, None);

    Ok((flatbuffer_builder.finished_data().to_vec(), body))
}

#[cfg(test)]
mod tests {
    use http::uri::Authority;
    use hyper::Body;

    use crate::driver::wasm::carp_generated::fb::Request;

    use super::*;

    #[tokio::test]
    async fn it_converts_request_with_simple_body_to_flatbuffers() {
        // Given
        let mut request = http::Request::builder()
            .uri("http://example.com")
            .header("Accept", "application/json")
            .body(Body::from("Hello World"))
            .unwrap();
        let uuid = uuid::Uuid::new_v4();

        // When
        let (flatbuffers_request_content, request_body) =
            to_flatbuffers(&uuid, &mut request).await.unwrap();

        // Then
        let flatbuffers_request =
            flatbuffers::root::<Request>(&flatbuffers_request_content).unwrap();

        assert_eq!(flatbuffers_request.method(), 1);
        assert_eq!(flatbuffers_request.path().unwrap(), "/");
        assert_eq!(flatbuffers_request.headers().unwrap().len(), 1);
        assert_eq!(
            flatbuffers_request.headers().unwrap().get(0).key().unwrap(),
            "accept"
        );
        assert_eq!(
            flatbuffers_request
                .headers()
                .unwrap()
                .get(0)
                .value()
                .unwrap(),
            "application/json"
        );
        assert_eq!(flatbuffers_request.body().unwrap().bytes(), b"Hello World");
        assert_eq!(flatbuffers_request.id().unwrap().bytes(), uuid.as_bytes());
        assert_eq!(&*request_body, b"Hello World");
    }

    #[tokio::test]
    async fn it_converts_flabuffer_bytes_to_http_request() {
        // Given
        let (flabuffer_bytes, _) = {
            let mut request = http::Request::builder()
                .uri("http://example.com")
                .header("Accept", "application/json")
                .body(Body::from("Hello World"))
                .unwrap();
            let uuid = uuid::Uuid::new_v4();

            to_flatbuffers(&uuid, &mut request).await.unwrap()
        };

        // When
        let mut request: http::Request<Body> = from_flatbuffers::<_, hyper::Error>(
            flabuffer_bytes,
            &Uri::from_static("http://example.com"),
        )
        .unwrap();

        // Then
        assert_eq!(request.method(), Method::GET);
        assert_eq!(request.uri().path(), "/");
        assert_eq!(
            request.uri().authority().cloned(),
            Some(Authority::from_static("example.com"))
        );
        assert_eq!(request.uri().scheme(), Some(&http::uri::Scheme::HTTP));
        assert_eq!(request.headers().len(), 1);
        assert_eq!(request.headers().get("accept").unwrap(), "application/json");
        assert_eq!(
            hyper::body::to_bytes(request.body_mut())
                .await
                .unwrap()
                .to_vec(),
            b"Hello World".to_vec()
        );
    }
}
