use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error<BodyError>
where
    BodyError: std::error::Error,
{
    #[error("Error during flatbuffers marshalling")]
    FlatbuffersError(#[from] flatbuffers::InvalidFlatbuffer),
    #[error("Invalid path in resquest/response received from plugin")]
    InvalidPath(#[from] http::uri::InvalidUri),
    #[error("Error while obtaining request/response body")]
    BodyError(#[source] BodyError),
}
