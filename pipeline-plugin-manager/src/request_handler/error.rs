use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error<ReqBodyError, ResBodyError, InnerError>
where
    ReqBodyError: std::error::Error,
    ResBodyError: std::error::Error,
    InnerError: std::error::Error,
{
    #[error("Error occured while calling plugin")]
    DriverReqBodyError(#[source] crate::driver::error::Error<ReqBodyError>),
    #[error("Error occured while calling plugin")]
    DriverResBodyError(#[source] crate::driver::error::Error<ResBodyError>),
    #[error("Error occured while handling request")]
    InnerError(#[source] InnerError),
}
