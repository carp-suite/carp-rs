use crate::driver::Driver;
use bytes::Bytes;
use either::Either;
use futures::{Future, FutureExt, Stream};
use http::Response;
use pipeline::context::RequestContext;
use pipeline::http_client::HttpClient;
use std::error::Error as StdError;
use std::sync::Arc;

use self::error::Error;

pub mod error;

pub struct PluginRequestHandler<Inner> {
    request_start_plugins: Arc<[Arc<Driver>]>,
    request_end_plugins: Arc<[Arc<Driver>]>,
    inner: Arc<Inner>,
}

impl<Inner> PluginRequestHandler<Inner> {
    pub fn new(
        inner: Inner,
        request_start_plugins: Vec<Arc<Driver>>,
        request_end_plugins: Vec<Arc<Driver>>,
    ) -> Self {
        PluginRequestHandler {
            request_start_plugins: request_start_plugins.into(),
            request_end_plugins: request_end_plugins.into(),
            inner: Arc::new(inner),
        }
    }
}

impl<ReqBody, ResBody, PluginRequestHandlerInner> HttpClient<ReqBody, ResBody>
    for PluginRequestHandler<PluginRequestHandlerInner>
where
    PluginRequestHandlerInner: HttpClient<ReqBody, ResBody> + 'static,
    ReqBody: From<Vec<u8>>
        + From<Bytes>
        + From<
            Box<
                dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                    + Send
                    + 'static,
            >,
        > + http_body::Body
        + Unpin
        + 'static,
    ReqBody::Error: StdError + Send + Sync + 'static,
    ResBody: From<Vec<u8>>
        + From<Bytes>
        + http_body::Body
        + From<
            Box<
                dyn Stream<Item = Result<Bytes, Box<dyn StdError + Send + Sync + 'static>>>
                    + Send
                    + 'static,
            >,
        > + Unpin,
    ResBody::Error: StdError + Send + Sync + 'static,
    PluginRequestHandlerInner::Error: StdError + Send + Sync + 'static,
{
    type Error = Error<ReqBody::Error, ResBody::Error, PluginRequestHandlerInner::Error>;
    type BeforeRequestFuture = impl Future<
        Output = Result<Either<http::Request<ReqBody>, http::Response<ResBody>>, Self::Error>,
    >;
    type AfterRequestFuture = impl Future<Output = Result<http::Response<ResBody>, Self::Error>>;
    type Future = impl Future<Output = Result<Response<ResBody>, Self::Error>> + 'static;

    fn before_request(&self, mut request: http::Request<ReqBody>) -> Self::BeforeRequestFuture {
        if self.request_start_plugins.is_empty() {
            return futures::future::ready(Ok(Either::Left(request))).left_future();
        }

        let request_start_usages = Arc::clone(&self.request_start_plugins);
        let ctx = request.extensions().get::<RequestContext>().unwrap();
        let uri = ctx.uri();
        let id = ctx.id();

        async move {
            for driver in request_start_usages.iter() {
                let result = driver
                    .request_start::<ReqBody, ResBody>(&id, &uri, &mut request)
                    .await
                    .map_err(Error::DriverReqBodyError)?;

                match result {
                    None => {}
                    Some(Either::Right(response)) => return Ok(Either::Right(response)),
                    Some(Either::Left(overriden)) => {
                        request = overriden;
                    }
                }
            }

            Ok(Either::Left(request))
        }
        .right_future()
    }

    fn after_request(&self, mut response: http::Response<ResBody>) -> Self::AfterRequestFuture {
        if self.request_end_plugins.is_empty() {
            return futures::future::ready(Ok(response)).left_future();
        }

        let request_end_usages = Arc::clone(&self.request_end_plugins);
        let ctx = response.extensions().get::<RequestContext>().unwrap();
        let id = ctx.id();
        let uri = ctx.uri();

        async move {
            for driver in request_end_usages.iter() {
                let result = driver
                    .request_end(&id, &uri, &mut response)
                    .await
                    .map_err(Error::DriverResBodyError)?;

                if let Some(overriden) = result {
                    response = overriden;
                }
            }

            Ok(response)
        }
        .right_future()
    }

    fn request(&self, request: http::Request<ReqBody>) -> Self::Future {
        let inner = self.inner.clone();
        async move { inner.request(request).await.map_err(Error::InnerError) }
    }
}
