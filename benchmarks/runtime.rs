#![feature(test)]
extern crate test;

use carp::runtime::{ApplicationRuntime, MultipleRuntime};
use http::{Response, Uri};
use hyper::{
    client::HttpConnector,
    service::{make_service_fn, service_fn},
    Body, Client, Server,
};
use models::{
    route::{cache_usage::CacheUsage, http_method::HttpMethod, Route},
    settings::{http_version::HttpVersion, Settings},
};
use pipeline::driver::PipelineDriver;
use pipeline_driver::hyper::hyper_driver;
use pipeline_request_handler::{handler::make, http_client::hyper::HyperClientService};
use std::{convert::Infallible, fmt::Debug, net::SocketAddr};
use tokio::{
    runtime::Builder,
    sync::oneshot::{self, Receiver},
};

fn start_dummy_server(port: u16) -> Receiver<()> {
    let (tx, rx) = oneshot::channel();
    let runtime = Builder::new_multi_thread().enable_all().build().unwrap();

    std::thread::spawn(move || {
        runtime.block_on(async move {
            let server = Server::bind(&SocketAddr::from(([127, 0, 0, 1], port))).serve(
                make_service_fn(|_| async move {
                    Ok::<_, Infallible>(service_fn(|_| async move {
                        Ok::<_, Infallible>(
                            Response::builder()
                                .status(200)
                                .body(Body::from("Hello World"))
                                .unwrap(),
                        )
                    }))
                }),
            );
            tx.send(()).unwrap();
            server.await.unwrap();
        })
    });

    rx
}

fn start_carp_server<E, RT>(
    settings: Settings,
    routes: Vec<Route>,
    runtime_creation: fn() -> Result<RT, E>,
) -> Receiver<()>
where
    E: Debug + 'static,
    RT: ApplicationRuntime + Sync + Send + 'static,
{
    let (tx, rx) = oneshot::channel();

    std::thread::spawn(move || {
        let runtime = runtime_creation().unwrap();
        runtime.driver_handle().block_on(async move {
            let driver = hyper_driver(settings)
                .unwrap()
                .start(move |_| {
                    make::<(), _, _>(
                        routes.clone(),
                        runtime.request_handler_handle(),
                        |scheme, authority, _, _| {
                            HyperClientService::new_with_authority(scheme, authority)
                        },
                    )
                    .unwrap()
                })
                .await
                .unwrap();
            tx.send(()).unwrap();
            driver.wait().await.unwrap();
        });
    });

    rx
}

async fn execute_request(client: &Client<HttpConnector>, target: Uri) {
    let response = client.clone().get(target).await.unwrap();
    assert_eq!(response.status(), 200);
    let body_string = hyper::body::to_bytes(response.into_body()).await.unwrap();
    assert_eq!(b"Hello World".to_vec(), body_string.to_vec());
}

fn benchmark_runtime<E, RT>(
    b: &mut test::Bencher,
    settings: Settings,
    routes: Vec<Route>,
    target_port: u16,
    target_uri: &'static str,
    runtime_creation: fn() -> Result<RT, E>,
) where
    E: Debug + 'static,
    RT: ApplicationRuntime + Sync + Send + 'static,
{
    let carp_rx = start_carp_server(settings, routes, runtime_creation);
    let dummy_rx = start_dummy_server(target_port);
    let client = Client::new();
    let runtime = Builder::new_multi_thread().enable_all().build().unwrap();
    runtime.block_on(dummy_rx).unwrap();
    runtime.block_on(carp_rx).unwrap();

    b.iter(|| runtime.block_on(execute_request(&client, Uri::from_static(target_uri))));
}

#[bench]
fn runtime_benchmark(b: &mut test::Bencher) {
    let settings =
        Settings::try_from(("http://0.0.0.0:7884/", HttpVersion::V1, None, None, vec![])).unwrap();
    let routes = vec![Route::try_from((
        "http://localhost:5500/api",
        HttpMethod::GET,
        Some(CacheUsage::from(("".to_owned(), vec![], vec![], vec![]))),
        vec![],
    ))
    .unwrap()];

    benchmark_runtime(
        b,
        settings,
        routes,
        5500,
        "http://localhost:7884/api",
        MultipleRuntime::try_new,
    );
}
