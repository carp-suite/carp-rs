#![feature(test)]
extern crate test;

use flexstr::{LocalStr, SharedStr};
use http::uri::*;

fn builder_uri_creation(query: Option<&str>, path: &str, scheme: &str, authority: &str) -> Uri {
    if query.is_some() {
        let query = query.unwrap();
        let path = LocalStr::from(path);
        let path_and_query = path + "?" + query;

        Uri::builder()
            .scheme(scheme)
            .authority(authority)
            .path_and_query(path_and_query.as_str())
            .build()
            .unwrap()
    } else {
        Uri::builder()
            .scheme(scheme)
            .authority(authority)
            .path_and_query(path)
            .build()
            .unwrap()
    }
}

fn from_parts_creation(
    path_and_query: Option<&PathAndQuery>,
    https: bool,
    authority: &Authority,
) -> Uri {
    let mut parts = Parts::default();
    let authority = authority.clone();
    let path_and_query = path_and_query.cloned();
    let scheme = if https { Scheme::HTTPS } else { Scheme::HTTP };

    parts.scheme = Some(scheme);
    parts.path_and_query = path_and_query;
    parts.authority = Some(authority);

    Uri::from_parts(parts).unwrap()
}

// Benchmark builder_uri_creation method
#[bench]
fn bench_builder_uri_creation(b: &mut test::Bencher) {
    let authority = SharedStr::from_static("example.com");
    let scheme = SharedStr::from_static("https");
    let path = "/some/path";
    let query = Some("a=b");

    b.iter(|| {
        let uri = builder_uri_creation(query, path, scheme.as_str(), authority.as_str());
        assert_eq!(uri.to_string(), "https://example.com/some/path?a=b");
    })
}

// Benchmark from_parts_creation method
#[bench]
fn bench_from_parts_creation(b: &mut test::Bencher) {
    let https = true;
    let authority = Authority::from_static("example.com");
    let path_and_query = Some(PathAndQuery::from_static("/some/path?a=b"));

    b.iter(|| {
        let uri = from_parts_creation(path_and_query.as_ref(), https, &authority);
        assert_eq!(uri.to_string(), "https://example.com/some/path?a=b")
    })
}
