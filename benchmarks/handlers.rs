#![feature(test)]

use futures::{future::ready, Future, FutureExt};
use http::{Method, Request, Response, StatusCode};
use hyper::body::{Body, HttpBody};
use intrinsics::{likely, unlikely};
use matchit::Router;
use std::{collections::HashSet, sync::Arc};
use thiserror::Error;
use tokio::runtime::{Handle, Runtime};
extern crate test;

#[derive(Error, Debug)]
enum Error<ClientError> {
    #[error("Error while performing the request")]
    Client(#[source] ClientError),
    #[error(transparent)]
    RecvError(#[from] tokio::sync::oneshot::error::RecvError),
    #[error(transparent)]
    JoinError(#[from] tokio::task::JoinError),
}

fn spawned_handler(
    router: &Router<(Arc<()>, HashSet<Method>)>,
    handle: &Handle,
    req: Request<Body>,
) -> impl Future<Output = Result<Response<Body>, Error<hyper::Error>>> {
    let match_option = router.at(req.uri().path()).ok();
    if likely(match_option.is_some()) {
        let (_client, methods) = match_option.as_ref().unwrap().value;

        if unlikely(!methods.contains(req.method())) {
            return ready(Ok(Response::builder()
                .status(405)
                .body(Body::default())
                .unwrap()))
            .left_future();
        }

        let spawn_future = handle.spawn(async move {
            Ok::<_, Error<hyper::Error>>(
                tokio::task::unconstrained(ready(Ok(Response::builder()
                    .status(StatusCode::OK)
                    .body(Body::empty())
                    .unwrap())))
                .await
                .map_err(Error::Client),
            )
        });

        async move {
            let client_result = spawn_future.await??;

            Ok(client_result?)
        }
        .right_future()
    } else {
        ready(Ok(Response::builder()
            .status(404)
            .body(Body::default())
            .unwrap()))
        .left_future()
    }
}

async fn arced_handler(
    router: &Router<(Arc<()>, HashSet<Method>)>,
    handle: &Handle,
    req: Request<Body>,
) -> Result<Response<Body>, Error<hyper::Error>> {
    let match_option = router.at(req.uri().path()).ok();
    if likely(match_option.is_some()) {
        let (client, methods) = match_option.as_ref().unwrap().value;

        if unlikely(!methods.contains(req.method())) {
            return Ok(Response::builder()
                .status(StatusCode::METHOD_NOT_ALLOWED)
                .body(Body::default())
                .unwrap());
        }

        let _ = Arc::clone(&client);
        let (tx, rx) = tokio::sync::oneshot::channel();
        handle.spawn(async move {
            let response_result = tokio::task::unconstrained(ready(Ok(Response::builder()
                .status(StatusCode::OK)
                .body(Body::default())
                .unwrap())))
            .await;
            tx.send(response_result).ok();
            Ok::<_, Error<hyper::Error>>(())
        });

        rx.await?.map_err(Error::Client)
    } else {
        Ok(Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(Body::default())
            .unwrap())
    }
}

#[bench]
fn arced_handler_benchmark(b: &mut test::Bencher) {
    let mut router = Router::new();
    router
        .insert("/", (Arc::new(()), HashSet::from_iter([Method::GET])))
        .unwrap();
    let router = Arc::new(router);
    let runtime = Runtime::new().unwrap();
    let runtime_handle = Arc::new(runtime.handle().clone());
    b.iter(move || {
        let request = Request::builder()
            .uri("http://localhost:5506/")
            .method(Method::GET)
            .body(Body::empty())
            .unwrap();
        runtime.block_on(async {
            let router = Arc::clone(&router);
            let handle = Arc::clone(&runtime_handle);
            let response = arced_handler(&*router, &*handle, request).await.unwrap();

            assert_eq!(response.status(), StatusCode::OK);
            assert_eq!(response.body().size_hint().exact().unwrap(), 0);
        })
    })
}

#[bench]
fn spawned_handler_benchmark(b: &mut test::Bencher) {
    let mut router = Router::new();
    router
        .insert("/", (Arc::new(()), HashSet::from_iter([Method::GET])))
        .unwrap();
    let router = Arc::new(router);
    let runtime = Runtime::new().unwrap();
    let runtime_handle = Arc::new(runtime.handle().clone());
    b.iter(move || {
        let request = Request::builder()
            .uri("http://localhost:5506/")
            .method(Method::GET)
            .body(Body::empty())
            .unwrap();
        runtime.block_on(async {
            let router = Arc::clone(&router);
            let handle = Arc::clone(&runtime_handle);
            let response = spawned_handler(&*router, &*handle, request).await.unwrap();

            assert_eq!(response.status(), StatusCode::OK);
            assert_eq!(response.body().size_hint().exact().unwrap(), 0);
        })
    })
}
