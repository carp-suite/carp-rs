use std::{
    env,
    ffi::OsStr,
    fs::{read_dir, remove_file, DirBuilder, File},
    io::Read,
    path::PathBuf,
    process::{Command, Stdio},
};

use scraper::{Html, Selector};
use thiserror::Error;

const LINE_COVERAGE_THRESHOLD: f64 = 80_f64;
const GRCOV_OPTIONS: &'static [&'static str] = &[
    ".",
    "-s",
    ".",
    "--llvm",
    "--binary-path",
    "./target/debug",
    "-t",
    "html",
    "--ignore-not-existing",
    "--ignore",
    "**/.cargo/registry/**/*.rs",
];

#[derive(Error, Debug)]
enum CoverageError {
    #[error("Line coverage must exceed 80%. Got {0}%")]
    LineCoverageNotExceeded(f64),
}

pub fn main() -> Result<(), anyhow::Error> {
    // Load environment variables

    dotenv::from_filename(".env.coverage")?;
    let ignore_files = env::var("IGNORE_FILES")?;
    let ignore_files: Vec<&str> = ignore_files.split(',').collect();

    // Run tests of project

    let mut test_command = Command::new("cargo");
    test_command.args(["test", "--workspace"]);
    test_command.envs(
        env::vars()
            .filter(|(k, _)| k.starts_with("TEST_"))
            .map(|(k, v)| (k.replace("TEST_", ""), v)),
    );
    test_command.stdout(Stdio::inherit());
    test_command.stderr(Stdio::inherit());

    println!("Output from {:#?} command", test_command);
    println!("=================================================");
    let test_command_result = test_command.output();
    println!("=================================================");

    test_command_result?;

    // Generate HTML report

    DirBuilder::new()
        .recursive(true)
        .create("/tmp/carp/coverage")?;
    let mut html_report_command = Command::new("grcov");
    html_report_command.args(GRCOV_OPTIONS);

    for file in ignore_files.iter().copied() {
        html_report_command.args(["--ignore", file]);
    }

    html_report_command.args(["-o", "/tmp/carp/coverage"]);

    html_report_command.stdout(Stdio::inherit());
    html_report_command.stderr(Stdio::inherit());

    println!("Running command {:#?}", html_report_command);
    println!("=================================================");
    html_report_command.output()?;

    // Generate cobertura report

    let mut cobertura_report_command = Command::new("grcov");
    cobertura_report_command.args(GRCOV_OPTIONS);

    for file in ignore_files.iter().copied() {
        cobertura_report_command.args(["--ignore", file]);
    }

    cobertura_report_command.args(["-o", "/tmp/carp/coverage"]);

    cobertura_report_command.stdout(Stdio::inherit());
    cobertura_report_command.stderr(Stdio::inherit());

    println!("Running command {:#?}", cobertura_report_command);
    println!("=================================================");
    cobertura_report_command.output()?;

    // Check coverage threshold
    let line_coverage_selector = Selector::parse(r#"nav.level p:nth-of-type(2) abbr"#).unwrap();
    let mut document_string = String::new();
    File::open("/tmp/carp/coverage/html/index.html")?.read_to_string(&mut document_string)?;
    let document = Html::parse_document(&document_string);
    let line_coverage_node = document.select(&line_coverage_selector).next().unwrap();
    let line_coverage = line_coverage_node
        .text()
        .next()
        .unwrap()
        .trim_end_matches('%')
        .trim();

    // Cleanup

    println!("Cleaning up files");
    println!("=================================================");
    let mut profraw_files_paths: Vec<PathBuf> = read_dir(".")?
        .map(|entry| entry.map(|e| e.path()))
        .collect::<Result<Vec<PathBuf>, std::io::Error>>()?;
    loop {
        let Some(path) = profraw_files_paths.pop() else {
            break;
        };
        if path.extension() == Some(OsStr::new("profraw")) {
            remove_file(path)?;
        } else if path.is_dir() {
            profraw_files_paths.append(
                &mut read_dir(path)?
                    .map(|rs| rs.map(|entry| entry.path()))
                    .collect::<Result<Vec<PathBuf>, std::io::Error>>()?,
            );
        }
    }
    println!("Removed files *.profraw files");
    println!("=================================================");

    println!("CHECKING LINE COVERAGE. GOT: {}", line_coverage);

    let line_coverage: f64 = line_coverage.parse()?;

    println!("=================================================");

    if line_coverage < LINE_COVERAGE_THRESHOLD {
        return Err(anyhow::Error::from(CoverageError::LineCoverageNotExceeded(
            line_coverage,
        )));
    }

    Ok(())
}
