use std::path::PathBuf;

use thiserror::Error;
use tokio::task::JoinError;

#[derive(Error, Debug)]
pub enum Error<FutError> {
    #[error("Error while reading response from worker process")]
    ReadError,
    #[error("Error while sending response from worker process")]
    WriteError,
    #[error("Error opening {0} file")]
    OpenFileError(PathBuf, #[source] fmmap::error::Error),
    #[error("Error creating {0} directory")]
    CreateDirError(PathBuf, #[source] std::io::Error),
    #[error("Error obtaining response")]
    FutureError(#[source] FutError),
    #[error("Worker exited")]
    WorkerError,
    #[error(transparent)]
    SerializeError(#[from] bincode::Error),
    #[error(transparent)]
    FmmapError(#[from] fmmap::error::Error),
    #[error(transparent)]
    JoinError(#[from] JoinError),
    #[error(transparent)]
    HyperError(#[from] hyper::Error),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
}
