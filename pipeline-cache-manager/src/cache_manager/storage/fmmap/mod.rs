use std::{
    env::temp_dir,
    fmt::UpperHex,
    sync::Arc,
    time::{Duration, SystemTime},
};

use self::error::Error;
use bytes::Bytes;
use fmmap::tokio::{AsyncMmapFileExt, AsyncMmapFileMut, AsyncMmapFileMutExt, AsyncMmapFileReader};
use futures::{
    future::{select, try_join, Either},
    pin_mut, Future, FutureExt, StreamExt, TryFutureExt,
};
use http::{HeaderMap, Response, StatusCode, Version};
use hyper::{
    body::{HttpBody, Sender},
    Body,
};
use parking_lot::{Mutex, RwLock};
use pipeline::caching::storage::CacheStorage;
use serde::{Deserialize, Serialize};
use tokio::{
    fs::create_dir_all,
    io::{AsyncReadExt, AsyncWriteExt},
    sync::{
        mpsc::{self, UnboundedReceiver, UnboundedSender},
        oneshot,
    },
    task::JoinHandle,
};

pub mod error;

#[derive(Serialize, Deserialize)]
struct ResponseMetadata {
    #[serde(with = "http_serde::status_code")]
    status: StatusCode,
    #[serde(with = "http_serde::header_map")]
    headers: HeaderMap,
    #[serde(with = "http_serde::version")]
    version: Version,
}

struct State {
    headers_file: Arc<AsyncMmapFileMut>,
    body_file: Arc<AsyncMmapFileMut>,
    has_been_written: bool,
}

impl State {
    pub fn get_mut(&mut self) -> (&mut AsyncMmapFileMut, &mut AsyncMmapFileMut) {
        let headers_file = Arc::get_mut(&mut self.headers_file).unwrap();
        let body_file = Arc::get_mut(&mut self.body_file).unwrap();

        (headers_file, body_file)
    }
}

enum Operation<Fut, FutError> {
    Invalidate(Option<oneshot::Sender<Result<(), Error<FutError>>>>),
    LoadOrStore(
        Fut,
        oneshot::Sender<Result<Response<Body>, Error<FutError>>>,
    ),
}

impl<Fut, Err> Operation<Fut, Err> {
    pub fn is_invalidate(&self) -> bool {
        match self {
            Operation::Invalidate(_) => true,
            _ => false,
        }
    }
}

async fn write_response<Err>(bytes: &[u8], file: &mut AsyncMmapFileMut) -> Result<(), Error<Err>> {
    log::trace!("Writing bytes from response headers: {bytes:?}");
    file.truncate(bytes.len() as u64).await?;
    let mut writer = file.writer(0)?;
    writer.write_all(bytes).await?;
    log::trace!("Finished writing response headers");

    Ok(())
}

const BLOCK_SIZE: u64 = 8192;

async fn forward_response<Err>(
    length: Option<u64>,
    mut body: Body,
    file: &mut AsyncMmapFileMut,
    mut response_writer: hyper::body::Sender,
) -> Result<(), Error<Err>> {
    async fn init_length<Err>(length: u64, file: &mut AsyncMmapFileMut) -> Result<u64, Error<Err>> {
        file.truncate(length).await?;
        Ok(length)
    }

    let mut accumulated_len = 0;
    let mut accumulated_real_len = if let Some(length) = length {
        init_length(length, file).await?
    } else {
        init_length(BLOCK_SIZE, file).await?
    };

    while let Some(bytes) = body.next().await {
        let bytes = bytes?;
        log::trace!("Received bytes from response body: {bytes:?}",);

        log::trace!(
            "Writing to cache file at position: {accumulated_len} and real_len: {accumulated_real_len}",
        );
        let mut writer = file.writer(accumulated_len)?;
        try_join(
            writer.write_all(&bytes).map_err(Error::IoError),
            response_writer
                .send_data(bytes.clone())
                .map_err(Error::HyperError),
        )
        .await?;
        log::trace!("Finished writing to response body");
        accumulated_len += bytes.len();

        if accumulated_len as u64 > accumulated_real_len {
            accumulated_real_len += BLOCK_SIZE;
            file.truncate(accumulated_real_len).await?;
        }
    }
    log::trace!("Truncating body file to real length {accumulated_len}");
    file.truncate(accumulated_len as u64).await?;
    log::trace!("Finished writing response body");

    Ok(())
}

async fn send_response<Err>(
    response_sender: oneshot::Sender<Result<Response<Body>, Error<Err>>>,
    body: Body,
    reader: &mut AsyncMmapFileReader<'_>,
) -> Result<(), Error<Err>>
where
    Err: std::fmt::Debug,
{
    let operation_result = || async {
        let mut read_buffer = Vec::new();
        reader.read_to_end(&mut read_buffer).await?;

        let metadata: ResponseMetadata = bincode::deserialize(&read_buffer)?;
        let mut response = Response::builder()
            .status(metadata.status)
            .body(body)
            .unwrap();
        *response.headers_mut() = metadata.headers;
        log::trace!("Deserialized response from cache: {response:?}");
        Ok(response)
    };
    response_sender.send(operation_result().await).unwrap();
    Ok(())
}

async fn forward_cache<Err>(
    mut sender: Sender,
    reader: &mut AsyncMmapFileReader<'_>,
) -> Result<(), Error<Err>> {
    let mut read_buffer = vec![0; 4096];

    while let Ok(size) = reader.read(&mut read_buffer).await {
        if size == 0 {
            break;
        }

        log::trace!("Send bytes from cached body {read_buffer:?} to response body");
        sender
            .send_data(Bytes::copy_from_slice(&read_buffer[..size]))
            .await?;
    }

    Ok(())
}

async fn receive_command<Fut, Err>(
    duration: &mut Option<(Duration, u64)>,
    receiver: &mut UnboundedReceiver<Operation<Fut, Err>>,
    buffer: &mut Option<Option<Operation<Fut, Err>>>,
) -> Option<Operation<Fut, Err>> {
    let recv_fut = receiver.recv();
    pin_mut!(recv_fut);

    if let Some(operation) = std::mem::take(buffer) {
        log::trace!("Reading operation from buffer");
        return operation;
    }

    if let Some((duration, ttl)) = duration.as_mut() {
        log::trace!("Waiting for {duration:#?}");
        let sleep_fut = tokio::time::sleep(*duration).map(|_| Some(Operation::Invalidate(None)));
        let now = SystemTime::now();
        pin_mut!(sleep_fut);

        log::trace!("Retrieve opertion from channel or timeout");
        let (is_from_channel, operation) = select(recv_fut, sleep_fut)
            .map(|either| match either {
                Either::Left((op, _)) => (true, op),
                Either::Right((op, _)) => (false, op),
            })
            .await;

        if !is_from_channel {
            log::trace!("Returning invalidation from TTL");
            *duration = Duration::from_secs(*ttl as u64);
            debug_assert!(operation.as_ref().filter(|op| op.is_invalidate()).is_some());
            return operation;
        }

        let mut elapsed = now.elapsed().unwrap();

        if &mut elapsed >= duration {
            log::trace!("TTL time hit while waiting for operation");
            let secs = *ttl - (elapsed.as_secs() % *ttl);
            log::trace!("Wrapped seconds elapsed {secs}");
            *duration = Duration::from_secs(secs);
            log::trace!("Update wrapping duration to {duration:#?}");

            if operation.is_some() {
                *buffer = Some(operation);
            }

            Some(Operation::Invalidate(None))
        } else {
            log::trace!("TTL time not hit while waiting for operation");
            *duration = *duration - elapsed;
            log::trace!("Deduced duration by {elapsed:#?}: {duration:#?}");
            operation
        }
    } else {
        log::trace!("Reading operation from channel directly");
        recv_fut.await
    }
}

async fn backend_store<Fut, Err>(
    mut recv: UnboundedReceiver<Operation<Fut, Err>>,
    headers_file: AsyncMmapFileMut,
    body_file: AsyncMmapFileMut,
    ttl: Option<u64>,
) -> Result<(), Error<Err>>
where
    Fut: Future<Output = Result<Response<hyper::Body>, Err>> + Send,
    Err: std::error::Error + Sync + Send + 'static,
{
    let mut buffer = None;
    let mut duration = ttl.map(|ttl| (Duration::from_secs(ttl), ttl));

    let state = RwLock::new(State {
        headers_file: Arc::new(headers_file),
        body_file: Arc::new(body_file),
        has_been_written: false,
    });

    while let Some(op) = receive_command(&mut duration, &mut recv, &mut buffer).await {
        match op {
            Operation::Invalidate(channel) => {
                let operation_result = || async {
                    let mut state = state.write();
                    let (headers_file, body_file) = state.get_mut();

                    log::trace!("Setting headers file length to 0");
                    headers_file.truncate(0).await?;
                    log::trace!("Setting body file length to 0");
                    body_file.truncate(0).await?;

                    log::trace!("Synchronizing changes");
                    headers_file.flush_async()?;
                    body_file.flush_async()?;

                    log::trace!("Set has been written to false");
                    state.has_been_written = false;
                    Ok(())
                };

                if let Some(channel) = channel {
                    channel.send(operation_result().await).unwrap();
                } else {
                    operation_result().await?;
                }

                duration = None;
            }
            Operation::LoadOrStore(response_future, response_sender) => loop {
                let state_read = state.read();

                if !state_read.has_been_written {
                    drop(state_read);
                    match state.try_write() {
                        Some(mut state) => {
                            let operation_result = || async {
                                let response = response_future.await.map_err(Error::FutureError)?;
                                log::trace!("Received response {response:#?} from future");
                                let (parts, body) = response.into_parts();
                                let metadata = ResponseMetadata {
                                    status: parts.status,
                                    version: parts.version,
                                    headers: parts.headers,
                                };
                                let metadata_bytes = bincode::serialize(&metadata)?;
                                let (headers_file, body_file) = state.get_mut();

                                let (tx, body_to_send) = Body::channel();
                                let mut response_to_send = Response::builder()
                                    .status(metadata.status)
                                    .version(parts.version)
                                    .body(body_to_send)
                                    .unwrap();

                                let content_length: Option<u64> = metadata
                                    .headers
                                    .get("content-length")
                                    .and_then(|val| val.to_str().ok())
                                    .and_then(|val| val.parse().ok())
                                    .or_else(|| body.size_hint().exact());

                                log::trace!("Incoming response body with {content_length:?} bytes");

                                *response_to_send.headers_mut() = metadata.headers;

                                Ok((
                                    response_to_send,
                                    content_length,
                                    metadata_bytes,
                                    headers_file,
                                    body_file,
                                    body,
                                    tx,
                                ))
                            };

                            log::trace!("Sending response to consumer");
                            match operation_result().await {
                                Ok((
                                    response_to_send,
                                    content_length,
                                    metadata_bytes,
                                    mut headers_file,
                                    mut body_file,
                                    body,
                                    tx,
                                )) => {
                                    response_sender.send(Ok(response_to_send)).unwrap();
                                    let write_response: Result<_, Error<Err>> = try_join(
                                        write_response(&metadata_bytes, &mut headers_file),
                                        forward_response(content_length, body, &mut body_file, tx),
                                    )
                                    .await;
                                    write_response?;
                                    state.has_been_written = true;
                                    duration = ttl.map(|ttl| (Duration::from_secs(ttl), ttl));
                                }
                                Err(err) => response_sender.send(Err(err)).unwrap(),
                            }
                            log::trace!("Sent response to consumer");
                            break;
                        }
                        None => continue,
                    }
                } else {
                    let state = state_read;
                    log::trace!("Starting to read response from cache");
                    let (sender, body) = Body::channel();
                    let headers_file = Arc::clone(&state.headers_file);
                    let body_file = Arc::clone(&state.body_file);

                    tokio::spawn(async move {
                        let readers_result = || async {
                            let headers_reader = headers_file.reader(0)?;
                            let body_reader = body_file.reader(0)?;

                            Ok((headers_reader, body_reader))
                        };
                        log::trace!("Obtained reader to cache");
                        match readers_result().await {
                            Ok((mut headers_reader, mut body_reader)) => {
                                log::trace!("Starting to send cached response to consumer");
                                try_join(
                                    send_response(response_sender, body, &mut headers_reader),
                                    forward_cache(sender, &mut body_reader),
                                )
                                .await?;
                                log::trace!("Finished sending cached response to consumer");
                            }
                            Err(err) => {
                                response_sender.send(Err(err)).unwrap();
                            }
                        }
                        Ok::<_, Error<Err>>(())
                    });

                    break;
                }
            },
        }
    }

    Ok(())
}

pub struct FmmapBackendStore<Fut, Err>(
    UnboundedSender<Operation<Fut, Err>>,
    Arc<Mutex<Option<JoinHandle<Result<(), Error<Err>>>>>>,
);

impl<Fut, Err> FmmapBackendStore<Fut, Err>
where
    Fut: Future<Output = Result<Response<hyper::Body>, Err>> + Send + 'static,
    Err: std::error::Error + Sync + Send + 'static,
{
    pub async fn try_new<Id>(
        ttl: Option<u64>,
        cache_name: String,
        id: Id,
    ) -> Result<Self, Error<Err>>
    where
        Id: UpperHex,
    {
        let (tx, rx) = mpsc::unbounded_channel();
        let tmp_path = temp_dir().join("carp").join("cache").join(cache_name);
        create_dir_all(&tmp_path)
            .await
            .map_err(|err| Error::CreateDirError(tmp_path.clone(), err))?;
        let headers_path = tmp_path.clone().join(format!("{id:02X}.headers"));
        let body_path = tmp_path.join(format!("{id:02X}.body"));
        let mut headers_file = AsyncMmapFileMut::open(headers_path.clone())
            .await
            .map_err(|err| Error::OpenFileError(headers_path, err))?;
        let mut body_file = AsyncMmapFileMut::open(body_path.clone())
            .await
            .map_err(|err| Error::OpenFileError(body_path, err))?;

        headers_file.set_remove_on_drop(true);
        body_file.set_remove_on_drop(true);
        let jh = tokio::spawn(backend_store(rx, headers_file, body_file, ttl));

        Ok(FmmapBackendStore(tx, Arc::new(Mutex::new(Some(jh)))))
    }
}

impl<Fut, Err> FmmapBackendStore<Fut, Err>
where
    Fut: Future<Output = Result<Response<Body>, Err>>,
{
    async fn load_or_store(
        jh: Arc<Mutex<Option<JoinHandle<Result<(), Error<Err>>>>>>,
        sender: UnboundedSender<Operation<Fut, Err>>,
        fut: Fut,
    ) -> Result<Response<Body>, Error<Err>> {
        let (tx, rx) = oneshot::channel();
        sender
            .send(Operation::LoadOrStore(fut, tx))
            .map_err(|_| Error::WriteError)?;
        let Ok(response) = rx.await else {
            let Some(jh) = jh.lock().take() else {
                return Err(Error::WorkerError);
            };
            return Err(jh.await.unwrap().unwrap_err());
        };

        response
    }

    async fn delete(
        jh: Arc<Mutex<Option<JoinHandle<Result<(), Error<Err>>>>>>,
        sender: UnboundedSender<Operation<Fut, Err>>,
    ) -> Result<(), Error<Err>> {
        let (tx, rx) = oneshot::channel();
        sender
            .send(Operation::Invalidate(Some(tx)))
            .map_err(|_| Error::WriteError)?;

        if let Err(_) = rx.await {
            let Some(jh) = jh.lock().take() else {
                return Err(Error::WorkerError);
            };
            return Err(jh.await.unwrap().unwrap_err());
        };

        Ok(())
    }
}

impl<Fut, E> CacheStorage<Body, Fut> for FmmapBackendStore<Fut, E>
where
    hyper::Body: From<Body> + Into<Body>,
    Fut: Future<Output = Result<Response<Body>, E>>,
{
    type Error = Error<E>;
    type LoadOrStoreFuture = impl Future<Output = Result<Response<Body>, Self::Error>>;
    type InvalidateFuture = impl Future<Output = Result<(), Self::Error>>;

    fn invalidate(&self) -> Self::InvalidateFuture {
        Self::delete(self.1.clone(), self.0.clone())
    }

    fn load_or_store(&self, response_fut: Fut) -> Self::LoadOrStoreFuture {
        Self::load_or_store(self.1.clone(), self.0.clone(), response_fut)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::Infallible;

    use futures::future::{pending, ready};
    use http::HeaderValue;
    use hyper::body::{self, to_bytes};
    use tokio::fs::File;

    use super::*;

    // Test that the cache backend is able to invalidate after the specified ttl
    #[test_log::test(tokio::test)]
    async fn it_invalidates_after_ttl_seconds() {
        // Given
        let id = Bytes::from("id-1");
        let cache_name = "Dummy Cache -1".to_owned();
        let sut = FmmapBackendStore::try_new(Some(10), cache_name.clone(), id.clone())
            .await
            .unwrap();
        let invalidated_response = Response::builder().status(202).body(Body::empty()).unwrap();
        let stored_response = Response::builder()
            .status(200)
            .body(Body::from("Hello World"))
            .unwrap();
        let _response = sut
            .load_or_store(ready(Ok::<_, Infallible>(invalidated_response)))
            .await
            .unwrap();

        // When
        tokio::time::sleep(std::time::Duration::from_secs(10)).await;
        let (parts, body) = sut
            .load_or_store(ready(Ok::<_, Infallible>(stored_response)))
            .await
            .unwrap()
            .into_parts();

        // Then
        let response_body = to_bytes(body).await.unwrap();
        let tmp_path = temp_dir().join("carp").join("cache").join(cache_name);
        let headers_path = tmp_path.clone().join(format!("{id:02X}.headers"));
        let body_path = tmp_path.join(format!("{id:02X}.body"));

        // Assert header and body files exists
        assert!(headers_path.exists());
        assert!(body_path.exists());

        // Assert body file is not empty and has size 11
        assert_eq!(body_path.metadata().unwrap().len(), 11);
        // Assert headers file has metadata with status 200, HTTP version 1.1 and empty headers in bincode
        // serialization
        let mut buffer = Default::default();
        File::open(headers_path)
            .await
            .unwrap()
            .read_to_end(&mut buffer)
            .await
            .unwrap();
        let metadata = bincode::deserialize::<ResponseMetadata>(&*buffer).unwrap();

        assert_eq!(metadata.status, 200);
        assert_eq!(metadata.headers.len(), 0);
        assert_eq!(metadata.version, Version::HTTP_11);

        // Assert returned response componentes are equal to the stored response
        assert_eq!(parts.status, 200);
        assert_eq!(&*response_body, b"Hello World");
    }

    #[tokio::test]
    async fn it_writes_response_to_temporal_file_with_chunked_body_when_polling_body() {
        // Given
        let id = Bytes::from("id-1");
        let cache_name = "Dummy Cache 0".to_owned();
        let sut = FmmapBackendStore::try_new(None, cache_name.clone(), id.clone())
            .await
            .unwrap();
        let response = Response::builder()
            .status(200)
            .header("Accept", "json")
            .header("Content-Type", "text/plain")
            .header("Accept-Encoding", "utf-8")
            .body(Body::wrap_stream(futures::stream::iter(vec![
                Ok::<_, hyper::Error>("Hello"),
                Ok::<_, hyper::Error>(" "),
                Ok::<_, hyper::Error>("World"),
            ])))
            .unwrap();

        // When
        let (_, body) = sut
            .load_or_store(ready(Ok::<_, Infallible>(response)))
            .await
            .unwrap()
            .into_parts();

        // Then
        let body = to_bytes(body).await.unwrap();
        let tmp_path = temp_dir().join("carp").join("cache").join(cache_name);
        let headers_path = tmp_path.clone().join(format!("{id:02X}.headers"));
        let body_path = tmp_path.join(format!("{id:02X}.body"));

        let mut headers_file = File::open(headers_path).await.unwrap();
        let mut metadata_buffer = Vec::new();

        headers_file
            .read_to_end(&mut metadata_buffer)
            .await
            .unwrap();
        let metadata: ResponseMetadata = bincode::deserialize(&metadata_buffer).unwrap();

        let mut body_file = File::open(body_path).await.unwrap();
        let mut body_buffer = Vec::new();

        body_file.read_to_end(&mut body_buffer).await.unwrap();

        assert_eq!(metadata.status, 200);
        assert_eq!(metadata.headers.len(), 3);
        assert_eq!(
            metadata.headers.get("Accept"),
            Some(&HeaderValue::try_from("json").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Content-Type"),
            Some(&HeaderValue::try_from("text/plain").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Accept-Encoding"),
            Some(&HeaderValue::try_from("utf-8").unwrap())
        );
        assert_eq!(
            body,
            Bytes::from("Hello World"),
            "First returned body does not match"
        );
        assert_eq!(
            Bytes::from(body_buffer),
            Bytes::from("Hello World"),
            "Cached file contents do not match"
        );
    }

    #[tokio::test]
    async fn it_writes_response_to_temporal_file() {
        // Given
        let id = Bytes::from("id-1");
        let cache_name = "Dummy Cache 1".to_owned();
        let sut = FmmapBackendStore::try_new(None, cache_name.clone(), id.clone())
            .await
            .unwrap();
        let response = Response::builder()
            .status(200)
            .header("Accept", "json")
            .header("Content-Type", "text/plain")
            .header("Accept-Encoding", "utf-8")
            .body(Body::from("Hello World"))
            .unwrap();

        // When
        let _response = sut
            .load_or_store(ready(Ok::<_, Infallible>(response)).right_future())
            .await
            .unwrap();

        // Then
        sut.load_or_store(pending().left_future()).await.unwrap();
        let tmp_path = temp_dir().join("carp").join("cache").join(cache_name);
        let headers_path = tmp_path.clone().join(format!("{id:02X}.headers"));
        let body_path = tmp_path.join(format!("{id:02X}.body"));

        let mut headers_file = File::open(headers_path).await.unwrap();
        let mut metadata_buffer = Vec::new();

        headers_file
            .read_to_end(&mut metadata_buffer)
            .await
            .unwrap();
        let metadata: ResponseMetadata = bincode::deserialize(&metadata_buffer).unwrap();

        let mut body_file = File::open(body_path).await.unwrap();
        let mut body_buffer = Vec::new();

        body_file.read_to_end(&mut body_buffer).await.unwrap();

        assert_eq!(metadata.status, 200);
        assert_eq!(metadata.headers.len(), 3);
        assert_eq!(
            metadata.headers.get("Accept"),
            Some(&HeaderValue::try_from("json").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Content-Type"),
            Some(&HeaderValue::try_from("text/plain").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Accept-Encoding"),
            Some(&HeaderValue::try_from("utf-8").unwrap())
        );
        assert_eq!(Bytes::from(body_buffer), Bytes::from("Hello World"));
    }

    #[tokio::test]
    async fn it_reads_response_after_writing_it() {
        // Given
        let id = Bytes::from("id-1");
        let cache_name = "Dummy Cache 2".to_owned();
        let sut = FmmapBackendStore::try_new(None, cache_name.clone(), id.clone())
            .await
            .unwrap();
        let response = Response::builder()
            .status(200)
            .header("Accept", "xml")
            .header("Content-Type", "text/plain")
            .header("Accept-Encoding", "utf-8")
            .body(Body::from("Hello World"))
            .unwrap();

        let _response = sut
            .load_or_store(ready(Ok::<_, Infallible>(response)).right_future())
            .await
            .unwrap();

        // When
        let (metadata, body) = sut
            .load_or_store(pending().left_future())
            .await
            .unwrap()
            .into_parts();

        // Then
        let body_buffer = body::to_bytes(body).await.unwrap();
        assert_eq!(metadata.status, 200);
        assert_eq!(metadata.headers.len(), 3);
        assert_eq!(
            metadata.headers.get("Accept"),
            Some(&HeaderValue::try_from("xml").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Content-Type"),
            Some(&HeaderValue::try_from("text/plain").unwrap())
        );
        assert_eq!(
            metadata.headers.get("Accept-Encoding"),
            Some(&HeaderValue::try_from("utf-8").unwrap())
        );
        assert_eq!(body_buffer, Bytes::from("Hello World"));
    }

    #[tokio::test]
    async fn it_clears_file_when_invalidating() {
        // Given
        let id = Bytes::from("id-1");
        let cache_name = "Dummy Cache 3".to_owned();
        let sut = FmmapBackendStore::try_new(None, cache_name.clone(), id.clone())
            .await
            .unwrap();
        let response = Response::builder()
            .status(200)
            .header("Accept", "json")
            .header("Content-Type", "text/plain")
            .header("Accept-Encoding", "utf-8")
            .body(Body::from("Hello World"))
            .unwrap();

        let _response = sut
            .load_or_store(ready(Ok::<_, Infallible>(response)))
            .await
            .unwrap();

        // When
        sut.invalidate().await.unwrap();

        // Then
        let tmp_path = temp_dir().join("carp").join("cache").join(cache_name);
        let headers_path = tmp_path.clone().join(format!("{id:02X}.headers"));
        let body_path = tmp_path.join(format!("{id:02X}.body"));

        assert_eq!(
            0,
            File::open(&headers_path)
                .await
                .unwrap()
                .metadata()
                .await
                .unwrap()
                .len()
        );
        assert_eq!(
            0,
            File::open(&body_path)
                .await
                .unwrap()
                .metadata()
                .await
                .unwrap()
                .len()
        );
    }
}
