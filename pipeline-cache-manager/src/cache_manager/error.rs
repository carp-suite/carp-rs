use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error<Backend, Client> {
    #[error("Error while loading response from cache")]
    Backend(#[source] Backend),
    #[error("Error while getting response from origin")]
    Client(#[from] Client),
}
