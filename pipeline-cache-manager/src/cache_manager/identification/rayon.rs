use bytes::{BufMut, Bytes, BytesMut};
use http::Request;
use patricia_tree::PatriciaSet;
use pipeline::caching::identification::{iter::Iter, CacheIdentification};
use rayon::{
    prelude::{IntoParallelIterator, IntoParallelRefIterator, ParallelBridge, ParallelIterator},
    slice::ParallelSliceMut,
    str::ParallelString,
};
use std::collections::HashSet;

const AMP: &'static [u8] = b"&";
const EQ: &'static [u8] = b"=";

pub struct RayonCacheIdentification {
    route_parameters: HashSet<String>,
    query_parameters: PatriciaSet,
    headers: HashSet<String>,
}

impl RayonCacheIdentification {
    pub fn new(
        route_parameters: impl IntoIterator<Item = String>,
        query_parameters: impl IntoIterator<Item = String>,
        headers: impl IntoIterator<Item = String>,
    ) -> Self {
        let mut query_parameters_trie = PatriciaSet::new();

        query_parameters_trie.extend(query_parameters);

        RayonCacheIdentification {
            route_parameters: HashSet::from_iter(route_parameters),
            query_parameters: query_parameters_trie,
            headers: HashSet::from_iter(headers.into_iter().map(|s| s.to_lowercase())),
        }
    }

    fn combine_kv_into_bytes<K, V>(mut bytes: BytesMut, (k, v): (K, V)) -> BytesMut
    where
        K: AsRef<[u8]>,
        V: AsRef<[u8]>,
    {
        if !bytes.is_empty() {
            bytes.put(AMP);
        }

        bytes.put(k.as_ref());
        bytes.put(EQ);
        bytes.put(v.as_ref());

        bytes
    }
}

impl<ReqBody> CacheIdentification<ReqBody> for RayonCacheIdentification {
    type Id = Bytes;

    fn identify<'k, 'v, Params>(
        &self,
        route_parameters: Params,
        request: &Request<ReqBody>,
    ) -> Self::Id
    where
        Params: Iter<'k, 'v>,
    {
        let route_parameters = route_parameters
            .iter()
            .par_bridge()
            .filter(|(k, _)| self.route_parameters.contains(*k))
            .map(|(k, v)| (k.as_bytes(), v.as_bytes()));

        let headers = request
            .headers()
            .iter()
            .par_bridge()
            .filter(|(k, _)| self.headers.contains(k.as_str()))
            .map(|(k, v)| (k.as_ref(), v.as_ref()));

        let query_parameters = request.uri().query();
        let query_parameters = query_parameters
            .par_iter()
            .flat_map(|q| q.par_split(|c| c == '&'))
            .filter_map(|assignment| {
                self.query_parameters
                    .get_longest_common_prefix(assignment)
                    .and_then(|prefix_bytes| {
                        let string_prefix = String::from_utf8_lossy(prefix_bytes);
                        let prefix_len = string_prefix.len();
                        if assignment[..prefix_len + 1].ends_with("=") {
                            Some((
                                assignment[..prefix_len].as_bytes(),
                                assignment[prefix_len + 1..].as_bytes(),
                            ))
                        } else {
                            None
                        }
                    })
            });

        let mut pairs_vec: Vec<(&[u8], &[u8])> = route_parameters
            .chain(query_parameters)
            .chain(headers)
            .collect();

        pairs_vec.par_sort_by_key(|(k, _)| *k);

        let bytes_mut = pairs_vec
            .into_par_iter()
            .fold(|| BytesMut::new(), Self::combine_kv_into_bytes)
            .reduce(
                || BytesMut::new(),
                |mut a, b| {
                    if a.is_empty() {
                        return b;
                    }

                    if b.is_empty() {
                        return a;
                    }

                    a.put(AMP);
                    a.extend(b);

                    a
                },
            );

        Bytes::from(bytes_mut)
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, iter::Map};

    use super::*;
    use http::{Request, Uri};

    struct HashMapIter<'a>(&'a HashMap<String, String>);

    impl<'a, 'k, 'v> Iter<'k, 'v> for HashMapIter<'a>
    where
        'a: 'k,
        'a: 'v,
    {
        type Iterator = Map<
            std::collections::hash_map::Iter<'a, String, String>,
            fn((&'a String, &'a String)) -> (&'k str, &'v str),
        >;

        fn iter(&self) -> Self::Iterator {
            self.0
                .iter()
                .map(|(name, value)| (name.as_str(), value.as_str()))
        }
    }

    #[test]
    fn it_generates_identification_successfully() {
        // Given
        let route_parameters = vec!["id".to_string(), "path".to_string()];
        let query_parameters = vec!["format".to_string(), "lang".to_string()];
        let headers = vec!["Authorization".to_string(), "Accept".to_string()];
        let request = Request::builder()
            .header("Authorization", "Basic bWtuOjEyMzQ=")
            .header("Accept", "json")
            .header("Accept-Encoding", "utf-8")
            .uri(Uri::from_static(
                "http://some-host/api/123/some/path?format=mpf&lang=en",
            ))
            .body(())
            .unwrap();
        let route_parameters_map = HashMap::from_iter(vec![
            ("id".to_string(), "123".to_string()),
            ("path".to_string(), "some/path".to_string()),
            ("other".to_string(), "value".to_string()),
        ]);
        let sut = RayonCacheIdentification::new(route_parameters, query_parameters, headers);
        let params = HashMapIter(&route_parameters_map);

        // When
        let identification = sut.identify(params, &request);

        // Then
        assert_eq!(Bytes::from("accept=json&authorization=Basic bWtuOjEyMzQ=&format=mpf&id=123&lang=en&path=some/path"), identification);
    }
}
