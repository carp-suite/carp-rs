use http::Request;
use pipeline::caching::identification::{iter::Iter, CacheIdentification};

pub struct Uuidv5Identification<Inner> {
    namespace: uuid::Uuid,
    inner: Inner,
}

impl<Inner> Uuidv5Identification<Inner> {
    const NAMESPACE: uuid::Uuid = uuid::uuid!("1efc7f27-070f-4a94-b33a-f3bb9510ba2b");
}

impl<Inner> Uuidv5Identification<Inner> {
    pub fn new(namespace: &[u8], inner: Inner) -> Self {
        let namespace = uuid::Uuid::new_v5(&Self::NAMESPACE, &namespace);
        Uuidv5Identification { namespace, inner }
    }
}

impl<Inner, ReqBody> CacheIdentification<ReqBody> for Uuidv5Identification<Inner>
where
    Inner: CacheIdentification<ReqBody>,
    Inner::Id: AsRef<[u8]>,
{
    type Id = uuid::Uuid;

    fn identify<'k, 'v, Params>(&self, params: Params, req: &Request<ReqBody>) -> Self::Id
    where
        Params: Iter<'k, 'v>,
    {
        let id = self.inner.identify(params, req);
        let uuid = uuid::Uuid::new_v5(&self.namespace, id.as_ref());

        uuid
    }
}

#[cfg(test)]
mod tests {
    use hyper::Body;

    use super::*;

    struct MockIdentification;

    impl CacheIdentification<Body> for MockIdentification {
        type Id = &'static [u8];
        fn identify<'k, 'v, Params>(&self, _params: Params, _req: &Request<Body>) -> &'static [u8]
        where
            Params: Iter<'k, 'v>,
        {
            b"id"
        }
    }

    struct Params<'a, 'k, 'v>(&'a [(&'k str, &'v str)]);

    impl<'a, 'k, 'v> Iter<'k, 'v> for Params<'a, 'k, 'v> {
        type Iterator = std::iter::Map<
            std::slice::Iter<'a, (&'k str, &'v str)>,
            fn(&(&'k str, &'v str)) -> (&'k str, &'v str),
        >;

        fn iter(&self) -> Self::Iterator {
            self.0.iter().map(|(k, v)| (k, v))
        }
    }

    #[test]
    fn it_generates_uuidv5_identification_from_namespace_and_inner_identification() {
        // Given
        let namespace = b"namespace";
        let sut = Uuidv5Identification::new(namespace, MockIdentification);

        // When
        let id = sut.identify(Params(&[("hello", "world")]), &Request::new(Body::empty()));

        // Then
        let mut buf = [0; 1024];
        assert_eq!(
            id.hyphenated().encode_lower(&mut buf),
            "a4aab41f-e386-510b-a10a-bfdefd2e1ad5"
        );
    }
}
